<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'v5\SubscriptionsController@showWelcome');

# Subscription Route
Route::get('subscribe', 'v5\SubscriptionsController@subscriptionPlanInfo')->name('subscribe');
Route::get('payment-checkout', 'v5\SubscriptionsController@subscriptionCheckoutInfo')->name('subscribe_payment');

# Vue Js(Front End Url)
Route::get('get-price-info', 'v5\SubscriptionsController@subscriptionPricingInfo')->name('get_price_info');
Route::post('get-subscribe', 'v5\SubscriptionsController@subscriptionSave')->name('store_scription_data');

# Success Route
Route::get('payment-success', 'v5\SubscriptionsController@paymentSuccess')->name('get_price_info');

# Webhook Route
Route::stripeWebhooks('webhooks');
