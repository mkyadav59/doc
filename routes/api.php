<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'v3'],function(){
    Route::get('install/vendor','v3\InstallController@vendorConfig');
    Route::get('install/customer','v3\InstallController@customerConfig');


    Route::post('countries/all', 'v3\CountryController@getAllCountries');

    Route::post('vendor/register','v3\VendorsController@registerVendor');
    Route::post('vendor/login','v3\VendorsController@vendorLogin');
    Route::post('vendor/update','v3\VendorsController@updatevendor');
    Route::post('gcmlogin', 'v3\VendorsController@GcmLogin');
    

    Route::resource('truck','v3\TrucksController');
    Route::post('truck/detail','v3\TrucksController@truckDetail');
    Route::post('truck/multitrucks','v3\TrucksController@getMultiTrucks');
    Route::post('truck/update','v3\TrucksController@updateTruck');
    Route::post('gettrucks','v3\TrucksController@getTrucksByRadious');
    Route::post('gettruckdetail','v3\TrucksController@getTruckDetailsWithItems');
    Route::post('truck/setlivelocation','v3\TrucksController@setTruckLocation');
    Route::post('truck/getlivelocation','v3\TrucksController@getTruckLocation');
    Route::post('truck/onoff','v3\TrucksController@setTruckOnOff');
    Route::post('truck/setoperatingtime','v3\TrucksController@setTruckOperatingTime');
    Route::post('truck/devicetoken', 'v3\TrucksController@storeDeviceToken');
    //Route::post('truck/updatedevicetoken', 'v3\TrucksController@updateDeviceToken');
    Route::post('truck/makedeviceprimary', 'v3\TrucksController@turnPrimaryDeviceOn');
    Route::post('truck/startstripe', 'v3\TrucksController@startStripeAccount');
    Route::post('truck/stopstripe', 'v3\TrucksController@stopStripeAccount');
    
    

    Route::resource('customer','v3\CustomersController');
    Route::post('customer/update', 'v3\CustomersController@updateCUstomer');
    Route::post('customer/favourite', 'v3\CustomersController@setFavouriteTruck');
    Route::post('customer/devicetoken', 'v3\CustomersController@storeDeviceToken');

    
    //Route::resource('order','v3\OrdersController');
    Route::post('order/addtocart','v3\OrdersController@addToCart');
    Route::post('order/updatecart','v3\OrdersController@updateToCart');
    Route::post('order/removecart','v3\OrdersController@removeCart');
    Route::post('order/showcart','v3\OrdersController@showCart');
    Route::post('order/placeorder','v3\OrdersController@placeOrder');
    Route::post('order/cartsize','v3\OrdersController@cartItemQty');
    Route::get('order/orderdetail/{id}','v3\OrdersController@orderDetailById');
    Route::get('order/pastordersbycustomer/{id}','v3\OrdersController@pastOrdersByCustomerId');
    Route::post('order/pastordersbytruck','v3\OrdersController@pastOrdersByTruckId');
    Route::post('order/accept','v3\OrdersController@acceptOrder');
    Route::post('order/ready','v3\OrdersController@readyOrder');
    Route::post('order/reminder','v3\OrdersController@reminderOrder');
    Route::post('order/deliver','v3\OrdersController@deliverOrder');
    Route::post('order/cancel','v3\OrdersController@cancelOrder');
    Route::post('order/reject','v3\OrdersController@rejectOrder');
    Route::post('order/orderstatusnotification','v3\OrdersController@orderStatusNotification');
    Route::post('order/checkitemstock','v3\OrdersController@checkItemStock');

    Route::post('order/test','v3\OrdersController@testNotification');

    //category controller
    Route::resource('category','v3\CategoryController');
    Route::post('category/update','v3\CategoryController@updateCategory');
    Route::post('category/updateorder','v3\CategoryController@categoryUpdateOrder');
    Route::post('category/updatestatus','v3\CategoryController@categoryUpdateStatus');
    Route::get('allcategoriesbytruckid/{id}','v3\CategoryController@allcategoriesbytruckid');

    //Items controller 
    Route::resource('item','v3\ItemsController');
    Route::get('allitemsbycategoryid/{id}','v3\ItemsController@allItemsByCategoryId');
    Route::get('allitemsbytruckid/{id}','v3\ItemsController@allItemsByTruckId');
    Route::post('itemorder','v3\ItemsController@itemOrder');
    Route::post('item/update','v3\ItemsController@updateItem');
    Route::post('item/updateorder','v3\ItemsController@updateItemOrder');
    Route::post('item/updatestatus','v3\ItemsController@updateItemStatus');
    Route::post('item/updatestock','v3\ItemsController@updateItemInstock');


    Route::post('verifyvendorotp','v3\OtpController@verifyVendorOtp');
    Route::post('verifycustomerotp','v3\OtpController@verifyCustomerOtp');


    //Customization Type controller
    Route::get('allcustomizationsbyitemid/{id}','v3\CustomizationTypeController@allcustomizationsbyitemid');

    Route::post('customization/add','v3\CustomizationTypeController@addCustomization');
    Route::post('customization/update','v3\CustomizationTypeController@updateCustomization');
    Route::post('customization/updatestatus','v3\CustomizationTypeController@updateCustomizationStatus');
    Route::post('customization/delete','v3\CustomizationTypeController@deleteCustomization');
    Route::post('customization/moveup','v3\CustomizationTypeController@moveUpCustomization');
    Route::post('customization/movedown','v3\CustomizationTypeController@moveDownCustomization');



    //Customization Options controller
    Route::resource('customizationoption','v3\CustomizationOptionController');
    Route::post('customizationoption/update','v3\CustomizationOptionController@updateCustomizationOption');
    Route::post('customizationoption/delete','v3\CustomizationOptionController@deleteCustomizationOption');

    //Stripe
    Route::post('stripe/keys', 'v3\StripeController@getStripeKeys');
    Route::post('stripe/getpaymentintent', 'v3\StripeController@getPaymentIntent');
    Route::post('stripe/connectlink', 'v3\StripeController@getConnectLink');
    Route::post('stripe/diconnectstripe', 'v3\StripeController@disconnectStripeAccount');
    

    Route::get('stripe/callback/oauthauthorize', 'v3\StripeController@oauthAuthorize');
    Route::post('stripe/callback/webhook', 'v3\StripeController@webhook');


    //BROADCASTING

    Route::post('broadcast/hellocustomers', 'v3\BroadcastController@fromTruckToCustomers');

});




Route::group(['prefix' => 'v4'],function(){
    Route::get('install/vendor','v4\InstallController@vendorConfig');
    Route::get('install/customer','v4\InstallController@customerConfig');


    Route::post('countries/all', 'v4\CountryController@getAllCountries');

    Route::post('vendor/register','v4\VendorsController@registerVendor');
    Route::post('vendor/login','v4\VendorsController@vendorLogin');
    Route::post('vendor/update','v4\VendorsController@updatevendor');
    Route::post('gcmlogin', 'v4\VendorsController@GcmLogin');
    

    Route::resource('truck','v4\TrucksController');
    Route::post('truck/detail','v4\TrucksController@truckDetail');
    Route::post('truck/multitrucks','v4\TrucksController@getMultiTrucks');
    Route::post('truck/update','v4\TrucksController@updateTruck');
    Route::post('gettrucks','v4\TrucksController@getTrucksByRadious');
    Route::post('gettruckdetail','v4\TrucksController@getTruckDetailsWithItems');
    Route::post('truck/setlivelocation','v4\TrucksController@setTruckLocation');
    Route::post('truck/getlivelocation','v4\TrucksController@getTruckLocation');
    Route::post('truck/onoff','v4\TrucksController@setTruckOnOff');
    Route::post('truck/setoperatingtime','v4\TrucksController@setTruckOperatingTime');
    Route::post('truck/devicetoken', 'v4\TrucksController@storeDeviceToken');
    //Route::post('truck/updatedevicetoken', 'v4\TrucksController@updateDeviceToken');
    Route::post('truck/makedeviceprimary', 'v4\TrucksController@turnPrimaryDeviceOn');
    Route::post('truck/startstripe', 'v4\TrucksController@startStripeAccount');
    Route::post('truck/stopstripe', 'v4\TrucksController@stopStripeAccount');
    Route::post('truck/updatesettings', 'v4\TrucksController@updateDefaultSettings');
    
    
    Route::post('server/save', 'v4\ServersController@save');
    Route::post('server/show', 'v4\ServersController@show');
    Route::post('server/update', 'v4\ServersController@updateServer');
    Route::post('server/delete', 'v4\ServersController@deleteServer');


    Route::post('servicetable/save', 'v4\ServiceTablesController@save');
    Route::post('servicetable/show', 'v4\ServiceTablesController@show');
    Route::post('servicetable/update', 'v4\ServiceTablesController@updateServicetable');
    Route::post('servicetable/delete', 'v4\ServiceTablesController@deleteServicetable');


    Route::post('counterstation/save', 'v4\CounterStationController@save');
    Route::post('counterstation/show', 'v4\CounterStationController@show');
    Route::post('counterstation/update', 'v4\CounterStationController@updateCounterstation');
    Route::post('counterstation/delete', 'v4\CounterStationController@deleteCounterstation');


    Route::resource('customer','v4\CustomersController');
    Route::post('customer/update', 'v4\CustomersController@updateCUstomer');
    Route::post('customer/favourite', 'v4\CustomersController@setFavouriteTruck');
    Route::post('customer/devicetoken', 'v4\CustomersController@storeDeviceToken');

    
    //Route::resource('order','v4\OrdersController');
    
    Route::post('order/addtocart','v4\OrdersController@addToCart');
    Route::post('order/updatecart','v4\OrdersController@updateToCart');
    Route::post('order/removecart','v4\OrdersController@removeCart');
    Route::post('order/showcart','v4\OrdersController@showCart');
    Route::post('order/placeorder','v4\OrdersController@placeOrder');
    Route::post('order/cartsize','v4\OrdersController@cartItemQty');
    Route::get('order/orderdetail/{id}','v4\OrdersController@orderDetailById');
    Route::get('order/pastordersbycustomer/{id}','v4\OrdersController@pastOrdersByCustomerId');
    Route::post('order/pastordersbytruck','v4\OrdersController@pastOrdersByTruckId');
    Route::post('order/accept','v4\OrdersController@acceptOrder');
    Route::post('order/ready','v4\OrdersController@readyOrder');
    Route::post('order/reminder','v4\OrdersController@reminderOrder');
    Route::post('order/paymentmethod','v4\OrdersController@updatePaymentMethod');
    Route::post('order/deliver','v4\OrdersController@deliverOrder');
    Route::post('order/cancel','v4\OrdersController@cancelOrder');
    Route::post('order/reject','v4\OrdersController@rejectOrder');
    Route::post('order/orderstatusnotification','v4\OrdersController@orderStatusNotification');
    Route::post('order/checkitemstock','v4\OrdersController@checkItemStock');
    Route::post('order/addservernstation','v4\OrdersController@addServernStation');


    Route::post('order/test','v4\OrdersController@testNotification');

    //category controller
    Route::resource('category','v4\CategoryController');
    Route::post('category/update','v4\CategoryController@updateCategory');
    Route::post('category/updateorder','v4\CategoryController@categoryUpdateOrder');
    Route::post('category/updatestatus','v4\CategoryController@categoryUpdateStatus');
    Route::get('allcategoriesbytruckid/{id}','v4\CategoryController@allcategoriesbytruckid');

    //Items controller 
    Route::resource('item','v4\ItemsController');
    Route::get('allitemsbycategoryid/{id}','v4\ItemsController@allItemsByCategoryId');
    Route::get('allitemsbytruckid/{id}','v4\ItemsController@allItemsByTruckId');
    Route::post('itemorder','v4\ItemsController@itemOrder');
    Route::post('item/update','v4\ItemsController@updateItem');
    Route::post('item/updateorder','v4\ItemsController@updateItemOrder');
    Route::post('item/updatestatus','v4\ItemsController@updateItemStatus');
    Route::post('item/updatestock','v4\ItemsController@updateItemInstock');


    Route::post('verifyvendorotp','v4\OtpController@verifyVendorOtp');
    Route::post('verifycustomerotp','v4\OtpController@verifyCustomerOtp');


    //Customization Type controller
    Route::get('allcustomizationsbyitemid/{id}','v4\CustomizationTypeController@allcustomizationsbyitemid');

    Route::post('customization/add','v4\CustomizationTypeController@addCustomization');
    Route::post('customization/update','v4\CustomizationTypeController@updateCustomization');
    Route::post('customization/updatestatus','v4\CustomizationTypeController@updateCustomizationStatus');
    Route::post('customization/delete','v4\CustomizationTypeController@deleteCustomization');
    Route::post('customization/moveup','v4\CustomizationTypeController@moveUpCustomization');
    Route::post('customization/movedown','v4\CustomizationTypeController@moveDownCustomization');



    //Customization Options controller
    Route::resource('customizationoption','v4\CustomizationOptionController');
    Route::post('customizationoption/update','v4\CustomizationOptionController@updateCustomizationOption');
    Route::post('customizationoption/delete','v4\CustomizationOptionController@deleteCustomizationOption');

    //Stripe
    Route::post('stripe/keys', 'v4\StripeController@getStripeKeys');
    Route::post('stripe/deleteaccount', 'v4\StripeController@deleteAccount');
    Route::post('stripe/getpaymentintent', 'v4\StripeController@getPaymentIntent');
    Route::post('stripe/getpaymentintentforterminal', 'v4\StripeController@getPaymentIntentForTerminal');
    Route::post('stripe/connectlink', 'v4\StripeController@getConnectLink');
    Route::post('stripe/diconnectstripe', 'v4\StripeController@disconnectStripeAccount');
    
    //Terminal
    Route::post('stripe/terminalconnectiontoken', 'v4\StripeController@terminalConnectionToken');
    Route::post('stripe/terminalconnectiontokenforconnect', 'v4\StripeController@terminalConnectionTokenForConnect');
    Route::post('stripe/terminalregistration', 'v4\StripeController@terminalRegistration');
    Route::post('stripe/terminalregistrationforconnect', 'v4\StripeController@terminalRegistrationForConnect');
    Route::post('stripe/capturepaymentintent', 'v4\StripeController@capturePaymentIntent');

    Route::post('stripe/allreaders', 'v4\StripeController@allReaders');

    //WebHook
    Route::get('stripe/callback/oauthauthorize', 'v4\StripeController@oauthAuthorize');
    Route::post('stripe/callback/webhook', 'v4\StripeController@webhook');


    //BROADCASTING

    Route::post('broadcast/hellocustomers', 'v4\BroadcastController@fromTruckToCustomers');

});

Route::group(['prefix' => 'v5'],function(){
    Route::get('get-customer-plan', 'v5\SubscriptionsController@getCustomerSubscriptionPlans');
    Route::post('customer-plan', 'v5\SubscriptionsController@storeCustomerSubscriptionPlans');
    Route::get('cancel-plan', 'v5\SubscriptionsController@CancelPlan');
    Route::get('invoice', 'v5\SubscriptionsController@Invoice');


    Route::get('store-vendor', 'v5\VendorRegistrationController@storeVendor');
    Route::get('email-verification-link/{token}', 'v5\VendorRegistrationController@verifyEmail')->name('email-verification-link');

    Route::post('customer-plan', 'v5\SubscriptionsController@storeCustomerSubscriptionPlans');
    Route::get('cancel-plan', 'v5\SubscriptionsController@CancelPlan');
    Route::get('invoice', 'v5\SubscriptionsController@Invoice');
});