<?php

namespace App\Jobs\StripWebhooks;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Models\WebhookCall;
use App\Models\Subscriptions;
use App\Models\SubscriptionPayment;
use App\Models\truck_subscription;
use Log;
class ChargeSucceeded implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;
    
    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        try{
            $charge = $this->webhookCall->payload['data']['object'];
            $vendor = truck_subscription::where('stripe_id', $charge['customer'])->first();
            if($vendor){
                SubscriptionPayment::create([
                    'truck_subscription_id'=> 1,
                    'name'=> $charge['billing_details']['name'],
                ]);
            }
        }catch(Exception $ex) {
            Log::error("Stripe->weebhook-> ".$ex->getMessage());
        }
        // try{
        //     $event = \Stripe\Event::constructFrom(
        //         json_decode($payload, true)
        //     );

        //     // Handle the event
        //     switch ($event->type) {
        //         case 'account.updated':
        //                 Log::error('account.updated');
        //                 Log::error($event);
        //         case 'application_fee.created':
        //                 $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent

        //                 $orderPayment = new order_payment;

        //                 $orderPayment->order_id             = 10000;
        //                 $orderPayment->payment_intent_id    = $event->data->object->charge;
        //                 //$orderPayment->payment_method_id    = $event->data->object->payment_method;
        //                 //$orderPayment->receipt_url          = $event->data->object->charges->data[0]['receipt_url'];
        //                 $orderPayment->payment_json_object  = $event;

        //                 $orderPayment->save();
        //             break;
        //         case 'payment_intent.succeeded':
        //                 $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent

        //                 $orderPayment = new order_payment;

        //                 $orderPayment->order_id             = $event->data->object->metadata->order_id;
        //                 $orderPayment->payment_intent_id    = $event->data->object->id;
        //                 $orderPayment->payment_method_id    = $event->data->object->payment_method;
        //                 $orderPayment->receipt_url          = $event->data->object->charges->data[0]['receipt_url'];
        //                 $orderPayment->payment_json_object  = $event;

        //                 $orderPayment->save();
        //             break;
        //         case 'payment_method.payment_failed':
        //         return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Payment did not go through, Please try after sometime.');
                    
        //             break;
        //         // ... handle other event types
        //         default:
        //             // Unexpected event type
        //             exit();
        //     }

        //     return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'paymentIntent webhook callback success');

        // } catch(Exception $ex) {
        //     Log::error("Stripe->weebhook-> ".$ex->getMessage());
        //     return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        // }

    }
}
