<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class truck extends Model
{
    public function vendors()
    {
    	return $this->belongsTo(vendor::class);
    }

    public function categories()
    {
    	return $this->hasMany(category::class);
    }

    public function orders()
    {
    	return $this->hasMany(order::class);
    }
}
