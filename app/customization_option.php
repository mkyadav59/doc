<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customization_option extends Model
{
    public function customization_types()
    {
    	return $this->belongsTo(customization_type::class);
    }

}
