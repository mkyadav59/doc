<?php

namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, Image;
use App\vendor, App\truck, App\item, App\customer, App\order, App\category, App\truckdevicetoken, App\customer_truck, App\version;


class TrucksController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'vendor_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'phone' => 'required|digits:10',
                'datetime' => 'required'

            );
                


            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $truck = new truck;
            $truck->vendor_id = (int) $arrData['vendor_id'];
            $truck->name = $arrData['name'];

            $imgType = '';
            
            if(!empty($request->file('logo'))){
                $image = $request->file('logo') ;
                
                $guessExtension = $image->guessExtension();
                $fileName = time().'.'.$guessExtension;
                $path = public_path().'/uploads/logo/'.$fileName;
                //$file->move($destinationPath,$fileName);
                $image = Image::make($image->getRealPath())->resize(400, 300)->save(sprintf($path));
                
                $truck->logo =  $fileName;
            }

			if(isset($arrData['latitude']))
				$truck->latitude = $arrData['latitude'];
            
			if(isset($arrData['longitude']))
			$truck->longitude = $arrData['longitude'];

            $truck->description = $arrData['description'];
            $truck->phone = $arrData['phone'];
            $truck->weekdaytime = $arrData['FromWeekday'].' TO '.$arrData['ToWeekday'];
            $truck->weekendtime = $arrData['FromWeekend'].' TO '.$arrData['ToWeekend'];
            $truck->active = (int) 1;
            $truck->created_at = $arrData['datetime'];
            $truck->updated_at = $arrData['datetime'];
            
            $truck->save();

            if($truck->id > 0){

				$this->createDummyCategories($truck->id);

                $trucks = DB::table('trucks')->where(['id'=> $truck->id, 'active' => 1])->get()->first();
                
                $trucks_weekdaytime = $trucks->weekdaytime; 
                $weekdaytime = explode(' ', $trucks_weekdaytime);

                $trucks_weekendtime = $trucks->weekendtime; 
                $weekendtime = explode(' ', $trucks_weekendtime);

                $trucks->FromWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[0] : $trucks_weekdaytime;
                $trucks->ToWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[2] : $trucks_weekdaytime;
                $trucks->FromWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[0] : $trucks_weekendtime;
                $trucks->ToWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[2] : $trucks_weekendtime;

                $trucks->id = (int) $trucks->id;
                $trucks->logo = $truck->logo;
                $trucks->vendor_id = (int) $trucks->vendor_id;
                $trucks->active = (int) $trucks->active;
                $trucks->rating = (int) $trucks->rating;

                
                return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message = 'Truck added successfully..');
            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading truck');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    
    }



	public function createDummyCategories($id)
		{
		
			if($id > 0)
			{
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Burgers";
				$category2->short_description = "burgers";
				$category2->description = "burgers";
				$category2->order = '1';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Pizzas";
				$category2->short_description = "pizzas";
				$category2->description = "pizzas";
				$category2->order = '2';
				$category2->active = '1';
				$category2->save();


				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Salads";
				$category2->short_description = "salads";
				$category2->description = "salads";
				$category2->order = '3';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Sandwiches";
				$category2->short_description = "sandwiches";
				$category2->description = "sandwiches";
				$category2->order = '4';
				$category2->active = '1';
				$category2->save();

				
				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Tachos";
				$category2->short_description = "tachos";
				$category2->description = "tachos";
				$category2->order = '5';
				$category2->active = '1';
				$category2->save();

				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Sides";
				$category2->short_description = "sides";
				$category2->description = "sides";
				$category2->order = '6';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Desserts";
				$category2->short_description = "desserts";
				$category2->description = "desserts";
				$category2->order = '7';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Snacks";
				$category2->short_description = "snacks";
				$category2->description = "snacks";
				$category2->order = '8';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Drinks";
				$category2->short_description = "drinks";
				$category2->description = "drinks";
				$category2->order = '9';
				$category2->active = '1';
				$category2->save();
			}

	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $truck = truck::find($id);

            if(!empty($truck)){
                $trucks = DB::table('trucks')->where(['id'=> $id])->get()->first();
                
                $trucks_weekdaytime = $trucks->weekdaytime; 
                $weekdaytime = explode(' ', $trucks_weekdaytime);

                $trucks_weekendtime = $trucks->weekendtime; 
                $weekendtime = explode(' ', $trucks_weekendtime);

                $trucks->FromWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[0] : $trucks_weekdaytime;
                $trucks->ToWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[2] : $trucks_weekdaytime;
                $trucks->FromWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[0] : $trucks_weekendtime;
                $trucks->ToWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[2] : $trucks_weekendtime;
                
                $trucks->id = (int) $trucks->id;
                $trucks->vendor_id = (int) $trucks->vendor_id;
                $trucks->logo =  $trucks->logo;
                $trucks->active = (int) $trucks->active;
                $trucks->rating = (int) $trucks->rating;


                return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message = 'Truck details');
            }else{
                 throw new Exception('invalid vendor id');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }



    public function updateTruck(Request $request)
    {   //echo "<pre>"; print_r($request->all());exit();
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'vendor_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'phone' => 'required',
                'datetime' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $truck = truck::find((int) $arrData['truck_id']);
            $truck->name = $arrData['name'];
            $truck->vendor_id = (int)$arrData['vendor_id'];
            $truck->latitude = $arrData['latitude'];
            $truck->longitude = $arrData['longitude'];
            $truck->description = $arrData['description'];
            $truck->phone = $arrData['phone'];
            $truck->weekdaytime = $arrData['FromWeekday'].' TO '.$arrData['ToWeekday'];
            $truck->weekendtime = $arrData['FromWeekend'].' TO '.$arrData['ToWeekend'];
            $truck->updated_at = $arrData['datetime'];

            if(!empty($request->file('logo'))){
                $image = $request->file('logo') ;
                
                $guessExtension = $image->guessExtension();
                $fileName = time().'.'.$guessExtension;
                $path = public_path().'/uploads/logo/'.$fileName;
                //$file->move($destinationPath,$fileName);
                $image = Image::make($image->getRealPath())->resize(400, 300)->save(sprintf($path));
                
                $truck->logo =  $fileName;
            }

            $truck->save();

            if($truck->id > 0){

                $trucks = DB::table('trucks')->where(['id'=> $truck->id])->get()->first();
                
                $trucks_weekdaytime = $trucks->weekdaytime; 
                $weekdaytime = explode(' ', $trucks_weekdaytime);

                $trucks_weekendtime = $trucks->weekendtime; 
                $weekendtime = explode(' ', $trucks_weekendtime);

                $trucks->FromWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[0] : $trucks_weekdaytime;
                $trucks->ToWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[2] : $trucks_weekdaytime;
                $trucks->FromWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[0] : $trucks_weekendtime;
                $trucks->ToWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[2] : $trucks_weekendtime;

                $trucks->id = (int) $trucks->id;
                $trucks->vendor_id = (int) $trucks->vendor_id;
                $trucks->logo =  $truck->logo;
                $trucks->active = (int) $trucks->active;
                $trucks->rating = (int) $trucks->rating;


                return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message = 'Truck updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading truck');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    //gettrucks
    public function getTrucksByRadious(Request $request)
    {
        try 
        {
			/*
				DROP PROCEDURE `trucksbyradious`;
				CREATE DEFINER=`root`@`localhost` PROCEDURE `trucksbyradious`(IN `v_latitude` VARCHAR(25), IN `v_longitude` VARCHAR(25), IN `v_radious` INT, IN `v_logo_url` VARCHAR(255)) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT
				id,
				name,
				address,
				phone,
				description,
				CONCAT( v_logo_url , logo) as logo,
				latitude,
				longitude,
				 ( 3959 * acos( cos( radians(v_latitude ) ) * cos( radians( latitude ) ) 
					* cos( radians( longitude ) - radians( v_longitude) ) + sin( radians(v_latitude ) ) * sin(radians(latitude)) ) ) AS distance 
				FROM trucks
				Where active = 0 OR active = 1 
				HAVING distance < v_radious
				ORDER BY distance 
          */
          
            $arrData = $request->all();
            $rules = array(
                'latitude' => 'required',
                'longitude' => 'required',
                'radious' => 'required',
                'logo_url' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            //$truck = DB::statement("call trucksbyradious(?,?,?,?)",$arrParams);
            $trucks['trucks'] = DB::select("CALL trucksbyradious('".$arrData['latitude']."','".$arrData['longitude']."', ".$arrData['radious'].", '".$arrData['logo_url']."' )");

            	/*
                $tr["id"] = 5;
                $tr["name"] = "Pi on the Spot";
                $tr["address"] = null;
                $tr["phone"] = "5023145005";
                $tr["description"] = "TGIFood Truck! Stop by and grab some Pi.\nhttp://pi-pizza.com/company";
                $tr["logo"] = "https://api.grilledchili.com/uploads/logo/1560218147.jpeg";
                $tr["latitude"] = $arrData['latitude'];
                $tr["longitude"] = $arrData['longitude'];
                $tr["distance"] = 0.60559170659645;

                $trucks['trucks'][0] = $tr;
                */
           
           if(!empty($trucks['trucks'])){

			//favoirte trucks
			/*
			 if(!empty($arrData['customer_id'])){
				 $favouriteTrucks = customer::find($arrData['customer_id'])->trucks()->get(['trucks.id','trucks.name', 'trucks.address', 'trucks.phone', 'trucks.description', DB::raw("CONCAT( '".$arrData['logo_url']."' , trucks.logo) as logo"), 'trucks.latitude', 'trucks.longitude' ])->toArray();
				
				if(!empty( $favouriteTrucks)){
					 $trucks['trucks'] = array_merge($trucks['trucks'],$favouriteTrucks);
				}
			 }
			*/
			//echo "<pre>"; print_r($trucks); exit;
                return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message = 'Trucks within '.$arrData['radious'].' miles radious');
            }else{
                 throw new Exception('Nearby No trucks found. Check your favourite');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }



    //url - gettruckdetail
    public function getTruckDetailsWithItems(Request $request)
    {
        try 
        {   
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = DB::table('trucks')->where(['id'=> $request->truck_id])->get();
            //echo "<pre>"; print_r($truck); exit();
            if($request->customer_id != null){
                $arrData = DB::table('customer_truck')->where(['customer_id' => $request->customer_id, 'truck_id' => $request->truck_id, 'favourite' => 1])->get();

                //echo "<pre>"; print_r($arrData); exit();
                
                if(!empty($arrData)){
                    $truck[0]->favourite = 1;
                }else{
                    $truck[0]->favourite = 0;
                }
            }else{
                    $truck[0]->favourite = 0;
            }

            if($truck[0]->logo != null)
                $truck[0]->logo =  url('/').'/uploads/logo/'.$truck[0]->logo;
            else
                $truck[0]->logo =  url('/').'/uploads/logo/logo_default.jpg';

            $resData['truck'] = $truck;
		
            if(!empty($truck))
            {

                /*$arrCategory = $category = DB::table('categories')->where(['truck_id'=> $request->truck_id, 'active' => 1])->orderBy('order')->get();

                $resData['category'] = $arrCategory;
                //echo "<pre>"; print_r($arrCategory); exit();
                $items = DB::table('items')
                        ->where(['truck_id'=>  $request->truck_id, 'active' =>  1])
                        ->get();

                foreach ($items as $item) {
                    if($item->img != NULL)
                        $item->img =  url('/').'/uploads/items/'.$item->img;
                    //else
                        //$item->img =  url('/').'/uploads/items/item_default.jpg';
                }

                $resData['items'] = $items;*/

                $arrCategory = $category = DB::table('categories')->where(['truck_id'=> $request->truck_id, 'active' => 1])->orderBy('order')->get(['id', 'name']);
                $items = DB::table('items')
                        ->where(['truck_id'=>  $request->truck_id, 'active' =>  1])
                        ->orderBy('order')
                        ->get();

                $arrCat = array();
                $arrItems = array();
                foreach ($arrCategory as $cat) {
                    $arrCat['categoryName'] = $cat->name;
                    $arrCat['items'] = [];
                    foreach ($items as $i) {
                        if($i->category_id == $cat->id){

                            if($i->img != NULL){
                                $i->img =  url('/').'/uploads/items/'.$i->img;
                            }
                            else{
                                $i->img =  url('/').'/uploads/items/item_default.jpg';
                            }

                            $arrCat['items'][] = $i;        
                        }
                    }
                    $resData['category'][] = $arrCat;

                }

                

                if($request->customer_id){
                    $order = customer::find($request->customer_id)->orders()->where(['truck_id' => $request->truck_id,'order_placed' => 0, 'order_cancelled' => 0, 'order_rejected' => 0])->get(['id']);
                    if(!empty($order)){
                        $resData['order'] = $order;
                    }else{

                        $order = new \stdClass;
                        $order->id = 0;
                        $a[] = $order;
                        $resData['order'] = $a;
                    }
                }else{
                        $order = new \stdClass;
                        $order->id = 0;
                        $a[] = $order;
                        $resData['order'] = $a;
                }

                return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'Truck details');
            }else{
                 throw new Exception('invalid truck request');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function setTruckLocation(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'latitude' => 'required',
                'longitude' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $truck = truck::find((int) $arrData['truck_id']);
            $truck->latitude = $arrData['latitude'];
            $truck->longitude = $arrData['longitude'];

            $truck->save();

            if($truck->id > 0){
                $val = new \stdClass();
                $val->truck_id = (int) $truck->id;
                return $response = $this->responseData($data = $val,$status = true ,$code = '200',$message = 'Truck location updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while updating truck location.');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function setTruckOnOff(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            $truck = truck::find((int) $arrData['truck_id']);
            $truck->active = $arrData['active'];

            $truck->save();

            if($truck->id > 0){
                $val = new \stdClass();
                $val->truck_id = (int) $truck->id;

				if($truck->active == 1)
					return $response = $this->responseData($data = $val,$status = true ,$code = '200',$message = 'Now you can accept the orders.');
				else
					return $response = $this->responseData($data = $val,$status = true ,$code = '200',$message = 'No order will be processed until you turn it on.');
            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while updating truck status.');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function storeDeviceToken(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'app_type' => 'required',
                'device_type' => 'required',
                'device_token' => 'required',
                //'hash' => 'required', FIRST TIME IT WILL BE NULL
                'datetime' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            if(!empty($arrData['hash'])){

                $arrTruckData = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'hash' => $arrData['hash']])->get(['id', 'active']);

                if(!empty($arrTruckData)){

                    if($arrTruckData[0]->active == 1){

                        $version = DB::table('versions')->where(['type' => $arrData['app_type'], 'device_type' => $arrData['device_type'] ])->orderBy('id', 'desc')->first(['version','mandatory']);

                        $dc = truckdevicetoken::find($arrTruckData[0]->id);

                        $dc->truck_id = $arrData['truck_id'];
                        $dc->device_type = $arrData['device_type'];
                        $dc->token = $arrData['device_token'];
                        $dc->updated_at = $arrData['datetime'];

                        $dc->save();

                        $data['device_token'] = $dc;
                        $data['version'] = $version;

                        return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token updated.');
                    }else{
                        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Your session got expired, Please login again.");  
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Hash not found.");   
                }
            }
            else
            {

                //responsible to delete all previouse entries of same device truckdevicetokens
                $tc = truckdevicetoken::where('token', $arrData['device_token'])->delete();


                $dc = new truckdevicetoken; 

                $dc->truck_id = $arrData['truck_id'];
                $dc->device_type = $arrData['device_type'];
                $dc->token = $arrData['device_token'];
                $dc->hash = md5($arrData['datetime']);
                $dc->created_at = $arrData['datetime'];
                $dc->updated_at = $arrData['datetime'];
                $dc->save();
                
                $data['device_token'] = $dc;
                $data['version'] = new \stdClass;

                return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token added.');

            }
        }
        catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }

}
