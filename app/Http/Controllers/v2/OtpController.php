<?php

namespace App\V2;
namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Validator, Exception;
use App\otp, App\vendor, App\truck;


class OtpController extends BaseController
{
    public function sendOtp($entity_id, $type)
    {
         //send otp
        $otp = new otp;
        $otp->otp = rand(1111,9999);
        $otp->entity_id = $entity_id;
        $otp->type = $type;

        $otp->save();

        return $otp->otp;
    }

    public function verifyVendorOtp(Request $request)
    {
        try {
            $arrData = $request->all();

            $rules = array(
            'vendor_id' => 'required',
            'otp' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
             if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $otp = otp::where(['entity_id'=>$arrData['vendor_id'], 'otp'=>$arrData['otp'], 'type'=> 'vendor'])->get(['otp'])->last();
            
            if(!empty($otp)){

                //Delete all previous OTP
                $otp = otp::where(['entity_id'=>$arrData['vendor_id'], 'type'=> 'vendor'])->delete();

                //registration validation
                $vendor = vendor::where(['id' => $arrData['vendor_id'], 'active' => -1 ])->get()->first();

                if(!empty($vendor)){
                    $vendor->active = 1;
                    $vendor->save();
                }

                //get vendor details and truck listing
                $vendor =  DB::table('vendors')->where(['id' => $arrData['vendor_id'], 'active'=> 1] )->get()->first();
                
                if(!empty($vendor)){

                    $vendor->id = (int) $vendor->id;
                    $vendor->active = (int) $vendor->active; 


                    $resData['vendor'] = $vendor;

                    $trucks = truck::where(['vendor_id'=> $vendor->id])->get(['id','name','logo']);
                    $resData['trucks'] = $trucks;
					
					$config['logo_img_url'] = url('/').'/uploads/logo/';
					$config['item_img_url'] = url('/').'/uploads/items/';
                    $config['location_update_interval'] = 30;

					$resData['config'] = $config;;

                    if(!empty($vendor)){
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Otp is valid');
                    }else{
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'No trucks registered.');
                    }
                }else{
                    throw new Exception('Your account has been suspended.');
                }

                
            }else{
                 throw new Exception('Invalid otp.');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function verifyCustomerOtp(Request $request)
    {
        try {
            $arrData = $request->all();

            $rules = array(
            'id' => 'required',
            'otp' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
             if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $otp = otp::where(['entity_id'=>$arrData['id'], 'otp'=>$arrData['otp'], 'type'=> 'customer'])->latest()->get(['otp']);
			//echo "<pre>";  print_r( $arrData); print_r( $otp); exit;
            if(!empty($otp)){

                //delete previous otp 
                $otp = otp::where(['entity_id'=>$arrData['id'], 'type'=> 'customer'])->delete();
                
                //get customer details and truck listing
                $customer =  DB::table('customers')->where(['id' => $arrData['id'], 'active'=> 1] )->get()->first();
                
                if(!empty($customer)){

                    $resData['customer'] = $customer;

                  
                    if(!empty($customer)){
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Otp is valid');
                    }else{
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'No trucks registered.');
                    }
                }else{
                    throw new Exception('Your account has been suspended.');
                }

                
            }else{
                 throw new Exception('Invalid otp.');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }
}
