<?php

namespace Api\V2;
namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator;
use App\vendor, App\truck, App\otp;

class VendorsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'invalid action');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'invalid action');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $arrData = $request->all();
        //check if only mobile number provided for login or all data for registraion
        if(!empty($arrData['name']) && !empty($arrData['email'])){

            $rules = array(
                'name' => 'required',
                'mobile' => 'required|digits:10',
                'email' => 'required|email',
                'country_code' => 'required|numeric',
                'datetime' => 'required'
            );

            
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $rules = array(
                'mobile' => 'required|unique:vendors'
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Mobile number is already registered.");
            }

            $vendor = vendor::where(['mobile' => $arrData['mobile'], 'active' => 1] )->get(['id', 'firstname'])->first();
            //return $vendor;
            if(!empty($vendor)){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Mobile number is already registered.");
            }


            //split name if firstname lastname provided 
            $name = explode(' ', $arrData['name']);
            $firstname = (count($name) > 1) ? $name[0] :  $arrData['name'];
            $lastname = (count($name) > 1) ? $name[1] :  '';
            

            //insert in to database
            $vendor = new vendor;
            $vendor->firstname = $firstname;
            $vendor->lastname = $lastname;
            $vendor->mobile = $arrData['mobile'];
            $vendor->email = $arrData['email'];
            $vendor->country_code = $arrData['country_code'];
            $vendor->created_at = $arrData['datetime'];
            $vendor->updated_at = $arrData['datetime'];
            $vendor->active = -1;
            
            $vendor->save();


            $vendor = vendor::where(['mobile' => $arrData['mobile'], 'active' => -1] )->get(['id', 'firstname'])->last();
            //return $vendor;
            if(!empty($vendor)){
                $otp = (new OtpController)->sendOtp($vendor->id, 'vendor');
                $vendor->otp = $otp;


                $mobile_number = $arrData['mobile'];            //send SMS
                $message = "Your One-Time Password(OTP) is ".$vendor->otp." for VTM. Please do not share this password it with anyone - GrilledChili";

                $url = "https://control.msg91.com/api/sendhttp.php?authkey=162659ADcVnvKop59500dbf&mobiles=".$arrData['mobile']."&message=".$message."&sender=GCHILI&route=4&country=".$arrData['country_code'];
                file_get_contents($url);

            }else{
                 return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Please check mobile number or do registration');
            }

        }else{
            $rules = array(
                'mobile' => 'required|digits:10',
                'country_code' => 'required|numeric'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            $vendor = vendor::where(['mobile' => $arrData['mobile'], 'active' => 1] )->get(['id', 'firstname'])->first();
            //return $vendor;
            if(!empty($vendor)){
                $otp = (new OtpController)->sendOtp($vendor->id, 'vendor');
                $vendor->otp = $otp;


                $mobile_number = $arrData['mobile'];            //send SMS
                $message = "Your One-Time Password(OTP) is ".$vendor->otp." for VTM. Please do not share this password it with anyone - GrilledChili";

                $url =  "https://control.msg91.com/api/sendhttp.php?authkey=162659ADcVnvKop59500dbf&mobiles=".$arrData['mobile']."&message=".$message."&sender=GCHILI&route=4&country=".$arrData['country_code'];
                file_get_contents($url);

            }else{
                 return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Please check mobile number or do registration');
            }
        }

        return $response = $this->responseData($data = $vendor, $status = true ,$code = '200',$message = 'Hey '.$vendor->firstname. ', an otp has been sent to this number.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $vendor = DB::table('vendors')->where(['id' => $id, 'active'=> 1] )->get();

                if(!empty($vendor)){
                    $vendor = $vendor[0];

                    $vendor->id = (int) $vendor->id;
                    $vendor->active = (int) $vendor->active;

                    $resData['vendor'] = $vendor;

                    $trucks = truck::where(['vendor_id'=> $vendor->id, 'active' => 1])->get(['id','name','logo']);
                    $resData['trucks'] = $trucks;

                    if(!empty($trucks)){
                        return $response = $this->responseData($date = $vendor, $status = true ,$code = '200',$message = 'Vendor details.');
                    }else{
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'No trucks registered.');
                    }
                }else{
                    throw new Exception('Your account has been suspended.');
                }


            if(!empty($vendor)){
                return $response = $this->responseData($data = $arrResponse,$status = true ,$code = '200',$message = 'Vendor Details');
            }else{
                 throw new Exception('invalid vendor id');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'invalid action');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'invalid action');
    }




    public function updateVendor(Request $request)
    {
       try
        {

            $arrData = $request->all();
            $rules = array(
                'id' => 'required',
                'name' => 'required',
                'email' => 'email',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            //split name if firstname lastname provided 
            $name = explode(' ', $arrData['name']);
            $firstname = (count($name) > 1) ? $name[0] :  $arrData['name'];
            $lastname = (count($name) > 1) ? $name[1] :  '';
            
            //insert in to database
            $vendor = vendor::find($arrData['id']);
            if(!empty($vendor)){
                $vendor->firstname = $firstname;
                $vendor->lastname = $lastname;
                $vendor->email = (!empty($arrData['email']) && $arrData['email'] != '') ? $arrData['email'] : null;
                $vendor->latitude = (!empty($arrData['latitude']) && $arrData['latitude'] != '') ? $arrData['latitude'] : null;
                $vendor->longitude = (!empty($arrData['longitude']) && $arrData['longitude'] != '') ? $arrData['longitude'] : null;
                $vendor->save();
            }
            else{
                 throw new Exception('invalid vendor id');
            }

            return $response = $this->responseData($data = $vendor,$status = true ,$code = '200',$message = 'Proceed');

        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function GcmLogin(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'mobile' => 'required|digits:10'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            $vendor = vendor::where(['mobile' => $arrData['mobile'], 'active' => 1] )->get(['id', 'firstname'])->first();
            //return $vendor;
            if(!empty($vendor)){
                $otp = (new OtpController)->sendOtp($vendor->id, 'vendor');
                $vendor->otp = $otp;


            }else{
                 return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Please check mobile number or do registration');
            }

            return $response = $this->responseData($data = $vendor, $status = true ,$code = '200',$message = 'Hey '.$vendor->firstname. ', an otp has been sent to this number.');


        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 

    }

}
