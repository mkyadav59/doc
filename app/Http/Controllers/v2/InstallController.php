<?php
namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator;
use App\vendor, App\category;

class InstallController extends BaseController
{
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function vendorConfig($version='')
    {
    	try {
            phpinfo();exit();
            
    		if($version == '')//consdered default values
    		{
	        	$resData['logo_img_url'] = 'http://app.sudikmaharana.in/uploads/logo/';
	        	$resData['item_img_url'] = 'http://app.sudikmaharana.in/uploads/items/';
	        	 
	            if(!empty($resData)){
	                return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Vendor application settings.');
	            }else{
	                return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Something went wrong please try again later..');
	            }
	        }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function customerConfig($version='')
    {
    	try {
    		if($version == '')//consdered default values
    		{
	        	$resData['logo_img_url'] = 'http://localhost:8000/uploads/logo/';
	        	$resData['item_img_url'] = 'http://localhost:8000/uploads/items/';
	        	 
	            if(!empty($resData)){
	                return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Customer application settings.');
	            }else{
	                return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Something went wrong please try again later..');
	            }
	        }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }
}