<?php
 
namespace Api\v1;
namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator;
use App\customer, App\truck, App\otp, App\customer_truck, App\token, App\customerdevicetoken;

class CustomersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'invalid action');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'invalid action');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrData = $request->all();
        
        $rules = array(
            'name' => 'required',
            'mobile' => 'required',
            'country_code' => 'required'
        );
        $validator = Validator::make($arrData,$rules);
        if($validator->fails()){
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
        }
        //split name if firstname lastname provided 
        $name = explode(' ', $arrData['name']);
        $firstname = (count($name) > 1) ? $name[0] :  $arrData['name'];
        $lastname = (count($name) > 1) ? $name[1] :  '';
        
        $regCustomer = customer::where(['mobile' => $arrData['mobile'], 'active' => 1] )->get(['id', 'firstname']);
        
        //if customer already registered;
        if(!empty($regCustomer)){
            //update customer name
            $customer = customer::find($regCustomer[0]['id']);
            $customer->firstname = $firstname;
            $customer->lastname = $lastname;
            $customer->mobile = $arrData['mobile'];
            $customer->country_code = $arrData['country_code'];

            $customer->save();

        }else{
            //insert in to database
            $customer = new customer;
            $customer->firstname = $firstname;
            $customer->lastname = $lastname;
            $customer->mobile = $arrData['mobile'];
            $customer->country_code = $arrData['country_code'];
            $customer->active = 1;
            
            $customer->save();
        }

        // send otp
        if(!empty($customer)){
            $resData['id'] = $customer->id;
            $resData['otp'] = (new OtpController)->sendOtp($customer->id, 'customer');


            $mobile_number = $arrData['mobile'];            //send SMS
            $message = "Your One-Time Password(OTP) is ".$resData['otp']." for VTM. Please do not share this password with anyone - GrilledChili";

            $url =  "https://control.msg91.com/api/sendhttp.php?authkey=162659ADcVnvKop59500dbf&mobiles=".$arrData['mobile']."&message=".$message."&sender=GCHILI&route=4&country=".$arrData['country_code'];
            json_decode(file_get_contents($url), true);

        }else{
             return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Please check mobile number');
        }

        
        return $response = $this->responseData($data = $resData, $status = true ,$code = '200',$message = 'Hello '.$customer->firstname. ', an otp has been send to this number.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $customer = DB::table('customers')->where(['id' => $id, 'active'=> 1] )->get();

                if(!empty($customer)){
                    $customer = $customer[0];
                    $resData['customer'] = $customer;

                    $trucks = truck::where(['customer_id'=> $customer->id, 'active' => 1])->get(['id','name','logo']);
                    $resData['trucks'] = $trucks;

                    if(!empty($trucks)){
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Vendor details.');
                    }else{
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'No trucks registered.');
                    }
                }else{
                    throw new Exception('Your account has been suspended.');
                }


            if(!empty($customer)){
                return $response = $this->responseData($data = $arrResponse,$status = true ,$code = '200',$message = 'Vendor Details');
            }else{
                 throw new Exception('invalid customer id');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return '3';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }



    public function updateCustomer(Request $request)
    {
        try
        {

            $arrData = $request->all();
            $rules = array(
                'id' => 'required',
                'name' => 'required',
                'email' => 'email'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            //split name if firstname lastname provided 
            $name = explode(' ', $arrData['name']);
            $firstname = (count($name) > 1) ? $name[0] :  $arrData['name'];
            $lastname = (count($name) > 1) ? $name[1] :  '';
            
            //insert in to database
            $customer = customer::find($arrData['id']);
            if(!empty($customer)){
                $customer->firstname = $firstname;
                $customer->lastname = $lastname;
                $customer->email = (!empty($arrData['email']) && $arrData['email'] != '') ? $arrData['email'] : null;
                $customer->latitude = (!empty($arrData['latitude']) && $arrData['latitude'] != '') ? $arrData['latitude'] : null;
                $customer->longitude = (!empty($arrData['longitude']) && $arrData['longitude'] != '') ? $arrData['longitude'] : null;
                $customer->save();
            }
            else{
                 throw new Exception('invalid customer id');
            }

            return $response = $this->responseData($data = $customer,$status = true ,$code = '200',$message = 'Proceed');

        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    //set truck as favourite

    public function setFavouriteTruck(Request $request)
    {
        try
        {

            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
				'favourite' => 'required'
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            if(isset($arrData['favourite']) && $arrData['favourite'] == 1 ) {
            
				$ct = new customer_truck; 

                $ct->customer_id = $arrData['customer_id'];
                $ct->truck_id = $arrData['truck_id'];
                $ct->save();
                
                return $response = $this->responseData($data = $ct, $status = true ,$code = '200',$message = 'truck added as favourite');
            }else{

                $ct = customer_truck::where(['customer_id' => $arrData['customer_id'], 'truck_id' => $arrData['truck_id'], 'favourite' => 1])->first();

				if(!empty($ct)){
					$ctt = customer_truck::find($ct['id']);
					$ctt->favourite = 0;
					$ctt->customer_id = $arrData['customer_id'];
					$ctt->truck_id = $arrData['truck_id'];
					$ctt->save();
				
					return $response = $this->responseData($data = $ctt, $status = true ,$code = '200',$message = 'truck deleted from your favourite list.');
				}
				else{
					 return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Truck is not in favourite list.');
				}
            }
        }
        catch (Exception $ex) {
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }



    public function storeDeviceToken(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'app_type' => 'required',
                'device_type' => 'required',
                'device_token' => 'required',
                //'hash' => 'required', FIRST TIME THIS WILL BE NULL
                'datetime' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            if(!empty($arrData['hash'])){

                $arrTruckData = DB::table('customerdevicetokens')->where(['customer_id' => $arrData['customer_id'], 'hash' => $arrData['hash']])->get(['id', 'active']);

                if(!empty($arrTruckData)){

                    if($arrTruckData[0]->active == 1){

                        $version = DB::table('versions')->where(['type' => $arrData['app_type'], 'device_type' => $arrData['device_type'] ])->orderBy('id', 'desc')->first(['version','mandatory']);

                        $dc = customerdevicetoken::find($arrTruckData[0]->id);

                        $dc->customer_id = $arrData['customer_id'];
                        $dc->device_type = $arrData['device_type'];
                        $dc->token = $arrData['device_token'];
                        $dc->updated_at = $arrData['datetime'];

                        $dc->save();

                        $data['device_token'] = $dc;
                        $data['version'] = $version;

                        return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token updated.');
                    }else{
                        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Your session got expired, Please login again.");  
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Hash not found.");   
                }
            }
            else
            {
                $tc = customerdevicetoken::where('customer_id', $arrData['customer_id'] )->delete();

                $dc = new customerdevicetoken; 

                $dc->customer_id = $arrData['customer_id'];
                $dc->device_type = $arrData['device_type'];
                $dc->token = $arrData['device_token'];
                $dc->hash = md5($arrData['datetime']);
                $dc->created_at = $arrData['datetime'];
                $dc->updated_at = $arrData['datetime'];
                $dc->save();
                
                $data['device_token'] = $dc;
                $data['version'] = new \stdClass;

                return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token added.');

            }
        }
        catch (Exception $ex) {
        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 

    }


}
