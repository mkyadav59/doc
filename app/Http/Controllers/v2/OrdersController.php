<?php

namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, DateTime;
use App\vendor, App\truck, App\item, App\customer, App\order, App\item_order, App\token;
use App\customerdevicetoken,  App\truckdevicetoken;

use Edujugon\PushNotification\PushNotification;

class OrdersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $order = DB::table('item_order')->where(['order_id' => 1])->get(['item_id','quantity', 'price']);
        // return $order;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function addToCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
                'order_id' => 'required',
                'item_id' => 'required',
                'quantity' => 'required',
                'price' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            //check if desired quantity is available in stock or not 
            // $stockStatus = item::find($arrData['item_id']);
            // if($stockStatus->instock >= $arrData['quantity'])
            // {   
            //     //$stockStatus->instock = $stockStatus->instock - $arrData['quantity'];

            //     //$stockStatus->save();
            // }
            // else{
            //     throw new Exception('only '.$stockStatus->instock.' available in stock.');
            // }
            $truck = DB::table('trucks')->where(['id'=> $request->truck_id, 'active' => 1])->get(['id']);
            if(!empty($truck) > 0){

                if($arrData['order_id'] == 0){
                    //insert in to database
                    $order = new order;
                    $order->customer_id = $arrData['customer_id'];
                    $order->truck_id = $arrData['truck_id'];
                    $order->pick_up_time = 0;
                    $order->total = 0;
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->save();

                    $arrData['order_id'] = $order->id;
                }


                if($arrData['order_id'] > 0){

                    //check if item is already exists 
                    $itemOrder = item_order::where(['order_id' => $arrData['order_id'], 'item_id' => $arrData['item_id'] ])->get(['id']);
                   
                    if(!empty($itemOrder) > 0){
                        

                        $updateItem = item_order::find($itemOrder[0]['id']);

                        //update quantity else delete from cart
                        if($arrData['quantity'] > 0){
                            $updateItem->quantity = $arrData['quantity'];
                            $updateItem->price = $arrData['price'];
                            
                            if(!empty($arrData['note']))
                                $updateItem->note = $arrData['note'];

                            $updateItem->save();
                        }else{
                            $updateItem->delete();
                        }

                    }
                    else{
                        $item_order = new item_order;
                        $item_order->order_id = $arrData['order_id'];
                        $item_order->item_id = $arrData['item_id'];
                        $item_order->quantity = $arrData['quantity'];
                        $item_order->price = $arrData['price'];
                        $item_order->created_at = $arrData['datetime'];
                        $item_order->updated_at = $arrData['datetime'];
                        
                        if(!empty($arrData['note']))
                            $item_order->note = $arrData['note'];

                        $item_order->save();

                    }
                    
                    /*
                    //update order amount 
                    $amount = item_order::where(['order_id' => $arrData['order_id']])
                        ->get( 
                            array(
                                DB::raw('sum(price * quantity) AS total')
                            )
                        );

                    $updateOrder = order::find($arrData['order_id']);
                    $updateOrder->total = $amount[0]->total;

                    $updateOrder->save();
                    */

                    $updateOrder = order::find($arrData['order_id']);

                    $updateOrder['itemsInCart'] =(int) item_order::where(['order_id' => $arrData['order_id']])->sum('quantity');
                   
                    return $response = $this->responseData($data =  $updateOrder , $status = true ,$code = '200',$message = 'Order updated successfully');

                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading order');
                }
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'At present not accepting orders.');
            }   
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }



    public function showCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'order_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $cart_items = DB::table('items')
                        ->join('item_order', 'items.id', '=', 'item_order.item_id')
                        ->select('items.id','items.name','items.description','item_order.price', 'item_order.quantity', 'item_order.note')
                        ->where('item_order.order_id',  $arrData['order_id'])
                        ->get();
            
            return $response = $this->responseData($data =  $cart_items , $status = true ,$code = '200',$message = 'item of cart');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }


    public function cartItemQty(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            
            $cartQty = item_order::where(['order_id' => $arrData['order_id']])->sum('quantity');
            
            return $response = $this->responseData($data =  $cartQty , $status = true ,$code = '200',$message = '');

        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }


    public function updateOrderJson($id)
    {
        try 
        {
            //update order json
             $o= DB::table('orders')
                                    ->join('customers', 'orders.customer_id', '=', 'customers.id')
                                    ->select('orders.id', 'customers.firstname', 'customers.mobile', 'truck_id', 'pick_up_time', 'note', 'tip','total', 'order_status', 'order_placed_time') 
                                    ->where(['orders.id' => $id])
                                    ->first();


            $o->id = (int)$o->id;
            $o->firstname = $o->firstname;
            $o->truck_id = (int)$o->truck_id;
            $o->pick_up_time = date("h:iA", strtotime($o->order_placed_time . "+".$o->pick_up_time." minutes"));
            $o->total = (float)$o->total;


            

            $json= new \stdClass;
            $json->details = $o;

            $orderItems = DB::table('item_order')
                            ->join('items', 'items.id', '=', 'item_order.item_id')
                            ->select( 'items.id as item_id','items.name', 'item_order.quantity', 'item_order.price','item_order.note')
                            ->where('item_order.order_id',  $id)
                            ->get();
            $json->items = $orderItems;

            return json_encode($json);
        }catch (Exception $ex) {
           //return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }



    public function placeOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
                'order_id' => 'required',
                'pickup_option' => 'required',
                'tip' => 'required',
                'sub_total' => 'required',
                'total' => 'required',
                'datetime' => 'required',
                'lat' => 'required',
                'long' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            


            $truck = DB::table('trucks')->where(['id'=> $request->truck_id, 'active' => 1])->first();
            if(!empty($truck) > 0) {

                //check user distance from the truck
                $lat1 = $truck->latitude;
                $lon1 = $truck->longitude;
                $lat2 = $request->lat;
                $lon2 = $request->long;

                $theta = $lon1 - $lon2;
                $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
                $miles = acos($miles);
                $miles = rad2deg($miles);
                $miles = $miles * 60 * 1.1515;

                if($miles > 1)
                {
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Please make sure you are within a mile range to place order.');   
                }



                $order = order::where(['id' => $arrData['order_id']])->first(); //, 'order_placed' => 0
                
                if(!empty($order))
                {
                    //************DEDUCT STOCK**********************
                    $response_message = "";
                    $arrItems=[];
                    $orderItems = item_order::where(['order_id' => $arrData['order_id']])->get();

                    foreach ($orderItems as $items) {
                        //check
                        $requestedStock = $items->quantity;

                        if($requestedStock > 0 ){

                            $item = item::where(["id" => $items->item_id])->first();
                            
                            if($item->instock >= $requestedStock){
                                $item->requestedStock = $requestedStock;
                                $arrItems[]= $item;
                            }else{

                                // if this item is the only one item in the cart then that cart al
                                if($item->instock == 0){
                                    
                                    $response_message .= $item->name ." is sold out. ";
                                    
                                }
                                else{
                                    $response_message .= $item->name ." only ".$item->instock." left. Please change order.";
                                    
                                }
                            }

                        }
                    }

                    if(!empty($response_message)){
                        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $response_message);
                    }

                    //PAYMENT GATEWAY CODE 


                    //DEDUCT STOCK ON PAYMENT SUCCES
                    foreach ($arrItems as $item) {
                        $item->instock = $item->instock - $item->requestedStock;

                        unset($item->requestedStock);

                        $item->save();
                    }
                    //*****************************

                    $order->pick_up_time = $arrData['pickup_option'];
                    $d = new DateTime($arrData['datetime']);
                    $d->modify("+{$arrData['pickup_option']} minutes");
                    $order->order_wanted_time = $d->format('Y-m-d H:i:s');
                    $order->tip = $arrData['tip'];
                    $order->sub_total = $arrData['sub_total'];
                    $order->total = $arrData['total'];
                    $order->order_status = 'Placed';
                    $order->order_placed = 1;
                    $order->order_placed_time = $arrData['datetime'];

                    if(!empty($arrData['note']))
                    $order->note = $arrData['note'];
                    
                    $order->save();

                    //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();
                        

                    $res['order'] = $order;
                    $res['item'] = $orderItems;

                    $arrCustToken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                    


                    //SEND PUSH NOTIFICATION

                    if($arrCustToken->device_type == 'android'){
                            $push = new PushNotification('fcm');
                            $push->setMessage([
                                            'data' => [
                                                 'title'=>'Order #'.$arrData['order_id'].' placed',
                                                 'body'=>'You will receive acceptance from merchant.',
                                                 'order_id' => $arrData['order_id']
                                                 ]

                                         ])
                            ->setDevicesToken($arrCustToken->token)
                            ->send()
                            ->getFeedback();
                    }
                    else{

                        $push = new PushNotification('apn');

                        $message = [
                            'aps' => [
                                'alert' => [
                                    'title'=>'Order #'.$arrData['order_id'].' placed',
                                    'body'=>'You will receive acceptance from merchant.',
                                    'order_id' => $arrData['order_id']
                                ],
                                'sound' => 'default'

                            ]
                        ];
                        
                        $result = $push->setMessage($message)
                            ->setDevicesToken($arrCustToken->token)
                            ->send()
                            ->getFeedback();


                        $result->device_token = $arrCustToken->token;

                    }

                    $arrTruckToken = DB::table('truckdevicetokens')->where(['truck_id' => $order->truck_id, 'active' => 1])->orderBy('id', 'desc')->pluck('token')->toArray(); 
                                  
                    if(!empty($arrTruckToken)){
                        $push = new PushNotification('apn');
                        
                        $push->setConfig([
                            'dry_run' => false,
                            'passPhrase' => '1234',
                            'certificate' => base_path() . '/vendor/edujugon/push-notification/src/Config/iosCertificates/M_Certificate.pem',
                        ]); 

                        $message = [
                            'aps' => [
                                'alert' => [
                                    'title' => 'New Order #'.$arrData['order_id'],
                                    'body' => 'Please check there is new order.'
                                ],
                                'sound' => 'default'

                            ]
                        ];

                        $result = $push->setMessage($message)
                            ->setDevicesToken($arrTruckToken)
                            ->send()
                            ->getFeedback();


                        $result->device_token = $arrTruckToken;
                    }
        
                return $response = $this->responseData($data = $res, $status = true ,$code = '200',$message = 'Thank you for placing the order. Please look out for an order acceptance message and an order number from the food merchant within 15 min.');
                
                }else{
                    throw new Exception('Order already placed');
                }

            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'At present not accepting orders.');
            }            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }



    public function orderDetailById($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $order = DB::table('orders')
                    ->join('trucks', 'orders.truck_id', '=', 'trucks.id')
                    ->select('orders.id', 'orders.truck_id','trucks.name as truck_name', 'trucks.phone', 'pick_up_time', 'note', 'sub_total', 'tip','total', 'order_status', 'order_placed_time') 
                    ->where(['orders.id' => $id, 'order_placed' => 1])
                    ->get();

            $resData['order'] = $order;


            $items =  DB::table('item_order')
                    ->join('items', 'items.id', '=', 'item_order.item_id')
                    ->select('item_order.order_id', 'items.id','items.name', 'item_order.order_id', 'item_order.quantity', 'item_order.price','item_order.note')
                    ->where('item_order.order_id',  $order[0]->id)
                    ->get();

            if(!empty($items) > 0)
            $resData['items'] = $items;
            else
            $resData['items'] = [];

            return $response = $this->responseData($data = $resData, $status = true ,$code = '200',$message = 'Order details');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function pastOrdersByCustomerId($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            // $orders = customer::find($id)
            //             ->orders()
            //             ->join('trucks', 'orders.truck_id', '=', 'trucks.id')
            //             ->where(['order_placed' => 1])->orderBy('id', 'DESC')
            //             ->limit(100)
            //             ->get(['orders.id', 'truck_id', 'trucks.name', 'pick_up_time', 'note', 'total', 'order_status', 'order_placed_time']);

            $orders = DB::table('orders')
                        ->select('orders.id', 'truck_id', 'trucks.name', 'pick_up_time', 'note', 'total', 'order_status',DB::raw('DATE_FORMAT(order_placed_time, "%Y-%c-%d %h:%i%p") as order_placed_time'))
                        ->join('trucks', 'orders.truck_id', '=', 'trucks.id')
                        ->where([ 'customer_id' => $id ,'order_placed' => 1])
                        ->orderBy('id', 'DESC')
                        ->limit(100)
                        ->get();

            return $response = $this->responseData($data = $orders, $status = true ,$code = '200',$message = 'Past Orders');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    public function pastOrdersByTruckId_old(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }



            $arrOrders['order_placed'] = [];
            $arrOrders['order_accepted'] = [];
            $arrOrders['order_ready'] = [];
            $arrOrders['order_delivered'] = [];
            $arrOrders['order_rejected'] = [];
            $arrOrders['order_cancelled'] = [];

            $orders = DB::table('orders')
                                        ->join('customers', 'orders.customer_id', '=', 'customers.id')
                                        ->select('orders.id', 'customers.firstname', 'customers.mobile', 'truck_id', 'pick_up_time', 'note', 'tip','total', 'order_status', 'order_placed_time', 'order_placed', 'order_accepted', 'order_ready', 'order_delivered', 'order_rejected', 'order_cancelled') 
                                        ->where(['truck_id' => $arrData['truck_id']])
                                        ->orderby('orders.id')
                                        ->whereDate('order_placed_time', '>=', $arrData['datetime'])
                                        ->get();

            //get all items of all orders placed today 
                                     
            $gItems =  DB::table('item_order')
                    ->join('items', 'items.id', '=', 'item_order.item_id')
                    ->select('items.id as item_id','items.name', 'item_order.order_id', 'item_order.quantity', 'item_order.price','item_order.note')
                    ->whereDate('item_order.created_at', '>=', $arrData['datetime'])
                    ->get();

            foreach ($orders as $o) {

                $o->id = (int)$o->id;
                $o->firstname = $o->firstname;
                $o->truck_id = (int)$o->truck_id;
                $o->pick_up_time = date("h:iA", strtotime($o->order_placed_time . "+".$o->pick_up_time." minutes"));
                $o->total = (float)$o->total;
                
                    $a = new \stdClass;
                    $a->details = $o;
                    
                    $arrI = [];

                    foreach ($gItems as $k => $i) {
                        if($i->order_id == $o->id){
                            array_push($arrI, $i);
                            unset($i->order_id);
                            unset($gItems[$k]);
                        }
                    }
                    $a->items = $arrI;

                if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  )
                {
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_placed'],$a);
                }
                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_accepted'],$a);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 1 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_ready'],$a);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 1 AND 
                   $o->order_delivered == 1 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_delivered'],$a);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 1 AND 
                   $o->order_cancelled == 0  OR
                   $o->order_accepted == 1
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_rejected'],$a);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 1  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_cancelled'],$a);
                }
            }
            

            return $response = $this->responseData($data = $arrOrders, $status = true ,$code = '200',$message = 'Past Orders..');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }



    public function pastOrdersByTruckId(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }



            $arrOrders['order_placed'] = [];
            $arrOrders['order_accepted'] = [];
            $arrOrders['order_ready'] = [];
            $arrOrders['order_delivered'] = [];
            $arrOrders['order_rejected'] = [];
            $arrOrders['order_cancelled'] = [];

            $orders = DB::table('orders')
                                        ->select('orders.id', 'truck_id', 'pick_up_time', 'note', 'tip','total', 'order_status', 'order_placed_time', 'order_placed', 'order_accepted', 'order_ready', 'order_delivered', 'order_rejected', 'order_cancelled', 'order_json') 
                                        ->where(['truck_id' => $arrData['truck_id']])
                                        ->orderby('orders.id')
                                        ->whereDate('order_placed_time', '>=', $arrData['datetime'])
                                        ->get();

            $acceptedOrders = DB::table('orders')
                                        ->select('order_json', 'order_ready') 
                                        ->where(['truck_id' => $arrData['truck_id'], 'order_placed' => 1 , 'order_accepted' => 1, 'order_delivered' => 0, 'order_rejected'=>0, 'order_cancelled' =>0])
                                        ->orderby('order_wanted_time')
                                        ->whereDate('order_placed_time', '>=', $arrData['datetime'])
                                        ->get();

            foreach ($acceptedOrders as $o2) {
                if($o2->order_ready == 0)
                {
                    array_push($arrOrders['order_accepted'],json_decode($o2->order_json));
                }else{
                    array_push($arrOrders['order_ready'],json_decode($o2->order_json));
                }
            }

            foreach ($orders as $o) {

                if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  )
                {
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_placed'],json_decode($o->order_json));
                }
                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    //array_push($arrOrders['order_accepted'],json_decode($o->order_json));
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 1 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    //array_push($arrOrders['order_ready'],json_decode($o->order_json));
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 1 AND 
                   $o->order_delivered == 1 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_delivered'],json_decode($o->order_json));
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 1 AND 
                   $o->order_cancelled == 0  OR
                   $o->order_accepted == 1
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_rejected'],json_decode($o->order_json));
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 1  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_cancelled'],json_decode($o->order_json));
                }
            }

            

            $arrResponse['orders'] = $arrOrders;
            
            $arrResponse['items'] = item::where(['truck_id' => $arrData['truck_id'], 'active' => 1])
                                        ->where('instock', '<=', '10' )

                                        ->orderBy('instock' , 'asc')

                                        ->get(['id', 'name', 'instock']);

            return $response = $this->responseData($data = $arrResponse, $status = true ,$code = '200',$message = 'Past Orders..');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    public function acceptOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1,'order_accepted' => 0, 'order_cancelled' => 0])->first();

            if(!empty($order))
            {
                $order->order_status = 'Accepted';
                $order->order_accepted = 1;
                $order->order_accepted_time = $arrData['datetime'];
                $order->order_acceptance_interval = Date( "i:s",strtotime($arrData['datetime']) - strtotime($order->order_placed_time));
                $order->updated_at = $arrData['datetime'];

                $order->save();

                //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();
            
            
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' accepted',
                                             'body'=>'Expect a message when ready for pickup.',
                                             'order_id' => $arrData['order_id']
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' accepted',
                                'body'=>'Expect a message when ready for pickup.',
                                'order_id' => $arrData['order_id']
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order accepted successfully.');
            }else{
                throw new Exception('Order already accepted');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }




    public function readyOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1, 'order_accepted' => 1, 'order_ready' => 0])->first();
            
            if(!empty($order))
            {   
                $order->order_status = 'Ready';
                $order->order_ready = 1;
                $order->order_ready_time = $arrData['datetime'];
                $order->order_ready_interval = Date( "i:s", strtotime($order->pick_up_time) - strtotime($arrData['datetime']));
                $order->updated_at = $arrData['datetime'];
                $order->save();

                //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();

                
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' ready',
                                             'body'=>'Please come and pickup your order.',
                                             'order_id' => $arrData['order_id']
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' ready',
                                'body'=>'Please come and pickup your order.',
                                'order_id' => $arrData['order_id']
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }
            }else{
                throw new Exception('Order already ready');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order ready for pickup.');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function reminderOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id']])->first();
            
            if(!empty($order))
            {
                
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' ready',
                                             'body'=>'Please come and pickup your order',
                                             'order_id' => $arrData['order_id']
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' ready',
                                'body'=>'Please come and pickup your order',
                                'order_id' => $arrData['order_id']
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;

                }
            }else{
                throw new Exception('Order already ready');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order ready for pickup.');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }



    public function deliverOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1, 'order_accepted' => 1, 'order_ready' => 1, 'order_delivered' => 0])->first();
            
            if(!empty($order))
            {
                $order->order_status = 'Delivered';
                $order->order_delivered = 1;
                $order->order_delivered_time = $arrData['datetime'];
                $order->order_pickup_interval = Date( "i:s",strtotime($arrData['datetime']) - strtotime($order->order_ready_time));
                $order->updated_at = $arrData['datetime'];
                $order->save();

                //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();

                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();//->pluck('token', 'device_type')->toArray();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' delivered.',
                                             'body'=>'Thank you for your order. Enjoy your meal!',
                                             'order_id' => $arrData['order_id']
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' delivered.',
                                 'body'=>'Thank you for your order. Enjoy your meal!',
                                 'order_id' => $arrData['order_id']
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }
            }else{
                throw new Exception('Order already pickedup.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Please thank the customer. Tell them Enjoy your meal!!');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    //Customer is going to cancel the order 
    //So pushnotification will be sent to Truck devices
    public function cancelOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();
            
            if(!empty($order))
            {
                if($order->order_ready == 1){
                    throw new Exception('Order is ready, so can not be cancel.');
                }
                else if($order->order_accepted == 1){
                    throw new Exception('Order already accepted, so can not be cancel.');
                }


                $arrItems=[];
                $orderItems = item_order::where(['order_id' => $arrData['order_id']])->get();

                foreach ($orderItems as $items) {
                    
                    $requestedStock = $items->quantity;

                    if($requestedStock > 0 ){

                        $item = item::where(["id" => $items->item_id])->first();
                        
                        $item->instock = $item->instock + $requestedStock;
                        $item->save();

                    }
                }

                $order->order_status = 'cancelled';
                $order->order_cancelled = 1;
                $order->order_cancelled_time = $arrData['datetime'];
                $order->order_cancelled_remark = (!empty($arrData['remark'])) ? $arrData['remark'] : Null;
                $order->updated_at = $arrData['datetime'];
                $order->save();

                //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();

                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order Cancelled',
                                             'body'=>'You have cancelled your order!!'
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order Cancelled',
                                'body'=>'You have cancelled your order!!'
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }


            
            }else{
                throw new Exception('Order not placed or not found.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order cancelled successfully..');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }





    public function rejectOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();
            
            if(!empty($order))
            {

                if($order->order_rejected == 1){
                    return $response = $this->responseData($data = $order,$status = FALSE ,$code = '221',$message = "order already rejected");
                }

                $arrItems=[];
                $orderItems = item_order::where(['order_id' => $arrData['order_id']])->get();

                foreach ($orderItems as $items) {
                    
                    $requestedStock = $items->quantity;

                    if($requestedStock > 0 ){

                        $item = item::where(["id" => $items->item_id])->first();
                        
                        $item->instock = $item->instock + $requestedStock;
                        $item->save();

                    }
                }

                $order->order_status = 'Rejected';
                $order->order_rejected = 1;
                $order->order_rejected_time = $arrData['datetime'];
                $order->order_rejected_interval = Date( "i:s",strtotime($arrData['datetime']) - strtotime($order->order_placed_time));
                $order->order_rejected_remark = (!empty($arrData['remark'])) ? $arrData['remark'] : Null;
                $order->updated_at = $arrData['datetime'];
                $order->save();

                //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();




                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' rejected',
                                             'body'=>'Order rejected for unavoidable reason.',
                                             'order_id' => $arrData['order_id']
                                             ],
                                        'sound' => 'default'

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' rejected',
                                'body'=>'Order rejected for unavoidable reason.',
                                'order_id' => $arrData['order_id']
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }

            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Invalid order");
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'order rejected successfully.');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    
}
