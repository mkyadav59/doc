<?php

namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
class BaseController extends Controller
{   
    /**
     * used to generate response data
     * @param array $data
     * @param boolean $status
     * @param integer $code
     * @param string $message
     * @return jsonObject
     */
    public function responseData($data = array(),$status = TRUE ,$code = '200',$message = ''){
        
        return Response::json([
                        'status' => $status,
                        'data' => $data,
                        'message' => $message
                    ],$code); // Status code here
    }
}
