<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, Log;
use App\item, App\customization_option;

class CustomizationOptionController extends BaseController
{
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'type_id' => 'required',
                'item_id' => 'required',
                'name' => 'required',
                'price' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objCustomization = new customization_option;

            $objCustomization->type_id = (int)$arrData['type_id'];
            $objCustomization->item_id = (int)$arrData['item_id'];
            $objCustomization->name = $arrData['name'];
            $objCustomization->price = (double)$arrData['price'];
            $objCustomization->active = 1;
            $objCustomization->created_at = $arrData['datetime'];
            $objCustomization->updated_at = $arrData['datetime'];
        
            $objCustomization->save();


            if($objCustomization->id > 0){
                return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Customization Option added successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $customization_type = DB::table('customization_options')->where(['id' => $id])->get();

            if(!empty($customization_type)){
                
                return $response = $this->responseData($data = $customization_type,$status = true ,$code = '200',$message = 'customization options details');
            }else{
                 throw new Exception('invalid customization options id');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

  


    public function updateCustomizationOption(Request $request)
    {
        try
        {
            $arrData = $request->all();

            $rules = array(
                'option_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objCustomization = customization_option::find($arrData['option_id']);
            

            if(isset($arrData['name']))
                $objCustomization->name = $arrData['name']; 

            if(isset($arrData['price']))
                $objCustomization->price = $arrData['price'];

            $objCustomization->updated_at = $arrData['datetime'];
        
            $result = $objCustomization->save();


            if($result){

                return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Category updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCustomizationOption(Request $request)
    {
        try{
            $arrData = $request->all();

            $rules = array(
                'option_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objCustomization = customization_option::find($arrData['option_id'])->delete();
            
            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'Option deleted successfully.');
        }catch (Exception $ex) {

            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        } 
    }
     

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allcustomizationsbyitemid($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }
            $resData = array();
            $arrType = array();
            $arrOptions = array();

                
            $arrCustomization_type = $customization_type = DB::table('customization_types')->where(['item_id'=> $id])->orderBy('order')->get();
            
            $customization_options = DB::table('customization_options')->where(['item_id'=>  $id])->get();
                
                
                foreach ($arrCustomization_type as $cat) {
                    $arrCat['id'] = $cat->id;
                    $arrCat['name'] = $cat->name;
                    $arrCat['required'] = $cat->required;
                    $arrCat['order'] = $cat->order;
                    $arrCat['active'] = $cat->active;
                    $arrCat['customization_options'] = [];
                    foreach ($customization_options as $i) {
                        if($i->type_id == $cat->id){

                            $arrCat['customization_options'][] = $i;        
                        }
                    }
                    $resData['detail'] = item::find($id);
                    $resData['customization_types'][] = $arrCat;
                }
                return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'All customizations belongs to the item');
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        }
    }



}
