<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, Image, Log;
use App\vendor, App\category;

class CategoryController extends BaseController
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $category = new category;
            $category->truck_id = $arrData['truck_id'];
            $category->name = $arrData['name'];
            $category->description = $arrData['description'];
            $category->order = category::where( ['truck_id' => $arrData['truck_id']] )->count() + 1;
            $category->active = '1';
            $category->created_at = $arrData['datetime'];
            $category->updated_at = $arrData['datetime'];
        
            $category->save();

            if($category->id > 0){
                return $response = $this->responseData($data = $category, $status = true ,$code = '200',$message = 'Category added successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try 
        {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            $category = DB::table('categories')->where(['id'=> $id ])->get()->first();//category::find($id);

            if(!empty($category)){
                
                return $response = $this->responseData($data = $category,$status = true ,$code = '200',$message = 'Category details');
            }else{
                 return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'invalid category id');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }




    public function updateCategory(Request $request)
    {
        try
        {
            $arrData = $request->all();

            $rules = array(
                'category_id' => 'required',
                'truck_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $category = category::find($arrData['category_id']);
            $category->name = $arrData['name'];
          
            $category->description = $arrData['description'];
            $category->active = $arrData['active'];
            $category->updated_at = $arrData['datetime'];
        
            $category->save();

            if($category->id > 0){

                return $response = $this->responseData($data = $category, $status = true ,$code = '200',$message = 'Category updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allcategoriesbytruckid($id)
    {
        try 
        {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }
            $resData = array();
            $arrCat = array();
            $arrItems = array();

                
            $arrCategory = $category = DB::table('categories')->where(['truck_id'=> $id])->orderBy('order')->get();
            
            $items = DB::table('items')->where(['truck_id'=>  $id])->orderBy('order')->get();
                
                foreach ($arrCategory as $cat) {
                    $arrCat['id'] = $cat->id;
                    $arrCat['name'] = $cat->name;
                    $arrCat['order'] = $cat->order;
                    $arrCat['active'] = $cat->active;
                    $arrCat['items'] = [];
                    foreach ($items as $i) {
                        if($i->category_id == $cat->id){
                            $i->price = round($i->price / 100, 2);
                            if($i->img != NULL){
                                $i->img =  url('/').'/uploads/items/'.$i->img;
                            }
                            else{
                                $i->img =  url('/').'/uploads/items/item_default.jpg';
                            }

                            $arrCat['items'][] = $i;        
                        }
                    }
                    $resData['category'][] = $arrCat;
                }
                return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'All categories belongs to the truck');
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function categoryUpdateOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();

            $rules = array(
                'category_id' => 'required',
                'truck_id' => 'required',
                'move' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $category = category::find($arrData['category_id']);

            //echo "<pre>"; print_r($category->order); exit();

            if($arrData['move'] == "up"){


                $o = (int) $category->order - 1;
                $moveUp = category::where(['truck_id' => $arrData['truck_id'], 'order' =>  $o])->first();

                
                if(!empty($moveUp)){
                    $moveUp->order  = $category->order;
                    $moveUp->updated_at = $arrData['datetime'];
                    $moveUp->save();

                    if($moveUp->id > 0){
                        $category->order = $o;
                        $category->updated_at = $arrData['datetime'];
                        $category->save();
                    }
                }

            }elseif ($arrData['move'] == "down") {
                
                $o = (int) $category->order + 1;
                $moveDown = category::where(['truck_id' => $arrData['truck_id'], 'order' => $o])->first();

                if(!empty($moveDown)){

                    $moveDown->order  = $category->order;
                    $moveDown->updated_at = $arrData['datetime'];
                    $moveDown->save();

                    if($moveDown->id > 0){
                        $category->order = $category->order + 1;
                        $category->updated_at = $arrData['datetime'];
                        $category->save();
                    }
                }
            }


            if($category->id > 0){

                return $response = $this->responseData($data = $category, $status = true ,$code = '200',$message = 'Category Order updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



    public function categoryUpdateStatus(Request $request)
    {
        try
        {
            $arrData = $request->all();

            $rules = array(
                'category_id' => 'required',
                'truck_id' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $category = category::find($arrData['category_id']);
            $category->active = $arrData['active'];
            $category->updated_at = $arrData['datetime'];

            $category->save();

            if($category->id > 0){

                return $response = $this->responseData($data = $category, $status = true ,$code = '200',$message = 'Category updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }
}
