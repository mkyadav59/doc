<?php

namespace App\Http\Controllers\v4;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Notifications\Notifiable;
use Edujugon\PushNotification\PushNotification;

use DB, Exception, Validator, DateTime, Log;
use App\truck, App\customer, App\order, App\order_payment, App\customer_truck, App\customerdevicetoken,  App\truckdevicetoken;

class BroadcastController extends BaseController
{
	//send notification to customers marked truck as favourite
    public function fromTruckToCustomers(Request $request)
    {
    	try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'message' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }


            //get customer device token by truck id from customer truck table 
            $androidDeviceTokens = DB::table('customerdevicetokens')
            						->join('customer_truck', 'customerdevicetokens.customer_id', '=', 'customer_truck.customer_id')
            						->where(['truck_id' => $arrData['truck_id'], 'device_type' => 'android' ])
            						->get(['token'])->pluck('token')->toArray();

            $push = new PushNotification('fcm');
            $result1 = $push->setMessage([
                            'data' => [
                                 'title'=>'Broadcast message',
                                 'body'=> $arrData['message'],
                                 'truck_id' => $arrData['truck_id']
                                 ]

                         ])
			            ->setDevicesToken($androidDeviceTokens)
			            ->send()
			            ->getFeedback();

						//DELETE BROKEN TOKENS FROM DATABASE
                        if(!empty($result1->tokenFailList)) {
                            foreach ($result1->tokenFailList as $token) {
                                customerdevicetokens::where(['token' => $token])->delete();
                            }
                        }

            //get customer device token by truck id from customer truck table 
            $iosDeviceTokens = DB::table('customerdevicetokens')
            						->join('customer_truck', 'customerdevicetokens.customer_id', '=', 'customer_truck.customer_id')
            						->where(['truck_id' => $arrData['truck_id'], 'device_type' => 'ios' ])
            						->get(['token'])->pluck('token')->toArray();

            $push = new PushNotification('apn');

                        $message = [
                            'aps' => [
                                'alert' => [
										'title'=>'Broadcast message',
										'body'=> $arrData['message'],
										'truck_id' => $arrData['truck_id']
                                ],
                                'badge' => 1,
                                'sound' => 'default'

                            ]
                        ];
                        
                        $result = $push->setMessage($message)
                            ->setDevicesToken($iosDeviceTokens)
                            ->send()
                            ->getFeedback();

                        //DELETE BROKEN TOKENS FROM DATABASE
                        if(!empty($result->tokenFailList)) {
                            foreach ($result->tokenFailList as $token) {
                                customerdevicetokens::where(['token' => $token])->delete();
                            }
                        }
            return $response = $this->responseData($data =  new \stdClass , $status = true ,$code = '200',$message = 'Message broadcasting done.');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }
}
