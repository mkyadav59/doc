<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, Log;
use App\servicetable;

class ServiceTablesController extends BaseController
{
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'name' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objservicetables = new servicetable;

            $objservicetables->truck_id = (int)$arrData['truck_id'];
            $objservicetables->name = $arrData['name'];
            $objservicetables->active = 1;
            $objservicetables->created_at = $arrData['datetime'];
            $objservicetables->updated_at = $arrData['datetime'];
        
            $objservicetables->save();


            if($objservicetables->id > 0){
                return $response = $this->responseData($data = $objservicetables, $status = true ,$code = '200',$message = 'Saved Successfully.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $allServicetables = DB::table('servicetables')->where(['truck_id' => $arrData['truck_id']])->get(['id', 'name', 'active']);

            if(!empty($allServicetables)){

                $responseData['services'] = $allServicetables;

                return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'All service tables.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '221',$message = 'No tables found');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

  


    public function updateServicetable(Request $request)
    {
        try
        {
            $arrData = $request->all();

            $rules = array(
                'servicetable_id' => 'required',
                'name' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objservicetables = servicetable::find($arrData['servicetable_id']);

            $objservicetables->name = $arrData['name']; 
            
            $objservicetables->active = (int) $arrData['active']; 

            $objservicetables->updated_at = $arrData['datetime'];
        
            $objservicetables->save();


            if($objservicetables){

                return $response = $this->responseData($data = $objservicetables, $status = true ,$code = '200',$message = 'Saved Successfully.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteservicetable(Request $request)
    {
        try{
            $arrData = $request->all();

            $rules = array(
                'servicetable_id' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objservicetables = servicetable::find($arrData['servicetable_id']);

            $objservicetables->active = 1; 

            $objservicetables->updated_at = $arrData['datetime'];
        
            $result = $objservicetables->save();

            
            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'servicetable deleted successfully.');
        }catch (Exception $ex) {

            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        } 
    }
     

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allservicetablesByTruckId($id)
    {
        try 
        {
            $arrData = $request->all();

            $rules = array(
                'truck_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $servicetables = DB::table('servicetables')->where(['truck_id'=>  $id])->get();
                
            $responseData['servicetables'] = $servicetables;

            return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'All customizations belongs to the item');
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        }
    }




}
