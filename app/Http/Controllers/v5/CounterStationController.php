<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, Log;
use App\counterstation;

class CounterStationController extends BaseController
{
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'name' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objcounterstations = new counterstation;

            $objcounterstations->truck_id = (int)$arrData['truck_id'];
            $objcounterstations->name = $arrData['name'];
            $objcounterstations->active = 1;
            $objcounterstations->created_at = $arrData['datetime'];
            $objcounterstations->updated_at = $arrData['datetime'];
        
            $objcounterstations->save();


            if($objcounterstations->id > 0){
                return $response = $this->responseData($data = $objcounterstations, $status = true ,$code = '200',$message = 'Saved Successfully.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $allCounterStations = DB::table('counterstations')->where(['truck_id' => $arrData['truck_id']])->get(['id', 'name', 'active']);

            if(!empty($allCounterStations)){
                
                $responseData['services'] = $allCounterStations;

                return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'All Pickup Stations.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No Pickup Station found');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

  


    public function updateCounterstation(Request $request)
    {
        try
        {
            $arrData = $request->all();

            $rules = array(
                'counterstation_id' => 'required',
                'name' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objcounterstations = counterstation::find($arrData['counterstation_id']);

            $objcounterstations->name = $arrData['name']; 

            $objcounterstations->active = (int) $arrData['active']; 

            $objcounterstations->updated_at = $arrData['datetime'];
        
            $objcounterstations->save();


            if($objcounterstations){

                return $response = $this->responseData($data = $objcounterstations, $status = true ,$code = '200',$message = 'Saved Successfully.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCounterstation(Request $request)
    {
        try{
            $arrData = $request->all();

            $rules = array(
                'truck_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objcounterstations = counterstation::find($arrData['truck_id'])->delete();
            
            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'counterstation deleted successfully.');
        }catch (Exception $ex) {

            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        } 
    }
     

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allcounterstationsByTruckId($id)
    {
        try 
        {
            $arrData = $request->all();

            $rules = array(
                'truck_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $counterstations = DB::table('counterstations')->where(['truck_id'=>  $id])->get();
                
            $responseData['counterstations'] = $counterstations;

            return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'All customizations belongs to the item');
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        }
    }




}
