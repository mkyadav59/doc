<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, Log;
use App\server;

class ServersController extends BaseController
{
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'name' => 'required',
                'mobile' => 'required',
                'send_sms' => 'required',
                'datetime' => 'required'

            );


            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $result = DB::table('servers')->where(['truck_id' => $arrData['truck_id'], 'name' => $arrData['name']])->get()->first();

            if(!empty($result)){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "The name has already been taken.");
            }

            //get country code
            $result = DB::table('trucks')
                            ->join('vendors', 'vendors.id', '=', 'trucks.vendor_id')
                            ->where(['trucks.id' => $arrData['truck_id'] ])
                            ->get(['vendors.country_code'])->first();
            
            //insert in to database
            $objServers = new server;

            $objServers->truck_id = (int)$arrData['truck_id'];
            $objServers->name = $arrData['name'];
            $objServers->country_code = $result->country_code;
            $objServers->mobile = $arrData['mobile'];
            $objServers->send_sms = (int) $arrData['send_sms'];
            $objServers->active = 1;
            $objServers->created_at = $arrData['datetime'];
            $objServers->updated_at = $arrData['datetime'];
        
            $objServers->save();

            if($objServers->id > 0){
                return $response = $this->responseData($data = $objServers, $status = true ,$code = '200',$message = 'Saved Successfully.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $allServes = DB::table('servers')->where(['truck_id' => $arrData['truck_id']])->get(['id', 'name', 'mobile', 'send_sms', 'active']);

            if(!empty($allServes)){

                $responseData['services'] = $allServes;

                return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'All servers.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '221',$message = 'No server found');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

  


    public function updateServer(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'server_id' => 'required',
                'name' => 'required',
                'mobile' => 'required',
                'send_sms' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $result = DB::table('servers')
                        ->where('id', '!=' , $arrData['server_id'])
                        ->where(['truck_id' => $arrData['truck_id'], 'name' => $arrData['name']])->get()->first();

            if(!empty($result)){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "The name has already been taken.");
            }

            //insert in to database
            $objServers = server::find($arrData['server_id']);

            $objServers->name = $arrData['name']; 

            $objServers->mobile = $arrData['mobile']; 
            
            $objServers->send_sms = (int) $arrData['send_sms']; 
            
            $objServers->active = (int) $arrData['active']; 

            $objServers->updated_at = $arrData['datetime'];
        
            $objServers->save();


            if($objServers){

                return $response = $this->responseData($data = $objServers, $status = true ,$code = '200',$message = 'Saved Successfully.');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteServer(Request $request)
    {
        try{
            $arrData = $request->all();

            $rules = array(
                'truck_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $objServers = server::find($arrData['truck_id'])->delete();
            
            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'Server deleted successfully.');
        }catch (Exception $ex) {

            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        } 
    }
     

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allServersByTruckId($id)
    {
        try 
        {
            $arrData = $request->all();

            $rules = array(
                'truck_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $servers = DB::table('servers')->where(['truck_id'=>  $id])->get();
                
            $responseData['servers'] = $servers;

            return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'All customizations belongs to the item');
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Something went wrong please try after sometime");
        }
    }



}
