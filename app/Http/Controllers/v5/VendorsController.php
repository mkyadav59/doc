<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator, Log;
use App\vendor, App\truck, App\otp;
use Illuminate\Support\Facades\Hash;
use App\Models\VendorRegistration;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Mail\VendorRegistrationMail;
use AWS;

class VendorsController extends BaseController
{
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerVendor(Request $request)
    {

        try
        {

            $arrData = $request->all();
            $rules = array(
                'name' => 'required',
                'mobile' => 'required|digits:10',
                'email' => 'required|email',
                'country_code' => 'required|numeric',
                'datetime' => 'required'
            );

            
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

           

            $vendor = vendor::where(['mobile' => $arrData['mobile']] )->get()->first();
            //return $vendor;
            if(isset($vendor)){
                //if vendor already registered and active
                if($vendor['active'] == 1){
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Mobile number is already registered.");
                }
                //if vendor registered but not active
                elseif($vendor['active'] == -1){

                    otp::where(['entity_id'=>$vendor['id'], 'type'=> 'vendor'])->delete();

                    $vendor->otp =(int) DB::select("CALL saveOtp('".$arrData['mobile']."',".$vendor->id.", 'vendor')")[0]->OTP;

                    $message = "Your One-Time Password (OTP) is ".$vendor->otp.". Please do not share this password with anyone - GrilledChili";

                    $sms = AWS::createClient('sns');
        
                    $sms->publish([
                            'Message' => $message,
                            'PhoneNumber' => $arrData['country_code'].$arrData['mobile'], 
                            'MessageAttributes' => [
                                'AWS.SNS.SMS.SMSType'  => [
                                    'DataType'    => 'String',
                                    'StringValue' => 'Transactional',
                                 ]
                             ],
                          ]);
                }
            }
            //new vendor registration
            else{

                //split name if firstname lastname provided 
                $name = explode(' ', $arrData['name']);
                $firstname = (!empty($name) > 1) ? $name[0] :  $arrData['name'];
                $lastname = (!empty($name) > 1) ? $name[1] :  '';
                

                //insert in to database
                $vendor = new vendor;
                $vendor->firstname = $firstname;
                $vendor->lastname = $lastname;
                $vendor->mobile = $arrData['mobile'];
                $vendor->email = $arrData['email'];
                $vendor->country_code = $arrData['country_code'];
                $vendor->created_at = $arrData['datetime'];
                $vendor->updated_at = $arrData['datetime'];
                $vendor->active = -1;
                
                $vendor->save();

                //return $vendor;
                if(!empty($vendor)){

                    otp::where(['entity_id'=>$vendor['id'], 'type'=> 'vendor'])->delete();

                    $vendor->otp =(int) DB::select("CALL saveOtp('".$arrData['mobile']."',".$vendor->id.", 'vendor')")[0]->OTP;

                    $message = "Your One-Time Password (OTP) is ".$vendor->otp.". Please do not share this password with anyone - GrilledChili";

                    $sms = AWS::createClient('sns');
        
                    $sms->publish([
                            'Message' => $message,
                            'PhoneNumber' => $arrData['country_code'].$arrData['mobile'], 
                            'MessageAttributes' => [
                                'AWS.SNS.SMS.SMSType'  => [
                                    'DataType'    => 'String',
                                    'StringValue' => 'Transactional',
                                 ]
                             ],
                          ]);

                        $emailVerificationLink = route("email-verification-link", ["token"=>$token]);
                        $data = ([
                            'name' => $name,
                            'email' => $mobile,
                            'mobile' => $email,
                            'password' => $generatePassword,
                            'verify_link' => $emailVerificationLink,
                        ]);
    
                        Mail::to($email)->send(new VendorRegistrationMail($data));

                }else{
                     return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Please check mobile number or do registration');
                }
            }

        return $response = $this->responseData($data = $vendor, $status = true ,$code = '200',
            $message = 'Hey '.$vendor->firstname.', an OTP has been sent to ('.$arrData['country_code'].') '.$arrData['mobile']);
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function vendorLogin(Request $request)
    {

        try
        {
            $arrData = $request->all();
            $rules = array(
                'mobile' => 'required|digits:10',
                'country_code' => 'required|numeric',
                'truck_id' => 'required|numeric',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            $vendor = vendor::where(['mobile' => $arrData['mobile']] )->get(['id', 'firstname'])->first();
            


            otp::where(['entity_id'=>$vendor->id, 'type'=> 'vendor'])->delete();
            
            $vendor->otp = (int) DB::select("CALL saveOtp('".$arrData['mobile']."',".$vendor->id." , 'vendor')")[0]->OTP;

            if($arrData['truck_id'] > 0){
                $truck = DB::table('trucks')->where(['id'=> $arrData['truck_id']])->get(['name','alias'])->first();

                $message = "For ".$truck->name.' ('.$truck->alias.") One-Time Password (OTP) is ".$vendor->otp.". Please do not share this password with anyone - GrilledChili";

            }else{
                $message = "One-Time Password (OTP) is ".$vendor->otp.". Please do not share this password with anyone - GrilledChili";
            }
            
            $sms = AWS::createClient('sns');

            $sms->publish([
                    'Message' => $message,
                    'PhoneNumber' => $arrData['country_code'].$arrData['mobile'], 
                    'MessageAttributes' => [
                        'AWS.SNS.SMS.SMSType'  => [
                            'DataType'    => 'String',
                            'StringValue' => 'Transactional',
                         ]
                     ],
                  ]);


            return $response = $this->responseData($data = $vendor, $status = true ,$code = '200',$message = 'Hey '.$vendor->firstname. ', an otp has been sent to this number.');

            
        }catch (Exception $ex) {
            echo $ex;exit();
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            $vendor = DB::table('vendors')->where(['id' => $id, 'active'=> 1] )->get()->first();

                if(!empty($vendor)){

                    $vendor->id = (int) $vendor->id;
                    $vendor->active = (int) $vendor->active;

                    $resData['vendor'] = $vendor;

                    $trucks = truck::where(['vendor_id'=> $vendor->id, 'active' => 1])->get(['id','name','logo']);
                    $resData['trucks'] = $trucks;

                    if(!empty($trucks)){
                        return $response = $this->responseData($date = $vendor, $status = true ,$code = '200',$message = 'Vendor details.');
                    }else{
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'No trucks registered.');
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Your account has been suspended.');
                }


            if(!empty($vendor)){
                return $response = $this->responseData($data = $arrResponse,$status = true ,$code = '200',$message = 'Vendor Details');
            }else{
                 return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'invalid vendor id');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }




    public function updateVendor(Request $request)
    {
       try
        {

            $arrData = $request->all();
            $rules = array(
                'id' => 'required',
                'name' => 'required',
                'email' => 'email',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            //split name if firstname lastname provided 
            $name = explode(' ', $arrData['name']);
            $firstname = (!empty($name) > 1) ? $name[0] :  $arrData['name'];
            $lastname = (!empty($name) > 1) ? $name[1] :  '';
            
            //insert in to database
            $vendor = vendor::find($arrData['id']);
            if(!empty($vendor)){
                $vendor->firstname = $firstname;
                $vendor->lastname = $lastname;
                $vendor->email = (!empty($arrData['email']) && $arrData['email'] != '') ? $arrData['email'] : null;
                $vendor->latitude = (!empty($arrData['latitude']) && $arrData['latitude'] != '') ? $arrData['latitude'] : null;
                $vendor->longitude = (!empty($arrData['longitude']) && $arrData['longitude'] != '') ? $arrData['longitude'] : null;
                $vendor->save();
            }
            else{
                 return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'invalid vendor id');
            }

            return $response = $this->responseData($data = $vendor,$status = true ,$code = '200',$message = 'Proceed');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function GcmLogin(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'mobile' => 'required|digits:10'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            $vendor = vendor::where(['mobile' => $arrData['mobile'], 'active' => 1] )->get(['id', 'firstname'])->first();
            //return $vendor;
            if(!empty($vendor)){
                // $otp = (new OtpController)->sendOtp($vendor->id, 'vendor');
                // $vendor->otp = $otp;

                $vendor->otp = (int) DB::select("CALL saveOtp('".$arrData['mobile']."',".$vendor->id." , 'vendor')")[0]->OTP;

            }else{
                 return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Please check mobile number or do registration');
            }

            return $response = $this->responseData($data = $vendor, $status = true ,$code = '200',$message = 'Hey '.$vendor->firstname. ', an otp has been sent to this number.');


        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 

    }

}
