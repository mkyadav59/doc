<?php

namespace App\Http\Controllers\v5;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\truck_subscription;
use Stripe\Stripe;
use DB, Validator, Exception, Log;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Cashier;
use Carbon\Carbon;
use Stripe\Customer;
use Stripe\Charge;
use Session;
use Laravel\Cashier\Exceptions\IncompletePayment;

class SubscriptionsController extends BaseController
{
    protected $stripe;

    public function showWelcome(){
        return view('welcome');
    }

    public function __construct() 
    {
        $key = env('STRIPE_SECRET');
        $this->stripe = new \Stripe\StripeClient($key);
    }

    public function Invoice($vendor, $response)
    {
        try {
            $vendor->invoices()->filter(function($invoice) use ($response, $vendor) {
                // dd($invoice->amount_paid);
                if($invoice->subscription === $response->stripe_id){
                    $metaInfo = [
                        'amount_paid' => number_format($invoice->amount_paid /100),
                        'attempt_count' => $invoice->attempt_count,
                        'attempted' => $invoice->attempted,
                        'paid_date' => date('d/m/Y', $invoice->created),
                        'customer_email' => $invoice->customer_email,
                        'hosted_invoice_url' => $invoice->hosted_invoice_url,
                        'invoice_pdf' => $invoice->invoice_pdf,
                        'period_start' => date('d/m/Y', $invoice->period_start),
                        'period_end' => date('d/m/Y', $invoice->period_end),
                        'payment_status' => $invoice->status,
                    ];
                    DB::table('subscriptions')->where([
                        ['stripe_id', $response->stripe_id],
                        ['truck_subscription_id', $vendor->truck_id]
                    ])->update($metaInfo);
                }
            });
        } catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
        
    }

    public function CancelPlan(Request $request)
    {
        try {
            $getActivePlan = $request->all();
            $vendor = truck_subscription::find(1);
            // auth('api')->user();

            $rules = [
                'plan_key' =>'required',
                'plan_types' => 'required',
                'cancel_plan'=>'required',
            ];
            $validator = Validator::make($getActivePlan,$rules);
            if($validator->fails()){
                $status = FALSE;
                $code = '302';
                $message = $validator->errors()->all();
            }

            $cancelPlan = $request->cancel_plan;
            $planId = $request->plan_key;
            $planTypes = $request->plan_types;
            $data = new \stdClass;

            if (!$vendor->subscription($planTypes, $planId)->ended()) {
                $status = true;
                $code = '200';
                $message = 'Please subscribe first.';
            }
            if(!empty($cancelPlan)){
                $status = $vendor->subscription('main', $planId)->cancelNow();
                if(!empty($status)){
                    $status = true;
                    $code = '200';
                    $message = 'Successfully unsubscribe to the plan.';
                }else{
                    $status = true;
                    $code = '200';
                    $message = 'Unable to find the plan.';
                }
            }
            return $response = $this->responseData($data = $data, $status = $status ,$code = $code, $message = $message);
        } catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Proble while canceling plan');
        }
       
    }   

    public function subscribedHere($vendor, $planId, $planType, $paymentMethodId)
    {
        try {
            $cardInfo = $paymentMethodId;
            if ($vendor->hasPaymentMethod()) {
                $paymentMethod = $vendor->defaultPaymentMethod();
                $paymentMethodId = $paymentMethod->id;
                $cardInfo = $paymentMethodId;
            }
            if ($planType == 'basic'){
                $status = $vendor->newSubscription($planType, $planId)->trialUntil(Carbon::now()->addDays(90))->create($cardInfo, [
                    'email' => 'mkyadav59@gmail.com',
                ], 
                [
                    'metadata' => ['note' => 'Thaks for subscribing.'],
                ]);
            }else{
                $status = $vendor->newSubscription($planType, $planId)->create($cardInfo, [
                    'email' => 'mkyadav59@gmail.com',
                ], 
                [
                    'metadata' => ['note' => 'Thaks for subscribing.'],
                ]);
            } 
            return $status;
            
        } catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '302',$message = "Fail subscribe.");
        }
        
    }

    public function subscriptionSave(Request $request)
    {
       try{
            $getCard = $request->all();
            $rules = [
                'payment_method' => 'required',
                'plan_types' => 'required',
                'plan_key' =>'required',
                'price' =>'required',
            ];
            $validator = Validator::make($getCard,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '302',$message = $validator->errors()->all());
            }
            $vendor = truck_subscription::find(1);    
            $planId = $request->plan_key;
            $planType = $request->plan_types;
            $paymentMethodId = $request->payment_method;
            
            if ($vendor->subscribed($planType, $planId)) {
                $status = FALSE ;
                $code = '200';
                $message = 'You have already subscribe to '.$planType.' plan';
            }else{
                $existingPlan = [];
                if ($vendor->subscriptions()->active()->count() > 0){
                    foreach($vendor->subscriptions as $plan){
                        if($plan->stripe_status == 'active'){
                            $status = $vendor->subscription($plan->name, $plan->stripe_id)->cancelNow();
                            $existingPlan[$plan->name] = $plan->stripe_plan;
                        }
                    }
                }
                try {
                    $response = $this->subscribedHere($vendor, $planId, $planType, $paymentMethodId);
                    if($response){
                        $status = True ;
                        $code = '200';
                        $message = 'Successfully subscribe to '.$planType.' plan';
                        // $this->Invoice($vendor, $response);
                    }else{
                        $status = True ;
                        $code = '300';
                        $message = 'Fail to subscribe';
                    }

                } catch (IncompletePayment $exception) {
                    return redirect()->route(
                        'cashier.payment',
                        [$exception->payment->id, 'redirect' => route('define route')]
                    );
                }
            }
            return $response = $this->responseData($data = new \stdClass, $status = $status ,$code = $code,$message = $message);
       }catch(Exception $ex){
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');

       }
        
    }   
    
    public function subscriptionPlanInfo()
    {
        try {
            $vendor = truck_subscription::find(1);
            //auth('api')->user()->createSetupIntent();
            $plansraw = $this->stripe->plans->all();
            $plans = $plansraw->data;
            $product = [];
            foreach($plans as $plan) {
                $add_prod = [];
                if($plan->active){
                    $prod = $this->stripe->products->retrieve($plan->product);
                    if($prod['active']){
                        $add_prod['name'] = $prod['name'];
                        $add_prod['plan_types'] = !empty($plan['nickname']) ? $plan['nickname'] : '';            
                        $add_prod['id'] = $plan['id'];
                        $add_prod['interval'] = !empty($plan['interval']) ? $plan['interval'] : 0;
                        $add_prod['interval_count'] = !empty($plan['interval_count']) ? $plan['interval_count'] : 0;
                        $add_prod['currency'] = !empty($plan['currency']) ? $plan['currency'] : '';
                        $add_prod['amount'] = !empty($plan['amount']) ? number_format($plan['amount']/100) : 0;
                        $product[] = $add_prod;
                    }
                }
            }
            $data = [
                'intent' =>$vendor->createSetupIntent(),
                'plans' => $product,
                'cancel_plan' => 'Cancel',
            ];
            return view('subscriptions.plan', compact('data'));
        } catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
        return view('payment')->with($data);
    }

    public function subscriptionPricingInfo()
    {   
        try {
            $plansraw = $this->stripe->plans->all();
            $plans = $plansraw->data;
            $price = [];
            foreach($plans as $plan) {
                $add_prod = [];
                if($plan->active){
                    $prod = $this->stripe->products->retrieve($plan->product);
                    if($prod['active']){
                        $price[$plan['nickname']][] = !empty($plan['amount']) ? number_format($plan['amount']/100) : 0;
                        $price[$plan['nickname']][] = $plan['id'];
                    }
                }
            }
            return $price;
        } catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '302',$message = "Fial to get plan price data.");
        }
    }

    public function subscriptionCheckoutInfo(Request $request)
    {   
        try {
            $getCard = $request->all();
            $rules = [
                'price' => 'required',
                'plan_id' => 'required',
                'plan_name'=>'required',
            ];
    
            $validator = Validator::make($getCard,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '302',$message = $validator->errors()->all());
            }
            $vendor = truck_subscription::find(1);
            $data = [
                'intent'=>$vendor->createSetupIntent(),
                'price'=>$request->price,
                'plan_id'=>$request->plan_id,
                'plan_name'=>$request->plan_name
            ];
            //auth('api')->user()->createSetupIntent();
            return view('subscriptions.checkout', compact('data'));
        } catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '302',$message = "Fail to proceed on payment on checkout page.");
        }
    }

    public function paymentSuccess(){
        return view('subscriptions.success');
    }
}
