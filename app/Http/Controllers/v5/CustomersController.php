<?php
 
namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use AWS;
use DB, Exception, Validator, Log;
use App\customer, App\truck, App\otp, App\customer_truck, App\token, App\customerdevicetoken;

class CustomersController extends BaseController
{
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrData = $request->all();
        
        $rules = array(
            'name' => 'required',
            'mobile' => 'required|digits:10',
            'country_code' => 'required'
        );
        $validator = Validator::make($arrData,$rules);
        if($validator->fails()){
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
        }
        //split name if firstname lastname provided 
        // $name = explode(' ', $arrData['name']);
        // $firstname = (!empty($name) > 1) ? $name[0] :  $arrData['name'];
        // $lastname = (!empty($name) > 1) ? $name[1] :  '';
        
        if(empty($arrData['country_code'])){
            $arrData['country_code'] = 1;
        }

        $regCustomer = customer::where(['mobile' => $arrData['mobile'], 'active' => 1] )->get(['id', 'firstname'])->first();
        
        //if customer already registered;
        if(!empty($regCustomer)){
            //update customer name
            $customer = customer::find($regCustomer->id);
            $customer->firstname = $arrData['name'];
            //$customer->lastname = $lastname;
            $customer->mobile = $arrData['mobile'];
            $customer->country_code = $arrData['country_code'];

            $customer->save();

        }else{
            //insert in to database
            $customer = new customer;
            $customer->firstname = $arrData['name'];
            //$customer->lastname = $lastname;
            $customer->mobile = $arrData['mobile'];
            $customer->country_code = $arrData['country_code'];
            $customer->active = 1;
            
            $customer->save();
        }

        // send otp
        if(!empty($customer)){
            $resData['id'] = $customer->id;
            // $resData['otp'] = (new OtpController)->sendOtp($customer->id, 'customer');

            $resData['otp'] = (int) DB::select("CALL saveOtp('".$arrData['mobile']."',".$customer->id." , 'customer')")[0]->OTP;

            $message = "Your One-Time Password(OTP) is ".$resData['otp']." for VTM. Please do not share this password with anyone - GrilledChili";

            $sms = AWS::createClient('sns');
    
            $sms->publish([
                    'Message' => $message,
                    'PhoneNumber' => $arrData['country_code'].$arrData['mobile'], 
                    'MessageAttributes' => [
                        'AWS.SNS.SMS.SMSType'  => [
                            'DataType'    => 'String',
                            'StringValue' => 'Transactional',
                         ]
                     ],
                  ]);

        }else{
             return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Please check mobile number');
        }

        
        return $response = $this->responseData($data = $resData, $status = true ,$code = '200',$message = 'Hello '.$customer->firstname. ', an otp has been sent to this mobile number. (+1) '.$arrData['mobile']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            $customer = DB::table('customers')->where(['id' => $id, 'active'=> 1] )->get()->first();

                if(!empty($customer)){
                    $resData['customer'] = $customer;

                    $trucks = truck::where(['customer_id'=> $customer->id, 'active' => 1])->get(['id','name','logo']);
                    $resData['trucks'] = $trucks;

                    if(!empty($trucks)){
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'Vendor details.');
                    }else{
                        return $response = $this->responseData($date = $resData, $status = true ,$code = '200',$message = 'No trucks registered.');
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Your account has been suspended.');
                }


            if(!empty($customer)){
                return $response = $this->responseData($data = $arrResponse,$status = true ,$code = '200',$message = 'Vendor Details');
            }else{
                 return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'invalid customer id');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

   

    public function updateCustomer(Request $request)
    {
        try
        {

            $arrData = $request->all();
            $rules = array(
                'id' => 'required',
                'name' => 'required',
                'email' => 'email'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            //split name if firstname lastname provided 
            $name = explode(' ', $arrData['name']);
            $firstname = (!empty($name) > 1) ? $name[0] :  $arrData['name'];
            $lastname = (!empty($name) > 1) ? $name[1] :  '';
            
            //insert in to database
            $customer = customer::find($arrData['id']);
            if(!empty($customer)){
                $customer->firstname = $firstname;
                $customer->lastname = $lastname;
                $customer->email = (!empty($arrData['email']) && $arrData['email'] != '') ? $arrData['email'] : null;
                $customer->latitude = (!empty($arrData['latitude']) && $arrData['latitude'] != '') ? $arrData['latitude'] : null;
                $customer->longitude = (!empty($arrData['longitude']) && $arrData['longitude'] != '') ? $arrData['longitude'] : null;
                $customer->save();
            }
            else{
                 return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'invalid customer id');
            }

            return $response = $this->responseData($data = $customer,$status = true ,$code = '200',$message = 'Proceed');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    //set truck as favourite

    public function setFavouriteTruck(Request $request)
    {
        try
        {

            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
				'favourite' => 'required'
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            if(isset($arrData['favourite']) && $arrData['favourite'] == 1 ) {
            
				$ct = new customer_truck; 

                $ct->customer_id = $arrData['customer_id'];
                $ct->truck_id = $arrData['truck_id'];
                $ct->save();
                
                return $response = $this->responseData($data = $ct, $status = true ,$code = '200',$message = 'truck added as favourite');
            }else{

                $ct = customer_truck::where(['customer_id' => $arrData['customer_id'], 'truck_id' => $arrData['truck_id'], 'favourite' => 1])->first();

				if(!empty($ct)){
					$ctt = customer_truck::find($ct['id']);
					$ctt->favourite = 0;
					$ctt->customer_id = $arrData['customer_id'];
					$ctt->truck_id = $arrData['truck_id'];
					$ctt->save();
				
					return $response = $this->responseData($data = $ctt, $status = true ,$code = '200',$message = 'Removed from your favourite list.');
				}
				else{
					 return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Truck is not in favourite list.');
				}
            }
        }
        catch (Exception $ex) {
        Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }



    public function storeDeviceToken(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'app_type' => 'required',
                'device_type' => 'required',
                'device_token' => 'required',
                //'hash' => 'required', FIRST TIME THIS WILL BE NULL
                'datetime' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            if(!empty($arrData['hash'])){

                $arrTruckData = DB::table('customerdevicetokens')->where(['customer_id' => $arrData['customer_id'], 'hash' => $arrData['hash']])->get(['id', 'active'])->first();

                if(!empty($arrTruckData)){

                    if($arrTruckData->active == 1){

                        $version = DB::table('versions')->where(['type' => $arrData['app_type'], 'device_type' => $arrData['device_type'] ])->orderBy('id', 'desc')->first(['version','mandatory']);

                        $dc = customerdevicetoken::find($arrTruckData->id);

                        $dc->customer_id = $arrData['customer_id'];
                        $dc->device_type = $arrData['device_type'];
                        $dc->token = $arrData['device_token'];
                        $dc->updated_at = $arrData['datetime'];

                        $dc->save();

                        $data['device_token'] = $dc;
                        $data['version'] = $version;

                        return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token updated.');
                    }else{
                        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Your session got expired, Please login again.");  
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Hash not found.");   
                }
            }
            else
            {
                $tc = customerdevicetoken::where('customer_id', $arrData['customer_id'] )->delete();

                $dc = new customerdevicetoken; 

                $dc->customer_id = $arrData['customer_id'];
                $dc->device_type = $arrData['device_type'];
                $dc->token = $arrData['device_token'];
                $dc->hash = md5($arrData['datetime']);
                $dc->created_at = $arrData['datetime'];
                $dc->updated_at = $arrData['datetime'];
                $dc->save();
                
                $data['device_token'] = $dc;
                $data['version'] = new \stdClass;

                return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token added.');

            }
        }
        catch (Exception $ex) {
        Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 

    }


}
