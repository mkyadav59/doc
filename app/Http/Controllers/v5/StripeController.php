<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception,Validator, Log;
use App\vendor, App\truck, App\truckdevicetoken, App\order, App\order_payment;


class StripeController extends BaseController
{

    public function getStripeKeys(Request $request)
    {
        try{
            $responseData['publish_key']    = $_ENV['STRIPE_KEY'];
            $responseData['secret_key']     = $_ENV['STRIPE_SECRET'];
            $responseData['client_id']      = $_ENV['STRIPE_CLIENT_ID'];

            return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'stripe keys');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    // Can not delete Standard account 
    // public function deleteAccount(Request $request)
    // {
    //     try{

    //         $arrData = $request->all();
    //         $rules = array(
    //             'truck_id' => 'required',
    //             'stripe_connect_id' => 'required',
    //         );

    //         $validator = Validator::make($arrData,$rules);
    //         if($validator->fails()){
    //             return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
    //         }

    //             $stripe =  new \Stripe\StripeClient($_ENV['STRIPE_SECRET']);

    //             $result = $stripe->accounts->delete(
    //               $arrData['stripe_connect_id'],
    //               []
    //             );

    //             print_r($result);

    //             if($result->deleted == true){
    //                 $truck = truck::find((int) $arrData['truck_id']);

    //                 $truck->stripe_connect_id = '';
                    
    //                 $truck->stripe_status = 0;

    //                 $truck->save();
    //             }



    //     } catch(Exception $ex) {
    //         Log::error("Stripe->terminalConnectionToken-> ".$ex->getMessage());
    //         return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
    //     }                   

    // }

    ///////  TERMINAL CODE  //////////////////////////////////////////////////////////


    public function terminalConnectionToken(Request $request)
    {
        try{

            $stripe = new \Stripe\StripeClient($_ENV['STRIPE_SECRET']);

            $connectionToken = $stripe->terminal->connectionTokens->create();



            $responseData['connectionToken'] = $connectionToken->secret;

            return $response = $this->responseData($data =$responseData,$status = true ,$code = '200',$message = 'Terminal connectionToken generated.');

        } catch(Exception $ex) {
            Log::error("Stripe->terminalConnectionToken-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }

    }


    public function terminalConnectionTokenForConnect(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = truck::where(['id'=>$arrData['truck_id']])->get(['name','stripe_connect_id'])->first();

            $stripe = new \Stripe\StripeClient($_ENV['STRIPE_SECRET']);

            $location = $stripe->terminal->locations->create([
                                      'display_name' => $truck['name']."_Reader",
                                      'address' => [
                                        'country' => "US",
                                      ]
                                    ], [
                                      'stripe_account' => $truck['stripe_connect_id']
                                    ]);


            $connectionToken = $stripe->terminal->connectionTokens->create(
                                    [
                                      //'location' => $location
                                    ], 
                                    [
                                      'stripe_account' => $truck['stripe_connect_id'],
                                    ]);



            $responseData['connectionToken'] = $connectionToken->secret;

            return $response = $this->responseData($data =$responseData,$status = true ,$code = '200',$message = 'Terminal connectionToken generated.');

        } catch(Exception $ex) {
            Log::error("Stripe->terminalConnectionToken-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }

    }


    public function terminalRegistration(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'registration_code' => 'required',
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }


            $stripe = new \Stripe\StripeClient($_ENV['STRIPE_SECRET']);

            $location = $stripe->terminal->locations->create([
                                      'display_name' => "GrilledChili_Reader",
                                      'address' => [
                                        'country' => "US",
                                      ]
                                    ]);


            $reader = $stripe->terminal->readers->create([
                                      'registration_code' => $arrData['registration_code'],
                                      'label' => "GrilledChili_Reader",
                                      'location' => $location,
                                    ]);

            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'Reader registrered.');

        } catch(Exception $ex) {
            Log::error("Stripe->terminalRegistration-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = "Stripe->terminalRegistration-> ".$ex->getMessage());
        }
    }




    public function terminalRegistrationForConnect(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'registration_code' => 'required',
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = truck::where(['id'=>$arrData['truck_id']])->get(['name','stripe_connect_id'])->first();

            $stripe = new \Stripe\StripeClient($_ENV['STRIPE_SECRET']);

            // $location = $stripe->terminal->locations->create([
            //                           'display_name' => $truck['name']."_Reader",
            //                           'address' => [
            //                             'country' => "US",
            //                           ]
            //                         ], [
            //                           'stripe_account' => $truck['stripe_connect_id']
            //                         ]);


            $reader = $stripe->terminal->readers->create([
                                      'registration_code' => $arrData['registration_code'],
                                      'label' => $truck['name']."_Reader",
                                      //'location' => $location,
                                    ], [
                                      'stripe_account' => $truck['stripe_connect_id']
                                    ]);


            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'Reader registrered.');

        } catch(Exception $ex) {
            Log::error("Stripe->terminalRegistration-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = "Stripe->terminalRegistration-> ".$ex->getMessage());
        }
    }



    public function allReaders(Request $request)
    {       
        $stripe = new \Stripe\StripeClient($_ENV['STRIPE_SECRET']);
        $readers = $stripe->terminal->readers->all(['limit' => 3]);
        return $response = $this->responseData($data = $readers,$status = true ,$code = '200',$message = 'Reader registrered.');
    }

    /////  TERMINAL CODE  //////////////////////////////////////////////////////////




    public function OLD_getConnectLink(Request $request)
    {
        try{

            \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

            $account = \Stripe\Account::create([
              'type' => 'standard',
            ]);


            $account_links = \Stripe\AccountLink::create([
              'account' => $account,
              'refresh_url' => url('/').'/api/v4/stripe/link_expired',
              'return_url' => url('/').'/api/v4/stripe/succeeded',
              'type' => 'account_onboarding',
            ]);


            $responseData = $account_links;

            return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'stripe keys');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



    public function getConnectLink(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'hash' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $responseData['url'] =  "https://connect.stripe.com/oauth/authorize?state=".$arrData['hash']."&client_id=".$_ENV['STRIPE_CLIENT_ID']."&response_type=code&scope=read_write";

            return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'stripe keys');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }
    
   
    public function oauthAuthorize(Request $request)
    {
        try{
            $arrData = $request->all();

            //match state
            if(!empty($arrData['state'])){

                $result = truckdevicetoken::where(['hash' => $arrData['state']])->get(['truck_id'])->first();

                
                if(!empty($result['truck_id'])){

                    \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

                    $stripe_response = \Stripe\OAuth::token([
                      'grant_type' => 'authorization_code',
                      'code' => $arrData['code']
                    ]);

                    $truck = truck::find((int) $result['truck_id']);

                    $truck->stripe_connect_id = $stripe_response->stripe_user_id;
                    
                    $truck->stripe_status = 0;

                    $truck->save();


                    echo "<h1>Congratulations, You can now accept card payments from Grilledchili app. (Go to settings and enable Stripe Srevice.)</h1>"; exit();

                }else{

                        return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'state not matching');
                }
            }else{

                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'state code missing.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function getPaymentIntent(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'order_id' => 'required',
                'tip' => 'required',
                'sub_total' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = truck::where(['id'=>$arrData['truck_id']])->get(['stripe_connect_id'])->first();

            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 0,'order_accepted' => 0, 'order_rejected' => 0, 'order_cancelled' => 0])->first();

            if(!empty($order)){

                $arrData['sub_total'] = $arrData['sub_total'] * 100;
                $arrData['tip'] = $arrData['tip'] * 100;

                $amount = $arrData['sub_total'] + $arrData['tip'];

                if(!empty($arrData['tax']))
                    $amount = $amount + ($arrData['tax'] * 100);
                
                \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);


                if((int)$_ENV['STRIPE_APPLICATION_FEES'] > 0){
                    $payment_intent = \Stripe\PaymentIntent::create([
                      'payment_method_types' => ['card'],
                      'amount' => $amount,
                      'currency' => 'usd',
                      'application_fee_amount' => (int)$_ENV['STRIPE_APPLICATION_FEES'],
                      'metadata' => [
                            'order_id' => $arrData['order_id'],
                      ],
                    ], ['stripe_account' =>  $truck['stripe_connect_id'] ]);
                    $client_secret = $payment_intent->client_secret;

                }else{
                    $payment_intent = \Stripe\PaymentIntent::create([
                      'payment_method_types' => ['card'],
                      'amount' => $amount,
                      'currency' => 'usd',
                      'metadata' => [
                            'order_id' => $arrData['order_id'],
                      ],
                    ], ['stripe_account' =>  $truck['stripe_connect_id'] ]);
                    $client_secret = $payment_intent->client_secret;
                }

                $responseData['client_secret'] = $client_secret;

                return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'paymentIntent');
                

            }
            else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Order might be already placed or rejected');
            }

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function getPaymentIntentForTerminal(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'order_id' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = truck::where(['id'=>$arrData['truck_id']])->get(['stripe_connect_id'])->first();

            $order = order::where(['id' => $arrData['order_id']])->first();

            if(!empty($order)){

                $amount = $order->sub_total + $order->tip + $order->tax;
                
                \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);


                if((int)$_ENV['STRIPE_APPLICATION_FEES'] > 0){
                    $payment_intent = \Stripe\PaymentIntent::create([
                      'payment_method_types' => ['card_present'],
                      'capture_method' => 'manual',
                      'amount' => $amount,
                      'currency' => 'usd',
                      'application_fee_amount' => (int)$_ENV['STRIPE_APPLICATION_FEES'],
                      'metadata' => [
                            'order_id' => $arrData['order_id'],
                      ],
                    ], ['stripe_account' =>  $truck['stripe_connect_id'] ]);
                    $client_secret = $payment_intent->client_secret;

                }else{
                    $payment_intent = \Stripe\PaymentIntent::create([
                      'payment_method_types' => ['card_present'],
                      'capture_method' => 'manual',
                      'amount' => $amount,
                      'currency' => 'usd',
                      'metadata' => [
                            'order_id' => $arrData['order_id'],
                      ],
                    ], ['stripe_account' =>  $truck['stripe_connect_id'] ]);
                    $client_secret = $payment_intent->client_secret;
                }

                $responseData['client_secret'] = $client_secret;

                return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'paymentIntent');
                

            }
            else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Order might be already placed or rejected');
            }

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function capturePaymentIntent(Request $request)
    {       
        try{

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'order_id' => 'required',
                'payment_intent_id' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = truck::where(['id'=> $arrData['truck_id']])->get(['stripe_connect_id'])->first();

            $order = order::where(['id' => $arrData['order_id']])->first();

            $amount = $order->sub_total + $order->tip + $order->tax;
                    
            \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

            $intent = \Stripe\PaymentIntent::retrieve(
                                        $arrData['payment_intent_id']
                                        , ['stripe_account' =>  $truck['stripe_connect_id']]
                                    );
            $result = $intent->capture([
                                        'application_fee_amount' => (int)$_ENV['STRIPE_APPLICATION_FEES'],
                                    ]);

            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'Payment succeeded');
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data =  new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function refundOrder($order_id)
    {
        try{

            $order = order::where(['id' => $order_id, 'order_placed' => 1, 'order_paid' => 1])->first();


            $paymentObj = order_payment::where(['order_id' =>$order_id])->get(['payment_intent_id'])->first();

            \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

            $refund = \Stripe\Refund::create([
                'payment_intent' => $paymentObj->payment_intent_id,
                'reason' => 'requested_by_customer',
                'refund_application_fee' => true,
                'reverse_transfer' => true,
                'metadata' => [
                                'order_id' => $order_id,
                              ],
            ]);

            return $refund->status;
           

        }catch (Exception $ex) {
            Log::error("StripeController->refundOrder-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 

    }



    public function webhook(Request $request)
    {   

        $payload = @file_get_contents('php://input');
        $event = null;
        
        try{
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );

            // Handle the event
            switch ($event->type) {
                case 'account.updated':
                        Log::error('account.updated');
                        Log::error($event);
                case 'application_fee.created':
                        $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent

                        $orderPayment = new order_payment;

                        $orderPayment->order_id             = 10000;
                        $orderPayment->payment_intent_id    = $event->data->object->charge;
                        //$orderPayment->payment_method_id    = $event->data->object->payment_method;
                        //$orderPayment->receipt_url          = $event->data->object->charges->data[0]['receipt_url'];
                        $orderPayment->payment_json_object  = $event;

                        $orderPayment->save();
                    break;
                case 'payment_intent.succeeded':
                        $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent

                        $orderPayment = new order_payment;

                        $orderPayment->order_id             = $event->data->object->metadata->order_id;
                        $orderPayment->payment_intent_id    = $event->data->object->id;
                        $orderPayment->payment_method_id    = $event->data->object->payment_method;
                        $orderPayment->receipt_url          = $event->data->object->charges->data[0]['receipt_url'];
                        $orderPayment->payment_json_object  = $event;

                        $orderPayment->save();
                    break;
                case 'payment_method.payment_failed':
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Payment did not go through, Please try after sometime.');
                    
                    break;
                // ... handle other event types
                default:
                    // Unexpected event type
                    exit();
            }

            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'paymentIntent webhook callback success');

        } catch(Exception $ex) {
            Log::error("Stripe->weebhook-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }

    }
}
