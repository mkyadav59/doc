<?php

namespace App\Http\Controllers\v5;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB, Exception, Validator, Log;
use Illuminate\Support\Facades\Hash;
use App\Models\VendorRegistration;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\vendor;
use Carbon\Carbon;
use App\Mail\VendorRegistrationMail;

class VendorRegistrationController extends BaseController
{
    public function storeVendor(Request $request){
        try {
                $getVendorDetails = $request->all();
                $rules = [
                    'name' => 'required',
                    'mobile' => 'required|digits:10',
                    'email' => 'required|email',
                ];
                $validator = Validator::make($getVendorDetails,$rules);
                if($validator->fails()){
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
                }
    
                # Verify Vendor
                $vendor = VendorRegistration::where(['email'=>$request->email, 'mobile'=>$request->mobile])->get()->toArray();
                
                $name = $request->name;
                $mobile =$request->mobile;
                $email = $request->email;

                $generatePassword = Hash::make(Str::random(8));
                $token = (string) Str::uuid(); 
            
                if(!empty($vendor)){
                    $status = FALSE;
                    $code = '200';
                    $message = 'Vendor has already registered';
                }else{
                    $insertGetId  = VendorRegistration::insertGetId([
                        'name'=> $request->name,
                        'mobile'=> $request->mobile,
                        'email'=> $request->email,
                        'password'=> bcrypt($generatePassword),
                        'created_at'=>Carbon::now(),
                        'email_verify_token'=>$token,
                    ]);
                }

                if(!empty($insertGetId)){
                    $status = TRUE;
                    $code = '200';
                    $message = 'Vendor registerd successfully';
                    $emailVerificationLink = route("email-verification-link", ["token"=>$token]);
                    $data = ([
                        'name' => $name,
                        'email' => $mobile,
                        'mobile' => $email,
                        'password' => $generatePassword,
                        'verify_link' => $emailVerificationLink,
                    ]);

                    Mail::to($email)->send(new VendorRegistrationMail($data));
                }
                return $response = $this->responseData($data = new \stdClass, $status = $status, $code = $code, $message = $message);
                
            }catch (Exception $e) {
                Log::error($ex);
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
            } 
    }

    public function verifyEmail(Request $request){
        $getToken = $request->token;
        if(!empty($getToken)){
            $checkToken = VendorRegistration::where('email_verify_token', $getToken)->first();
            if(!empty($checkToken)){
                if(empty($checkToken->email_verify)){
                    $status = TRUE;
                    $code = '200';
                    $message = 'Email address verify successfully';

                    $checkToken->email_verify = Carbon::now();
                    $checkToken->email_verify_status = 1;
                    $checkToken->save();
                }else{
                    $status = FALSE;
                    $code = '404';
                    $message = 'Link has been expired';
                }
            }else{
                $code = '301';
                $status = FALSE;
                $message = 'Invalid url token';
            }
        }
        return $response = $this->responseData($data = new \stdClass, $status = $status, $code = $code, $message = $message);

    }
}
