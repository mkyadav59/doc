<?php

namespace App\Http\Controllers\v4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Illuminate\Notifications\Notifiable;
use Edujugon\PushNotification\PushNotification;

use DB, Exception, Validator, DateTime, Log;
use App\vendor, App\truck, App\item, App\customer, App\order, App\item_order, App\order_payment;
use App\customerdevicetoken,  App\truckdevicetoken;


use AWS;

class OrdersController extends BaseController
{
   

    public function addToCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
                'order_id' => 'required',
                'item_id' => 'required',
                'quantity' => 'required',
                'price' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            //check if desired quantity is available in stock or not 
            // $stockStatus = item::find($arrData['item_id']);
            // if($stockStatus->instock >= $arrData['quantity'])
            // {   
            //     //$stockStatus->instock = $stockStatus->instock - $arrData['quantity'];

            //     //$stockStatus->save();
            // }
            // else{
            //     throw new Exception('only '.$stockStatus->instock.' available in stock.');
            // }
            $truck = DB::table('trucks')->where(['id'=> $request->truck_id, 'on_off' => 1])->get(['id']);
            if(!empty($truck) > 0){

                if($arrData['order_id'] == 0){
                    //insert in to database
                    $order = new order;
                    $order->customer_id = $arrData['customer_id'];
                    $order->truck_id = $arrData['truck_id'];
                    $order->pick_up_time = 0;
                    $order->total = 0;
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->save();

                    $arrData['order_id'] = $order->id;
                }


                if($arrData['order_id'] > 0){

                    //UPDATE ITEM
                    //check if item is already exists 
                    $updateItem = item_order::where(['order_id' => $arrData['order_id'], 'item_id' => $arrData['item_id'], 'note' => $arrData['note'], 'customizations'=> $arrData['customizations'] ])->first();
                    //print_r($itemOrder['id']);exit();
                    if(!empty($updateItem) > 0){

                        //$updateItem = item_order::find($itemOrder['id']);
                        $updateItem->price = $arrData['price'] * 100;
                        
                        if(empty($arrData['customizations']) || empty($arrData['note']) ) {    
                            $updateItem->quantity = $updateItem->quantity+$arrData['quantity'];
                            $updateItem->customizations = $arrData['customizations'];
                            $updateItem->note = $arrData['note'];
                        }else
                        {
                            $updateItem->quantity = $arrData['quantity'];
                            $updateItem->customizations = $arrData['customizations'];
                            $updateItem->note = $arrData['note'];
                        }


                        $updateItem->save();
                        
                    }
                    else{

                        $item_order = new item_order;
                        $item_order->order_id = $arrData['order_id'];
                        $item_order->item_id = $arrData['item_id'];
                        $item_order->quantity = $arrData['quantity'];
                        $item_order->price = $arrData['price'] * 100;
                        $item_order->created_at = $arrData['datetime'];
                        $item_order->updated_at = $arrData['datetime'];
                        
                        if(!empty($arrData['customizations']))
                            $item_order->customizations = $arrData['customizations'];

                        if(!empty($arrData['note']))
                            $item_order->note = $arrData['note'];

                        $item_order->save();

                    }
        
                    
                    //update order amount 
                    $amount = item_order::where(['order_id' => $arrData['order_id']])
                        ->get( 
                            array(
                                DB::raw('sum(price * quantity) AS total')
                            )
                        );

                    // $order = order::where('id', $arrData['order_id'])->first();
                    // $order->total = $amount[0]->total;
                    // $order->created_at = $arrData['datetime'];
                    // $order->updated_at = $arrData['datetime'];
                    // $order->save();
                    

                    $arrResponse = order::where('id', $arrData['order_id'])->get(['id'])->first();

                    $arrResponse['itemsInCart'] =(int) item_order::where(['order_id' => $arrData['order_id']])->sum('quantity');
                   
                    return $response = $this->responseData($data =  $arrResponse , $status = true ,$code = '200',$message = 'Cart updated successfully');

                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading order');
                }
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'At present not accepting orders.');
            }   
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



    public function updateToCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'id' => 'required',
                'quantity' => 'required',
                'price' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            //---------------check item in stock---------------------
            $item = DB::table('items')
                        ->join('item_order', 'items.id', '=', 'item_order.item_id')
                        ->where('item_order.id',  $arrData['id'])
                        ->get(['instock'])->first();         

            if($item->instock < $arrData['quantity']){
             
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Only ".$item->instock. " left in stock");
            }

            //-------------------------------

       
            $updateItem = item_order::find($arrData['id']);

            if($updateItem){
                $updateItem->quantity = $arrData['quantity'];
                $updateItem->price = $arrData['price'] * 100;
                
                if(!empty($arrData['customizations']))
                    $updateItem->customizations = $arrData['customizations'];

                if(!empty($arrData['note']))
                    $updateItem->note = $arrData['note'];

                $result = $updateItem->save();

                if($result){
                return $response = $this->responseData($data =  $updateItem , $status = true ,$code = '200',$message = 'Order updated successfully');

                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading order');
                }
            }
            else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Provided Item id not in the list');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



    public function showCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'order_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $cart_items = DB::table('items')
                        ->join('item_order', 'items.id', '=', 'item_order.item_id')
                        ->select('item_order.id','items.id as item_id','items.name','items.description','item_order.price', 'item_order.quantity', 'item_order.note', 'item_order.customizations')
                        ->where('item_order.order_id',  $arrData['order_id'])
                        ->get();
            
            foreach ($cart_items as $c) {
                $c->price = round($c->price / 100, 2);
            }

            return $response = $this->responseData($data =  $cart_items , $status = true ,$code = '200',$message = 'item of cart');
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

    public function removeCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            $deleteItem = item_order::find($arrData['id']);
                If($deleteItem){
                    $deleteItem->delete();
                }
                else
                {
                    return $response = $this->responseData($data =  new \stdClass , $status = true ,$code = '200',$message = 'Item already deleted.');
                }
            return $response = $this->responseData($data =  new \stdClass , $status = true ,$code = '200',$message = 'Cart item removed.');
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

    public function cartItemQty(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            
            $cartQty = (int) item_order::where(['order_id' => $arrData['order_id']])->sum('quantity');
            
            return $response = $this->responseData($data =  $cartQty , $status = true ,$code = '200',$message = '');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function updateOrderJson($id)
    {
        try 
        {
            //update order json
             $o= DB::table('orders')
                                    ->join('customers', 'orders.customer_id', '=', 'customers.id')
                                    ->select('orders.id', 'customers.firstname', 'customers.mobile', 'truck_id', 'pick_up_time', 'note', 'tip', 'tax', 'total', 'server_id', 'server_name', 'service_table_id', 'service_table_name','pickup_station_id', 'pickup_station_name','order_status', 'order_rejected_remark','order_placed_time', 'order_paid', 'payment_method') 
                                    ->where(['orders.id' => $id])
                                    ->first();


            $o->id = (int)$o->id;
            $o->firstname = $o->firstname;
            $o->truck_id = (int)$o->truck_id;
            $o->pick_up_time = date("h:iA", strtotime($o->order_placed_time . "+".$o->pick_up_time." minutes"));
            $o->tip = round($o->tip / 100, 2);
            $o->tax = round($o->tax / 100, 2);
            $o->total = round($o->total /100, 2);


            

            $json= new \stdClass;
            $json->details = $o;

            $orderItems = DB::table('item_order')
                            ->join('items', 'items.id', '=', 'item_order.item_id')
                            ->select( 'items.id as item_id','items.name', 'item_order.quantity', 'item_order.price','item_order.note', 'item_order.customizations')
                            ->where('item_order.order_id',  $id)
                            ->get();
            foreach ($orderItems as $i) {
                $i->price = round($i->price / 100, 2);
            }
            $json->items = $orderItems;

            return json_encode($json);
        }catch (Exception $ex) {
           //Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

    //Check Item stock before credicard pay
    public function checkItemStock(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }


            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 0])->first(); 
                
            if(!empty($order) && $order->order_cancelled == 0)
            {
                //************DEDUCT STOCK**********************
                $response_message = "";
                $arrItems=[];
                $orderItems = DB::table('item_order')
                    ->join('items', 'items.id', '=', 'item_order.item_id')
                    ->where('item_order.order_id',  $order->id)
                    ->get();//item_order::where(['order_id' => $arrData['order_id']])->get();

                foreach ($orderItems as $items) {
                    //check
                    $requestedStock = $items->quantity;

                    if($requestedStock > 0 ){

                        $item = item::where(["id" => $items->item_id])->first();
                        

                        if($item->instock >= $requestedStock){
                            $item->requestedStock = $requestedStock;
                            $arrItems[]= $item;
                        }else{

                            // if this item is the only one item in the cart then that cart al
                            if($item->instock == 0){
                                
                                $response_message .= $item->name ." is sold out. ";
                                
                            }
                            else{
                                $response_message .= $item->name ." only ".$item->instock." left in stock. Please update your order.";
                                
                            }
                        }
                    }
                }

                if(!empty($response_message)){
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $response_message);
                }else{
                    return $response = $this->responseData($data =  new \stdClass , $status = true ,$code = '200',$message = 'All items are in stock.');
                }

            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Please provide valid order id.");
            }

        }catch (Exception $ex) {
           //Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }

    }




    public function placeOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
                'order_id' => 'required',
                'pickup_option' => 'required',
                'tip' => 'required',
                'sub_total' => 'required',
                'total' => 'required',
                'datetime' => 'required',
                'lat' => 'required',
                'long' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            
            $truck = DB::table('trucks')->where(['id'=> $request->truck_id, 'on_off' => 1])->first();


            if(!empty($truck)) {

                // SKIP CUSTOMER RANGE CHECK IF HE IS PAYING ONLINE
                if(empty($arrData['payment_success'])){
                    //check user distance from the truck
                    $lat1 = $truck->latitude;
                    $lon1 = $truck->longitude;
                    $lat2 = $request->lat;
                    $lon2 = $request->long;

                    $theta = $lon1 - $lon2;
                    $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = round(($miles * 60 * 1.1515), 2);

                    if($miles > 5)
                    {
                        return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Please make sure you are within 5 mile range to place order.');   
                    }
                }



                $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 0])->first(); 
                
                if(!empty($order) && $order->order_cancelled == 0)
                {
                    //************DEDUCT STOCK**********************
                    $response_message = "";
                    $arrItems=[];
                    $orderItems = DB::table('item_order')
                        ->join('items', 'items.id', '=', 'item_order.item_id')
                        ->where('item_order.order_id',  $order->id)
                        ->get();//item_order::where(['order_id' => $arrData['order_id']])->get();

                    foreach ($orderItems as $items) {
                        //check
                        $requestedStock = $items->quantity;

                        if($requestedStock > 0 ){

                            $item = item::where(["id" => $items->item_id])->first();
                            

                            if($item->instock >= $requestedStock){
                                $item->requestedStock = $requestedStock;
                                $arrItems[]= $item;
                            }else{

                                // if this item is the only one item in the cart then that cart al
                                if($item->instock == 0){
                                    
                                    $response_message .= $item->name ." is sold out. ";
                                    
                                }
                                else{
                                    $response_message .= $item->name ." only ".$item->instock." left. Please change order.";
                                    
                                }
                            }
                        }
                    }

                    if(!empty($response_message)){
                        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $response_message);
                    }

                    


                    //DEDUCT STOCK ON PAYMENT SUCCES
                    foreach ($arrItems as $item) {
                        $item->instock = $item->instock - $item->requestedStock;

                        unset($item->requestedStock);

                        $item->save();
                    }
                    //*****************************

                    $order->pick_up_time = $arrData['pickup_option'];
                    $d = new DateTime($arrData['datetime']);
                    $d->modify("+{$arrData['pickup_option']} minutes");
                    $order->order_wanted_time = $d->format('Y-m-d H:i:s');
                    $order->tip = $arrData['tip'] * 100;
                    $order->sub_total = $arrData['sub_total'] * 100;
                    $order->total = $arrData['total'] * 100;
                    $order->order_status = 'Placed';
                    $order->order_placed = 1;

                    if(!empty($arrData['tax']) && $arrData['tax'] > 0)
                        $order->tax = $arrData['tax'] * 100;

                    if(!empty($arrData['note']))
                        $order->note = $arrData['note'];

                    if(isset($arrData['servicetable_id']))
                        $order->service_table_id = $arrData['servicetable_id'];

                    if(isset($arrData['servicetable_name'])){
                        $order->service_table_name = $arrData['servicetable_name'];
                        $order->note =  (isset($order->note)) ? ($arrData['servicetable_name'].' - '.$order->note) : $arrData['servicetable_name'];
                    }

                    //STORE PAYMENT DETAILS
                    if(!empty($arrData['payment_success'])){
                        $order->order_paid = 1;
                        $order->payment_method = "CARD";
                    }

                    $order->order_placed_time = $arrData['datetime'];


                    // $order->lat = $arrData['lat'];
                    // $order->long = $arrData['long'];

                    $order->lat = $truck->latitude;
                    $order->long = $truck->longitude;

                    
                    $order->save();


                    //Update order JSON
                    
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();

                        
                    $order->truck_name = $truck->name;
                    $order->phone = $truck->phone;

                    $res['order'] = $order;
                    $res['items'] = $orderItems;

                    $arrCustToken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id])->orderBy('id', 'desc')->first();
                    

                    //SEND PUSH NOTIFICATION

                    if($arrCustToken->device_type == 'android'){
                            $push = new PushNotification('fcm');
                            $push->setMessage([
                                            'data' => [
                                                 'title'=>'Order #'.$arrData['order_id'].' placed',
                                                 'body'=>'You will receive acceptance from merchant.',
                                                 'order_id' => $arrData['order_id']
                                                 ]

                                         ])
                            ->setDevicesToken($arrCustToken->token)
                            ->send()
                            ->getFeedback();
                    }
                    else{

                        $push = new PushNotification('apn');

                        $message = [
                            'aps' => [
                                'alert' => [
                                    'title'=>'Order #'.$arrData['order_id'].' placed',
                                    'body'=>'You will receive acceptance from merchant.',
                                    'order_id' => $arrData['order_id']
                                ],
                                'badge' => 1,
                                'sound' => 'default'

                            ]
                        ];
                        
                        $result = $push->setMessage($message)
                            ->setDevicesToken($arrCustToken->token)
                            ->send()
                            ->getFeedback();

                        //DELETE BROKEN TOKENS FROM DATABASE
                        if(!empty($result->tokenFailList)) {
                            foreach ($result->tokenFailList as $token) {
                                truckdevicetoken::where(['token' => $token])->delete();
                            }
                        }

                        $result->device_token = $arrCustToken->token;

                    }


                    

                    $arrTruckToken = DB::table('truckdevicetokens')
                                ->where(['truck_id' => $arrData['truck_id']])
                                ->orderBy('id', 'desc')->pluck('token')->toArray(); 
                                  
                    if(!empty($arrTruckToken)){
                        $push = new PushNotification('apn');
                        
                        $push->setConfig([
                            'dry_run' => env('DRY_RUN', FALSE),
                            'passPhrase' => '1234',
                            'certificate' => base_path() . '/vendor/edujugon/push-notification/src/Config/iosCertificates/M_Certificate.pem',
                        ]); 

                        $message = [
                            'aps' => [
                                'alert' => [
                                    'title' => 'New Order #'.$arrData['order_id'],
                                    'body' => 'Please check there is new order.'
                                ],
                                'badge' => 1,
                                'sound' => 'default'

                            ]
                        ];

                        $result = $push->setMessage($message)
                            ->setDevicesToken($arrTruckToken)
                            ->send()
                            ->getFeedback();

                        if(!empty($result->tokenFailList)) {
                            foreach ($result->tokenFailList as $token) {
                                truckdevicetoken::where(['token' => $token])->delete();
                            }
                        }
                    }
        
                return $response = $this->responseData($data = $res, $status = true ,$code = '200',$message = 'Thank you for placing the order. Please look out for an order acceptance message from the food merchant within 15 min.');
                
                } elseif(!empty($order) && $order->order_cancelled == 1){
                    return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Your session expired. Please add items to your cart again.');
                } else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Order already placed');
                }

            } else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'At present not accepting orders.');
            }            
        } catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



    public function orderDetailById($id)
    {
        try 
        {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            $order = DB::table('orders')
                    ->join('trucks', 'orders.truck_id', '=', 'trucks.id')
                    ->select('orders.id', 'orders.truck_id','trucks.name as truck_name', 'trucks.phone', 'pick_up_time', 'note', 'sub_total', 'tip', 'tax','total', 'order_status', 'order_rejected_remark', 'server_name','service_table_name', 'pickup_station_name','order_placed_time' , 'order_paid', 'payment_method') 
                    ->where(['orders.id' => $id, 'order_placed' => 1])
                    ->get()->first();


            if(!empty($order)){



                $order->sub_total   = round($order->sub_total / 100,2);
                $order->tip         = round($order->tip / 100, 2);
                $order->tax         = round($order->tax / 100, 2);
                $order->total       = round($order->total /100, 2);

                $orderPayment = DB::table('order_payment')
                                ->where(['order_id' => $order->id])
                                ->get(['receipt_url'])->first();

                if(!empty($orderPayment)){
                    $order->receipt_url = $orderPayment->receipt_url;
                }

                $resData['order'] = $order;


                $items =  DB::table('item_order')
                        ->join('items', 'items.id', '=', 'item_order.item_id')
                        ->select('item_order.order_id', 'items.id','items.name', 'item_order.order_id', 'item_order.quantity', 'item_order.price','item_order.note', 'item_order.customizations')
                        ->where('item_order.order_id',  $order->id)
                        ->get();


                

                if(!empty($items)){
                    foreach ($items as $i) {
                        $i->price = round($i->price / 100, 2);
                    }
                    $resData['items'] = $items;
                }
                else{
                    $resData['items'] = [];
                }

                return $response = $this->responseData($data = $resData, $status = true ,$code = '200',$message = 'Order details');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Incorrect Order ID');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function pastOrdersByCustomerId($id)
    {
        try 
        {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            // $orders = customer::find($id)
            //             ->orders()
            //             ->join('trucks', 'orders.truck_id', '=', 'trucks.id')
            //             ->where(['order_placed' => 1])->orderBy('id', 'DESC')
            //             ->limit(100)
            //             ->get(['orders.id', 'truck_id', 'trucks.name', 'pick_up_time', 'note', 'total', 'order_status', 'order_placed_time']);

            $orders = DB::table('orders')
                        ->select('orders.id', 'truck_id', 'trucks.name', 'pick_up_time', 'note', 'total', 'order_status','order_placed_time')
                        ->join('trucks', 'orders.truck_id', '=', 'trucks.id')
                        ->where([ 'customer_id' => $id ,'order_placed' => 1])
                        ->orderBy('id', 'DESC')
                        ->limit(100)
                        ->get();
            foreach ($orders as $o) {
                $o->total = round($o->total / 100, 2);
            }

            return $response = $this->responseData($data = $orders, $status = true ,$code = '200',$message = 'Past Orders');
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function pastOrdersByTruckId(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }


            $arrOrders['order_placed'] = [];
            $arrOrders['order_accepted'] = [];
            $arrOrders['order_ready'] = [];
            $arrOrders['order_delivered'] = [];
            $arrOrders['order_rejected'] = [];
            $arrOrders['order_cancelled'] = [];


            //OCT 7 9:00AM CHANGE 
            $arrData['datetime'] = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($arrData['datetime'])));

          
            $orders = DB::select("CALL getPlacedOrders('".$arrData['truck_id']."', '".$arrData['datetime']."')");

            foreach ($orders as $o) {
                array_push($arrOrders['order_placed'],json_decode($o->order_json));
            }



            $allOrders = DB::select("CALL getAcceptedOrders('".$arrData['truck_id']."', '".$arrData['datetime']."')");

            foreach ($allOrders as $o2) {
                if($o2->order_status == 'Accepted'){

                    array_push($arrOrders['order_accepted'],json_decode($o2->order_json));

                }elseif($o2->order_status == 'Ready'){

                    array_push($arrOrders['order_ready'],json_decode($o2->order_json));

                }elseif ($o2->order_status == "Delivered") {

                    array_push($arrOrders['order_delivered'],json_decode($o2->order_json));

                }elseif ($o2->order_status == "Rejected") {

                    array_push($arrOrders['order_rejected'],json_decode($o2->order_json));

                }elseif ($o2->order_status == "Cancelled") {

                    array_push($arrOrders['order_cancelled'],json_decode($o2->order_json));
                }
            }
            

            $arrResponse['orders'] = $arrOrders;


            //out fo stock items
            $arrResponse['items'] = item::where(['truck_id' => $arrData['truck_id'], 'active' => 1])
                                        ->where('instock', '<', '10' )
                                        ->orderBy('instock' , 'asc')
                                        ->get(['id', 'name', 'instock']);

            return $response = $this->responseData($data = $arrResponse, $status = true ,$code = '200',$message = 'Past Orders..');
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }




    public function acceptOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();

            if(!empty($order) && $order->order_accepted == 0 && $order->order_rejected == 0 && $order->order_cancelled == 0)
            {

                if(isset($arrData['server_id'])){
                    $order->server_id = (int) $arrData['server_id'];
                }

                if(isset($arrData['server_name'])){
                    $order->note = $arrData['server_name'].'-'.$order->note;
                }

                $order->order_status = 'Accepted';
                $order->order_status_notification = "Your order accepted, Expect a message when ready for pickup. ";
                $order->order_accepted = 1;
                $order->order_accepted_time = $arrData['datetime'];
                $order->order_acceptance_interval = Date( "i:s",strtotime($arrData['datetime']) - strtotime($order->order_placed_time));
                $order->updated_at = $arrData['datetime'];

                $order->save();

                //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();
            
            
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id])->orderBy('id', 'desc')->first();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' accepted',
                                             'body'=>'Expect a message when ready for pickup.',
                                             'order_id' => $arrData['order_id'],
                                             'order_status' => 'Accepted'
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' accepted',
                                'body'=>'Expect a message when ready for pickup.',
                                'order_id' => $arrData['order_id'],
                                'order_status' => 'Accepted'
                            ],
                                'badge' => 1,
                                'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();

                    if(!empty($result->tokenFailList)) {
                        foreach ($result->tokenFailList as $token) {
                            truckdevicetoken::where(['token' => $token])->delete();
                        }
                    }
                    
                    $result->device_token = $arrtoken->token;
                }
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order accepted successfully.');
            }elseif($order->order_accepted == 1){
                return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Order already accepted.');

            }elseif($order->order_rejected == 1){
                return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Order already rejected.');
            }elseif($order->order_cancelled == 1){
                return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Order cancelled by customer.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }




    public function readyOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }


            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1, 'order_accepted' => 1])->first();
            
            if($order->order_rejected == 1){
                return $response = $this->responseData($data = $order,$status = FALSE ,$code = '221',$message = "Order already rejected.");
            }


            if(!empty($order) && $order->order_ready == 0)
            {   
                $pickup_station_name = '';
                if($order->pickup_station_id > 0){
                    $pickup_station_name = 'from '.$order->pickup_station_name;
                }

                $order->order_status = 'Ready';
                $order->order_status_notification = "Your order ready, Please come and pickup your order.";
                $order->order_ready = 1;
                $order->order_ready_time = $arrData['datetime'];
                $order->order_ready_interval = Date( "i:s", strtotime($order->pick_up_time) - strtotime($arrData['datetime']));
                $order->updated_at = $arrData['datetime'];
                $order->save();

                //Update order JSON
                    $order->created_at = $arrData['datetime'];
                    $order->updated_at = $arrData['datetime'];
                    $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                    $order->save();

                
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id])->orderBy('id', 'desc')->first();
                
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' ready',
                                             'body'=>'Please pickup your order'.$pickup_station_name,
                                             'order_id' => $arrData['order_id'],
                                             'order_status' => 'Ready'
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' ready',
                                'body'=>'Please pickup your order'.$pickup_station_name,
                                'order_id' => $arrData['order_id'],
                                'order_status' => 'Ready'
                            ],
                                'badge' => 1,
                                'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();

                    if(!empty($result->tokenFailList)) {
                        foreach ($result->tokenFailList as $token) {
                            truckdevicetoken::where(['token' => $token])->delete();
                        }
                    }

                    $result->device_token = $arrtoken->token;
                }

                
                if($order->server_id > 0) {

                    //SEND SMS TO SERVER

                    $objServer = DB::table('servers')
                                ->join('trucks', 'trucks.id', '=', 'servers.truck_id')
                                ->join('truck_settings', 'truck_settings.truck_id', '=', 'servers.truck_id')
                                ->where(['servers.truck_id' => $order->truck_id, 'servers.id' => $order->server_id,'truck_settings.servers' => 1])
                                ->get(['servers.country_code','servers.mobile','servers.send_sms','trucks.name'])
                                ->first();


                    if(isset($objServer)){

                        $message = "Order #".$order->id." is ready at ".$objServer->name." for delivery to ".$order->service_table_name;

                        $sms = AWS::createClient('sns');
            
                        $sms->publish([
                            'Message' => $message,
                            'PhoneNumber' => $objServer->country_code.$objServer->mobile, 
                            'MessageAttributes' => [
                                'AWS.SNS.SMS.SMSType'  => [
                                    'DataType'    => 'String',
                                    'StringValue' => 'Transactional',
                                    ]
                                ],
                            ]);
                    }

                }


            }else{
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order already ready.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order ready for pickup.');
            
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function reminderOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            

            $order = order::where(['id' => $arrData['order_id']])->first();
            


            if($order->order_delivered == 1){
                    return $response = $this->responseData($data = $order,$status = FALSE ,$code = '221',$message = "Already Delivered.");
            }

            if(!empty($order))
            {
                
                $pickup_station_name = '';
                if($order->pickup_station_id > 0){
                    $pickup_station_name = 'from '.$order->pickup_station_name;
                }

                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id])->orderBy('id', 'desc')->first();
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' ready',
                                             'body'=>'Please pickup your order'.$pickup_station_name,
                                             'order_id' => $arrData['order_id'],
                                             'order_status' => 'Ready'
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' ready',
                                'body'=>'Please pickup your order'.$pickup_station_name,
                                'order_id' => $arrData['order_id']
                            ],
                                'badge' => 1,
                                'sound' => 'default'
                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();

                    if(!empty($result->tokenFailList)) {
                        foreach ($result->tokenFailList as $token) {
                            truckdevicetoken::where(['token' => $token])->delete();
                        }
                    }

                    $result->device_token = $arrtoken->token;
                }
            }else{
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order already ready');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Notification sent to Customer.');
            
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function updatePaymentMethod(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'payment_method' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();

            // $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1, 'order_accepted' => 1, 'order_ready' => 1, 'order_delivered' => 0])->first();
            
            if(!empty($order))
            {
                $order->payment_method = $arrData['payment_method'];
                $order->updated_at = $arrData['datetime'];
                $order->save();

                //Update order JSON
                $order->created_at = $arrData['datetime'];
                $order->updated_at = $arrData['datetime'];
                $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                $order->save();

                
            }else{
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order already picked up.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Payment method has been updated.');
            
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function deliverOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1, 'order_accepted' => 1, 'order_ready' => 1, 'order_delivered' => 0])->first();
            
            if(!empty($order))
            {
                $order->order_status = 'Delivered';
                $order->order_status_notification = NULL;
                $order->order_delivered = 1;
                $order->order_delivered_time = $arrData['datetime'];
                $order->order_pickup_interval = Date( "i:s",strtotime($arrData['datetime']) - strtotime($order->order_ready_time));
                $order->updated_at = $arrData['datetime'];
                $order->save();

                //Update order JSON
                $order->created_at = $arrData['datetime'];
                $order->updated_at = $arrData['datetime'];
                $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                $order->save();

                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id])->orderBy('id', 'desc')->first();//->pluck('token', 'device_type')->toArray();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' delivered.',
                                             'body'=>'Thank you for your order. Enjoy your meal!',
                                             'order_id' => $arrData['order_id']
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' delivered.',
                                'body'=>'Thank you for your order. Enjoy your meal!',
                                'order_id' => $arrData['order_id']
                            ],
                                'badge' => 1,
                                'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();

                    if(!empty($result->tokenFailList)) {
                        foreach ($result->tokenFailList as $token) {
                            truckdevicetoken::where(['token' => $token])->delete();
                        }
                    }

                    $result->device_token = $arrtoken->token;
                }
            }else{
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order already picked up.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Please thank the customer. Tell them Enjoy your meal!!');
            
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    //Customer is going to cancel the order 
    //So pushnotification will be sent to Truck devices
    public function cancelOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();
            
            if(!empty($order))
            {
                
                if($order->order_rejected == 1){
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = $order->order_rejected_remark);
                }

                if($order->order_accepted == 1){
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Order already accepted, so can not be cancelled.');
                }

                $arrItems=[];
                $orderItems = item_order::where(['order_id' => $arrData['order_id']])->get();

                foreach ($orderItems as $items) {
                    
                    $requestedStock = $items->quantity;

                    if($requestedStock > 0 ){

                        $item = item::where(["id" => $items->item_id])->first();
                        
                        $item->instock = $item->instock + $requestedStock;
                        $item->save();

                    }
                }

                $order->order_status = 'Cancelled';
                $order->order_status_notification = NULL;
                $order->order_cancelled = 1;
                $order->order_cancelled_time = $arrData['datetime'];
                $order->order_cancelled_remark = (!empty($arrData['remark'])) ? $arrData['remark'] : Null;
                $order->updated_at = $arrData['datetime'];
                $order->save();


                //REUND IF ORDER PAID BY STRIPE
                $refundMsg = '';

                // if($order->payment_method == 'CARD'){
                    
                //     $stripObj = new StripeController();
                //     $result = $stripObj->refundOrder($arrData['order_id']);

                //     if($result == "succeeded"){

                //         $order->payment_method = 'REFUNDED';
                //         $order->save();
                        
                //         $refundMsg = " Refund initiated.";
                //     }
                // }


                //Update order JSON
                $order->created_at = $arrData['datetime'];
                $order->updated_at = $arrData['datetime'];
                $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                $order->save();


                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id])->orderBy('id', 'desc')->first();
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                            'title'=>'Order #'.$arrData['order_id'].' cancelled',
                                            'body'=>'You have cancelled your order!!',
                                            'order_id' => $arrData['order_id']
                                            ],
                                        'sound' => 'default'
                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' cancelled',
                                'body'=>'You have cancelled your order!!',
                                'order_id' => $arrData['order_id']
                            ],
                                'badge' => 1,
                                'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();

                    if(!empty($result->tokenFailList)) {
                        foreach ($result->tokenFailList as $token) {
                            truckdevicetoken::where(['token' => $token])->delete();
                        }
                    }

                    $result->device_token = $arrtoken->token;
                }


            
            }else{
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order not placed or not found.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order cancelled successfully..');
            
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }





    public function rejectOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();
            
            if(!empty($order))
            {
                if($order->order_ready == 1){
                    return $response = $this->responseData($data = $order,$status = FALSE ,$code = '221',$message = "order is ready, can not be rejected");
                }

                if($order->order_rejected == 1){
                    return $response = $this->responseData($data = $order,$status = FALSE ,$code = '221',$message = "Order already rejected");
                }

                $arrItems=[];
                $orderItems = item_order::where(['order_id' => $arrData['order_id']])->get();

                foreach ($orderItems as $items) {
                    
                    $requestedStock = $items->quantity;

                    if($requestedStock > 0 ){

                        $item = item::where(["id" => $items->item_id])->first();
                        
                        $item->instock = $item->instock + $requestedStock;
                        $item->save();

                    }
                }

                $order->order_status = 'Rejected';
                $order->order_status_notification = NULL;
                $order->order_rejected = 1;
                $order->order_rejected_time = $arrData['datetime'];
                $order->order_rejected_interval = Date( "i:s",strtotime($arrData['datetime']) - strtotime($order->order_placed_time));
                $order->order_rejected_remark = (!empty($arrData['remark'])) ? $arrData['remark'] : Null;
                $order->updated_at = $arrData['datetime'];
                $order->save();

               

                //REUND IF ORDER PAID BY STRIPE
                //$refundMsg = '';

                // if($order->payment_method == 'CARD'){
                    
                //     $stripObj = new StripeController();
                //     $result = $stripObj->refundOrder($arrData['order_id']);

                //     if($result == "succeeded"){

                //         $order->payment_method = 'REFUNDED';
                //         $order->save();
                        
                //         $refundMsg = " Refund initiated.";
                //     }
                // }

                $refundMsg = $order->order_rejected_remark; 

                if($order->payment_method == 'CARD'){
                    $refundMsg = $refundMsg. '(Refund will be processed in 24 hours.)';
                }

                 //Update order JSON
                $order->created_at = $arrData['datetime'];
                $order->updated_at = $arrData['datetime'];
                $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                $order->save();


                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id])->orderBy('id', 'desc')->first();
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' rejected',
                                             'body'=> $refundMsg,
                                             'order_id' => $arrData['order_id'],
                                             'order_status' => 'rejected'
                                             ],
                                        'sound' => 'default'

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' rejected',
                                'body'=>$order->order_rejected_remark,
                                'order_id' => $arrData['order_id'],
                                'order_status' => 'rejected'
                            ],
                                'badge' => 1,
                                'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();

                    if(!empty($result->tokenFailList)) {
                        foreach ($result->tokenFailList as $token) {
                            truckdevicetoken::where(['token' => $token])->delete();
                        }
                    }

                    $result->device_token = $arrtoken->token;
                }

            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Invalid order");
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'order rejected successfully.');
            
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }




    public function orderStatusNotification(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $order = order::where(['customer_id' => $arrData['customer_id'], 'order_placed' => 1, 'order_delivered' => 0])
                        ->get()
                        ->first();

            
            $resData['order_status'] = $order->order_status;
            $resData['notification'] = $order->order_status_notification;

            $updatedAt = $order->updated_at;

            $o = order::find($order->id);
            $o->order_status_notification = NULL;
            $o->updated_at = $updatedAt;
            $o->save();


            return $response = $this->responseData($data = $resData, $status = true ,$code = '200',$message = 'order status.');   
        
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function addServernStation(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required',
                'datetime' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();
            
            if(!empty($order))
            {
                if(isset($arrData['server_id']))
                    $order->server_id = $arrData['server_id'];

                if(isset($arrData['server_name']))
                    $order->server_name = $arrData['server_name'];

                
                if(isset($arrData['pickup_station_id']))
                    $order->pickup_station_id = $arrData['pickup_station_id'];

                if(isset($arrData['pickup_station_name']))
                    $order->pickup_station_name = $arrData['pickup_station_name'];


                
                $order->updated_at = $arrData['datetime'];
                $order->save();

               
                $order->updated_at = $arrData['datetime'];
                $order->order_json =  $this->updateOrderJson($arrData['order_id']);
                $order->save();


            } else {
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Invalid order");
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = '');
            
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }



    public function testNotification(Request $request)
    {
        try{

            $arrData = $request->all();


            if(!empty($arrData['sms'])){
                $sms = AWS::createClient('sns');
        
                $sms->publish([
                        'Message' => 'Hello, This is just a test Message',
                        'PhoneNumber' => $arrData['mobile'], 
                        'MessageAttributes' => [
                            'AWS.SNS.SMS.SMSType'  => [
                                'DataType'    => 'String',
                                'StringValue' => 'Transactional',
                             ]
                         ],
                      ]);
            }

            if(!empty($arrData['truck_id'])){
                $arrTruckToken = DB::table('truckdevicetokens')
                                    ->where(['truck_id' => $arrData['truck_id']])
                                    ->orderBy('id', 'desc')->pluck('token')->toArray(); 
                              
                if(!empty($arrTruckToken)){
                    $push = new PushNotification('apn');
                    
                    $push->setConfig([
                        'dry_run' => true,
                        'passPhrase' => '1234',
                        'certificate' => base_path() . '/vendor/edujugon/push-notification/src/Config/iosCertificates/M_Certificate.pem',
                    ]); 

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title' => 'Test Notification',
                                'body' => 'This is test notification message.'
                            ],
                            'badge' => 1,
                            'sound' => 'default'

                        ]
                    ];

                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrTruckToken)
                        ->send()
                        ->getFeedback();

                    foreach ($result->tokenFailList as $token) {
                        truckdevicetoken::where(['token' => $token])->delete();
                    }

                }
            }


            if(!empty($arrData['customer_id']))
            {   
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $arrData['customer_id'], 'active' => 1])->orderBy('id', 'desc')->first();
              
                      if($arrtoken->device_type == 'android'){
                              $push = new PushNotification('fcm');
                              $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #1 rejected',
                                             'body'=>'Order rejected for unavoidable reason.',
                                             'order_id' => 1,
                                             'order_status' => 'rejected'
                                             ],
                                        'sound' => 'default'

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                      }
                      else{

                              $push = new PushNotification('apn');

                              $message = [
                                  'aps' => [
                                  'alert' => [
                                      'title'=>'Order #TEST rejected',
                                      'body'=>'Order rejected for unavoidable reason.',
                                      
                                  ],
                                    'badge' => 1,
                                    'sound' => 'default'

                                  ]
                              ];
                              
                              $result = $push->setMessage($message)
                                  ->setDevicesToken($arrtoken->token)
                                  ->send()
                                  ->getFeedback();


                              print_r($result);
                        }
            }
        } catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


}
