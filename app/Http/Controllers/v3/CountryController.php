<?php

namespace App\Http\Controllers\v3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use App\country;

class CountryController extends BaseController
{
    public function getAllCountries(Request $request)
    {
    	try {

    		$countries = country::where(['active' => 1])->orderBy('order')->get(['name','country_code', 'icon']);

            foreach ($countries as $c) {
                $c->icon = url('/').'/uploads/country_icons/'.$c->icon;
            }

            $responseData['countries'] = $countries;
    		return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'All countries list.');
    	}catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }
}
