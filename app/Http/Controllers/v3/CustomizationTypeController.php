<?php

namespace App\Http\Controllers\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use DB, Exception, Validator, Log;
use App\item, App\customization_type,App\customization_option;

class CustomizationTypeController extends BaseController
{
    
    
  

    public function addCustomization(Request $request)
    {
        try{
            $arrData = $request->all();
            $rules = array(
                'item_id' => 'required',
                'name' => 'required',
                'required' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = implode(" ",$validator->errors()->all()));
            }
            
            //insert in to database
            $objCustomization = new customization_type;
            $objCustomization->item_id = (int)$arrData['item_id'];
            $objCustomization->name = $arrData['name'];
            $objCustomization->required = (int)$arrData['required'];
            $objCustomization->order = customization_type::where( ['item_id' => $arrData['item_id']] )->count() + 1;
            $objCustomization->created_at = $arrData['datetime'];
            $objCustomization->updated_at = $arrData['datetime'];
        
            $objCustomization->save();


            if($objCustomization->id > 0){

               
                if(!empty($arrData['options'])){
                    $arrOptions = explode(',', $arrData['options']);

                    foreach ($arrOptions as $option) {
                        $o = explode('-', $option);

                        $objCustomizationOption = new customization_option;

                        $objCustomizationOption->type_id = $objCustomization->id;
                        $objCustomizationOption->item_id = (int)$arrData['item_id'];
                        $objCustomizationOption->name = trim($o[0]);
                        $objCustomizationOption->price = $o[1] * 100;
                        $objCustomizationOption->active = 1;
                        $objCustomizationOption->created_at = $arrData['datetime'];
                        $objCustomizationOption->updated_at = $arrData['datetime'];
                    
                        $objCustomizationOption->save();
                    }
                }

                return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Customization added successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch(Exception $ex){
            
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function updateCustomization(Request $request)
    {
        try{
            $arrData = $request->all();
            $rules = array(
                'customization_id' => 'required',
                'item_id' => 'required',
                'name' => 'required',
                'required' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = implode(" ",$validator->errors()->all()));
            }
            
            //update in to database
            $objCustomization = customization_type::find($arrData['customization_id']);
            $objCustomization->item_id = (int)$arrData['item_id'];
            $objCustomization->name = $arrData['name'];
            $objCustomization->required = (int)$arrData['required'];
            $objCustomization->updated_at = $arrData['datetime'];
        
            $objCustomization->save();


            if($objCustomization->id > 0){

                $objDeleteCustomizationOption = customization_option::where(['type_id' => $objCustomization->id])->delete();

                if(!empty($arrData['options'])){
                    $arrOptions = explode(',', $arrData['options']);

                    foreach ($arrOptions as $option) {
                        $o = explode('-', $option);

                        $objCustomizationOption = new customization_option;

                        $objCustomizationOption->type_id = $objCustomization->id;
                        $objCustomizationOption->item_id = (int)$arrData['item_id'];
                        $objCustomizationOption->name = $o[0];
                        $objCustomizationOption->price = $o[1] * 100;
                        $objCustomizationOption->active = 1;
                        $objCustomizationOption->created_at = $arrData['datetime'];
                        $objCustomizationOption->updated_at = $arrData['datetime'];
                    
                        $objCustomizationOption->save();
                    }
                }

                return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Customization added successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch(Exception $ex){
            
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

    
    public function updateCustomizationStatus(Request $request)
    {
        try{
            $arrData = $request->all();
            $rules = array(
                'customization_id' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = implode(" ",$validator->errors()->all()));
            }
            
            //update in to database
            $objCustomization = customization_type::find($arrData['customization_id']);
            $objCustomization->active = (int)$arrData['active'];
            $objCustomization->updated_at = $arrData['datetime'];
        
            $objCustomization->save();


            return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Customization status updated successfully');
            
        }catch(Exception $ex){
            
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }
     
    public function moveUpCustomization(Request $request)
    {
        try{

            $arrData = $request->all();

            $rules = array(
                'customization_id' => 'required',
                'item_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $objCustomization = customization_type::find($arrData['customization_id']);

            $o = (int) $objCustomization->order - 1;
                $moveUp = customization_type::where(['item_id' => $arrData['item_id'], 'order' =>  $o])->first();

                if(!empty($moveUp)){
                    $moveUp->order  = $objCustomization->order;
                    $moveUp->updated_at = $arrData['datetime'];
                    $moveUp->save();

                    if($moveUp->id > 0){
                        $objCustomization->order = $o;
                        $objCustomization->updated_at = $arrData['datetime'];
                        $objCustomization->save();
                    }

                    return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Customization moved successfully');
                }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function moveDownCustomization(Request $request)
    {
        try{

            $arrData = $request->all();

            $rules = array(
                'customization_id' => 'required',
                'item_id' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $objCustomization = customization_type::find($arrData['customization_id']);

            $o = (int) $objCustomization->order + 1;
                $moveUp = customization_type::where(['item_id' => $arrData['item_id'], 'order' =>  $o])->first();

                if(!empty($moveUp)){
                    $moveUp->order  = $objCustomization->order;
                    $moveUp->updated_at = $arrData['datetime'];
                    $moveUp->save();

                    if($moveUp->id > 0){
                        $objCustomization->order = (int) $objCustomization->order + 1;
                        $objCustomization->updated_at = $arrData['datetime'];
                        $objCustomization->save();
                    }

                    return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Customization moved successfully');
                }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function deleteCustomization(Request $request)
    {
        try{
            $arrData = $request->all();
            $rules = array(
                'customization_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = implode(" ",$validator->errors()->all()));
            }
            
            //update in to database
            $objCustomization = customization_type::find($arrData['customization_id'])->delete();


            return $response = $this->responseData($data = $objCustomization, $status = true ,$code = '200',$message = 'Customization deleted successfully');
           
        }catch(Exception $ex){
            
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    /**
     * Get all customization by item id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allcustomizationsbyitemid($id)
    {
        try 
        {   

            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }
            $resData = array();
            $arrType = array();
            $arrOptions = array();    
            $arrCatList = array();

            $itemData = item::find($id);


            if(!empty($itemData)){

                $itemData->price = round($itemData->price / 100, 2);

                if($itemData->img != NULL){

                    $itemData->img =  url('/').'/uploads/items/'.$itemData->img;
                }
                else{
                    $itemData->img =  url('/').'/uploads/items/item_default.jpg';
                }
            }


            $arrCustomization_type = $customization_type = DB::table('customization_types')->where(['item_id'=> $id])->orderBy('order')->get();
            
            $customization_options = DB::table('customization_options')->where(['item_id'=>  $id])->orderBy('id', 'ASC')->get();
                
                foreach ($arrCustomization_type as $cat) {
                    $arrCat['id'] = $cat->id;
                    $arrCat['name'] = $cat->name;
                    $arrCat['required'] = $cat->required;
                    $arrCat['order'] = $cat->order;
                    $arrCat['active'] = $cat->active;
                    $arrCat['customization_options'] = [];

                    foreach ($customization_options as $o) {
                        if($o->type_id == $cat->id){

                            $o->price = round($o->price / 100, 2);

                            $arrCat['customization_options'][] = $o;        
                        }
                    }
                    if(!empty($arrCat['customization_options']) > 0)
                        $arrCatList[] = $arrCat;
                }

                $resData['detail'] = $itemData;
                $resData['customization_types'] = $arrCatList;

                return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'All customization types and their options belongs to item.');
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



}
