<?php

namespace App\Http\Controllers\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception,Validator, Log;
use App\vendor, App\truck, App\truckdevicetoken, App\order, App\order_payment;


class StripeController extends BaseController
{
    public function getStripeKeys(Request $request)
    {
        try{
            $responseData['publish_key']    = $_ENV['STRIPE_KEY'];
            $responseData['secret_key']     = $_ENV['STRIPE_SECRET'];
            $responseData['client_id']      = $_ENV['STRIPE_CLIENT_ID'];

            return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'stripe keys');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function Old_getConnectLink(Request $request)
    {
        try{

            \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

            $account = \Stripe\Account::create([
              'type' => 'standard',
            ]);


            $account_links = \Stripe\AccountLink::create([
              'account' => $account,
              'refresh_url' => url('/').'/api/v3/stripe/refreshurl',
              'return_url' => url('/').'/api/v3/stripe/returnurl',
              'type' => 'account_onboarding',
            ]);


            $responseData = $account_links;

            return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'stripe keys');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



    public function getConnectLink(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'hash' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $responseData['url'] =  "https://connect.stripe.com/oauth/authorize?state=".$arrData['hash']."&client_id=".$_ENV['STRIPE_CLIENT_ID']."&response_type=code&scope=read_write";

            return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'stripe keys');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }
    
   
    public function oauthAuthorize(Request $request)
    {
        try{
            $arrData = $request->all();

            //match state
            if(!empty($arrData['state'])){

                $result = truckdevicetoken::where(['hash' => $arrData['state']])->get(['truck_id'])->first();

                
                if(!empty($result['truck_id'])){

                    \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

                    $stripe_response = \Stripe\OAuth::token([
                      'grant_type' => 'authorization_code',
                      'code' => $arrData['code']
                    ]);

                    $truck = truck::find((int) $result['truck_id']);

                    $truck->stripe_connect_id = $stripe_response->stripe_user_id;
                    
                    $truck->stripe_status = 0;

                    $truck->save();


                    echo "<h1>Congratulations! You can now accept card payments from your customers using GrilledChili app. <br/> Go to settings and enable Stripe Srevice.</h1>"; exit();

                }else{

                        return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'state not matching');
                }
            }else{

                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'state code missing.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function getPaymentIntent(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'order_id' => 'required',
                'tip' => 'required',
                'sub_total' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = truck::where(['id'=>$arrData['truck_id']])->get(['stripe_connect_id'])->first();

            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 0,'order_accepted' => 0, 'order_rejected' => 0, 'order_cancelled' => 0])->first();

            if(!empty($order)){

                $arrData['sub_total'] = $arrData['sub_total'] * 100;
                $arrData['tip'] = $arrData['tip'] * 100;

                $amount = $arrData['sub_total'] + $arrData['tip'];

                if(!empty($arrData['tax']))
                    $amount = $amount + ($arrData['tax'] * 100);

                \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);


                if((int)$_ENV['STRIPE_APPLICATION_FEES'] > 0){
                    $payment_intent = \Stripe\PaymentIntent::create([
                      'payment_method_types' => ['card'],
                      'amount' => $amount,
                      'currency' => 'usd',
                      'application_fee_amount' => (int)$_ENV['STRIPE_APPLICATION_FEES'],
                      'metadata' => [
                            'order_id' => $arrData['order_id'],
                      ],
                    ], ['stripe_account' =>  $truck['stripe_connect_id'] ]);
                    $client_secret = $payment_intent->client_secret;

                }else{
                    $payment_intent = \Stripe\PaymentIntent::create([
                      'payment_method_types' => ['card'],
                      'amount' => $amount,
                      'currency' => 'usd',
                      'metadata' => [
                            'order_id' => $arrData['order_id'],
                      ],
                    ], ['stripe_account' =>  $truck['stripe_connect_id'] ]);
                    $client_secret = $payment_intent->client_secret;
                }

                $responseData['client_secret'] = $client_secret;

                return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'paymentIntent');
                

            }
            else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Order might be already placed or rejected');
            }

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    public function refundOrder($order_id)
    {
        try{

            $order = order::where(['id' => $order_id, 'order_placed' => 1, 'order_paid' => 1])->first();


            $paymentObj = order_payment::where(['order_id' =>$order_id])->get(['payment_intent_id'])->first();

            \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

            $refund = \Stripe\Refund::create([
                'payment_intent' => $paymentObj->payment_intent_id,
                'reason' => 'requested_by_customer',
                'refund_application_fee' => true,
                'reverse_transfer' => true,
                'metadata' => [
                                'order_id' => $order_id,
                              ],
            ]);

            return $refund->status;
           

        }catch (Exception $ex) {
            Log::error("StripeController->refundOrder-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 

    }

    public function webhook(Request $request)
    {   

        $payload = @file_get_contents('php://input');
        $event = null;
        
        try{
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );

            // Handle the event
            switch ($event->type) {
                case 'application_fee.created':
                        $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent

                        $orderPayment = new order_payment;

                        $orderPayment->order_id             = 10000;
                        $orderPayment->payment_intent_id    = $event->data->object->charge;
                        //$orderPayment->payment_method_id    = $event->data->object->payment_method;
                        //$orderPayment->receipt_url          = $event->data->object->charges->data[0]['receipt_url'];
                        $orderPayment->payment_json_object  = $event;

                        $orderPayment->save();
                    break;
                case 'payment_intent.succeeded':
                        $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent

                        $orderPayment = new order_payment;

                        $orderPayment->order_id             = $event->data->object->metadata->order_id;
                        $orderPayment->payment_intent_id    = $event->data->object->id;
                        $orderPayment->payment_method_id    = $event->data->object->payment_method;
                        $orderPayment->receipt_url          = $event->data->object->charges->data[0]['receipt_url'];
                        $orderPayment->payment_json_object  = $event;

                        $orderPayment->save();
                    break;
                case 'payment_method.payment_failed':
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Payment did not go through, Please try after sometime.');
                    
                    break;
                // ... handle other event types
                default:
                    // Unexpected event type
                    exit();
            }

            return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'paymentIntent webhook callback success');

        } catch(Exception $ex) {
            Log::error("Stripe->weebhook-> ".$ex->getMessage());
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }

    }
}
