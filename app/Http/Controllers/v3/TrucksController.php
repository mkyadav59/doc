<?php

namespace App\Http\Controllers\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use Illuminate\Notifications\Notifiable;
use Edujugon\PushNotification\PushNotification;

use DB, Exception, Validator, Image, Log;
use App\vendor, App\truck, App\item, App\customer, App\order, App\category, App\truckdevicetoken, App\customer_truck, App\version;
use AWS;


class TrucksController extends BaseController
{
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'vendor_id' => 'required',
                'name' => 'required',
                'alias' =>'required',
                'description' => 'required',
                'phone' => 'required|digits:10',
                'datetime' => 'required'
            );
                


            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $truck = new truck;
            $truck->vendor_id = (int) $arrData['vendor_id'];
            $truck->name = $arrData['name'];
            $truck->alias = $arrData['alias'];

            $imgType = '';
            
            if(!empty($request->file('logo'))){
                $image = $request->file('logo') ;
                
                $guessExtension = $image->guessExtension();
                $fileName = time().'.'.$guessExtension;
                $path = public_path().'/uploads/logo/'.$fileName;
                $image = Image::make($image->getRealPath())->resize(768, 576)->save(sprintf($path));
                
                $truck->logo =  $fileName;
            }else{
                $truck->logo =  "logo_default.jpg";
            }

            $truck->description = $arrData['description'];
            $truck->phone = $arrData['phone'];

            if(!empty($arrData['tax_status']) && $arrData['tax_status'] == 1){
                $truck->tax_status = 1;
                $truck->tax_percentage = $arrData['tax_percentage'];
            }else{
                $truck->tax_status = 0;
                $truck->tax_percentage = 0;
            }

            $truck->operatingtime = date('h:iA', strtotime($arrData['datetime'])). ' TO 11:59PM';
            $truck->on_off = 1;
            
            $truck->created_at = $arrData['datetime'];
            $truck->updated_at = $arrData['datetime'];
            
            $truck->save();


            if($truck->id > 0){

				$this->createDummyCategories($truck->id);
                
                $truck->tax_percentage = (float) $truck->tax_percentage;
                $truck->logo = url('/').'/uploads/logo/'.$truck->logo;

                return $response = $this->responseData($data = $truck,$status = true ,$code = '200',$message = 'Truck added successfully..');
            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading truck');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    
    }



	public function createDummyCategories($id)
		{
		
			if($id > 0)
			{
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Burgers";
				$category2->short_description = "burgers";
				$category2->description = "burgers";
				$category2->order = '1';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Pizzas";
				$category2->short_description = "pizzas";
				$category2->description = "pizzas";
				$category2->order = '2';
				$category2->active = '1';
				$category2->save();


				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Salads";
				$category2->short_description = "salads";
				$category2->description = "salads";
				$category2->order = '3';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Sandwiches";
				$category2->short_description = "sandwiches";
				$category2->description = "sandwiches";
				$category2->order = '4';
				$category2->active = '1';
				$category2->save();

				
				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Tachos";
				$category2->short_description = "tachos";
				$category2->description = "tachos";
				$category2->order = '5';
				$category2->active = '1';
				$category2->save();

				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Sides";
				$category2->short_description = "sides";
				$category2->description = "sides";
				$category2->order = '6';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Desserts";
				$category2->short_description = "desserts";
				$category2->description = "desserts";
				$category2->order = '7';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Snacks";
				$category2->short_description = "snacks";
				$category2->description = "snacks";
				$category2->order = '8';
				$category2->active = '1';
				$category2->save();

				
				//insert in to database
				$category2 = new category;
				$category2->truck_id = $id;
				$category2->name = "Drinks";
				$category2->short_description = "drinks";
				$category2->description = "drinks";
				$category2->order = '9';
				$category2->active = '1';
				$category2->save();
			}

	}


    public function truckDetail(Request $request)
    {
        try 
        {

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'device_token' => 'required',
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $trucks = DB::table('trucks')->where(['id'=> $arrData['truck_id']])->get()->first();

            if(!empty($trucks)){
                
                $trucks->id = (int) $trucks->id;

                $trucks->vendor_id = (int) $trucks->vendor_id;
                $trucks->logo =  $trucks->logo;
                $trucks->active = (int) $trucks->active;
                $trucks->rating = (int) $trucks->rating;

                if(!empty($trucks->operatingtime)){
                    $operatingtime = explode(' ', $trucks->operatingtime);

                    $trucks->from_operatingtime = $operatingtime[0];
                    $trucks->to_operatingtime = $operatingtime[2];
                }else{
                    $trucks->from_operatingtime = null;
                    $trucks->to_operatingtime = null;
                }

                if($trucks->logo != null){
                    $trucks->logo =  url('/').'/uploads/logo/'.$trucks->logo;
                }
                else{
                    $trucks->logo =  url('/').'/uploads/logo/logo_default.jpg';
                }

                $result = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'token' => $arrData['device_token'] ])->get(['primary_device'])->first();
                if(!empty($result))
                    $trucks->primary_device = $result->primary_device;
                else
                    $trucks->primary_device = 0;


                return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message = 'Truck details');
            }else{
                 return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'invalid truck id');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function getMultiTrucks(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'mobile' => 'required|digits:10',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $vendor = vendor::where(['mobile' => $arrData['mobile'], 'active' => 1] )->get(['id', 'firstname'])->first();

            if(!empty($vendor))
            {
                $trucks = DB::table('trucks')
                            ->join('vendors', 'vendors.id', '=', 'trucks.vendor_id')
                            ->where(['vendors.mobile' => $arrData['mobile'] , 'trucks.active' => 1, 'vendors.active' => 1 ])
                            ->get([ 'trucks.id' ,'trucks.name', 'trucks.alias', 'trucks.logo']);


                if(count($trucks) > 0){         

                    foreach ($trucks as $t) {
                        if( !empty($t->logo) )
                            $t->logo =  url('/').'/uploads/logo/'.$t->logo;
                        else
                            $t->logo =  url('/').'/uploads/logo/logo_default.jpg';
                    }

                    $responseData['trucks'] = $trucks;

                    return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'All trucks');
                }else{
                    $responseData['trucks'] = [];
                    return $response = $this->responseData($data = $responseData,$status = true ,$code = '200',$message = 'No trucks found.');
                }
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'This Number is not registed as GrilledChili Merchant. Please enter valid mobile number.');
            }


        }catch (Exception $ex) {
            echo $ex;exit();
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    public function updateTruck(Request $request)
    {   
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'vendor_id' => 'required',
                'name' => 'required',
                'alias' =>'required',
                'description' => 'required',
                'phone' => 'required|digits:10',
                'datetime' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $truck = truck::find((int) $arrData['truck_id']);
            $truck->name = $arrData['name'];
            $truck->vendor_id = (int)$arrData['vendor_id'];
            // $truck->latitude = $arrData['latitude'];
            // $truck->longitude = $arrData['longitude'];
            $truck->alias = $arrData['alias'];
            $truck->description = $arrData['description'];
            $truck->phone = $arrData['phone'];
            // $truck->weekdaytime = $arrData['FromWeekday'].' TO '.$arrData['ToWeekday'];
            // $truck->weekendtime = $arrData['FromWeekend'].' TO '.$arrData['ToWeekend'];
            if(!empty($arrData['tax_status']) && $arrData['tax_status'] == 1){
                $truck->tax_status = 1;
                $truck->tax_percentage = $arrData['tax_percentage'];
            }else{
                $truck->tax_status = 0;
                $truck->tax_percentage = 0;
            }
            $truck->updated_at = $arrData['datetime'];

            if(!empty($request->file('logo'))){
                $image = $request->file('logo') ;
                
                $guessExtension = $image->guessExtension();
                $fileName = time().'.'.$guessExtension;
                $path = public_path().'/uploads/logo/'.$fileName;
                //$file->move($destinationPath,$fileName);
                $image = Image::make($image->getRealPath())->resize(768, 576)->save(sprintf($path));
                
                $truck->logo =  $fileName;
            }

            $truck->save();

            if($truck->id > 0){

                $trucks = DB::table('trucks')->where(['id'=> $truck->id])->get()->first();
                
                // $trucks_weekdaytime = $trucks->weekdaytime; 
                // $weekdaytime = explode(' ', $trucks_weekdaytime);

                // $trucks_weekendtime = $trucks->weekendtime; 
                // $weekendtime = explode(' ', $trucks_weekendtime);

                // $trucks->FromWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[0] : $trucks_weekdaytime;
                // $trucks->ToWeekday = ($weekdaytime[0] != 'Always') ? $weekdaytime[2] : $trucks_weekdaytime;
                // $trucks->FromWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[0] : $trucks_weekendtime;
                // $trucks->ToWeekend = ($weekendtime[0] != 'Always') ? $weekendtime[2] : $trucks_weekendtime;

                $trucks->id = (int) $trucks->id;
                $trucks->vendor_id = (int) $trucks->vendor_id;
                $trucks->logo =  $truck->logo;
                $trucks->active = (int) $trucks->active;
                $trucks->rating = (int) $trucks->rating;
                $trucks->tax_percentage = (float) $trucks->tax_percentage;

                return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message = 'Truck updated successfully');
            }else{
                Log::error($ex);
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading truck');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    

    //gettrucks
    public function getTrucksByRadious(Request $request)
    {
        try 
        {
			/*
				DROP PROCEDURE `trucksbyradious`;
				CREATE DEFINER=`root`@`localhost` PROCEDURE `trucksbyradious`(IN `v_latitude` VARCHAR(25), IN `v_longitude` VARCHAR(25), IN `v_radious` INT, IN `v_logo_url` VARCHAR(255)) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT
				id,
				name,
				address,
				phone,
				description,
				CONCAT( v_logo_url , logo) as logo,
				latitude,
				longitude,
				 ( 3959 * acos( cos( radians(v_latitude ) ) * cos( radians( latitude ) ) 
					* cos( radians( longitude ) - radians( v_longitude) ) + sin( radians(v_latitude ) ) * sin(radians(latitude)) ) ) AS distance 
				FROM trucks
				Where active = 0 OR active = 1 
				HAVING distance < v_radious
				ORDER BY distance 
          */
          
            $arrData = $request->all();
            $rules = array(
                'latitude' => 'required',
                'longitude' => 'required',
                'radious' => 'required',
                'logo_url' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            //$truck = DB::statement("call trucksbyradious(?,?,?,?)",$arrParams);
            $trucks['trucks'] = DB::select("CALL trucksbyradious('".$arrData['latitude']."','".$arrData['longitude']."', ".$arrData['radious'].", '".$arrData['logo_url']."' )");

           if(!empty($trucks['trucks'])){

			//favoirte trucks
			/*
			 if(!empty($arrData['customer_id'])){
				 $favouriteTrucks = customer::find($arrData['customer_id'])->trucks()->get(['trucks.id','trucks.name', 'trucks.address', 'trucks.phone', 'trucks.description', DB::raw("CONCAT( '".$arrData['logo_url']."' , trucks.logo) as logo"), 'trucks.latitude', 'trucks.longitude' ])->toArray();
				
				if(!empty( $favouriteTrucks)){
					 $trucks['trucks'] = array_merge($trucks['trucks'],$favouriteTrucks);
				}
			 }
			*/
			//echo "<pre>"; print_r($trucks); exit;
                return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message = 'Trucks within '.$arrData['radious'].' miles radious');
            }else{
                 return $response = $this->responseData($data = $trucks,$status = true ,$code = '200',$message ='Nearby No trucks found.');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }



    //url - gettruckdetail
    public function getTruckDetailsWithItems(Request $request)
    {
        try 
        {   
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = DB::table('trucks')->where(['id'=> $request->truck_id])->get();

            if(!empty($request->customer_id) ){
                $customer_truck = DB::table('customer_truck')->where(['customer_id' => $request->customer_id, 'truck_id' => $request->truck_id, 'favourite' => 1])->get()->first();

                if(!empty($customer_truck)){
                    $truck[0]->favourite = 1;
                }else{
                    $truck[0]->favourite = 0;
                }

            }else{
                    $truck[0]->favourite = 0;
            }

            if($truck[0]->logo != null)
                $truck[0]->logo =  url('/').'/uploads/logo/'.$truck[0]->logo;
            else
                $truck[0]->logo =  url('/').'/uploads/logo/logo_default.jpg';

            $resData['truck'] = $truck;
		
            if(!empty($truck))
            {

                $arrCategory = $category = DB::table('categories')->where(['truck_id'=> $request->truck_id, 'active' => 1])->orderBy('order')->get(['id', 'name']);
                $items = DB::table('items')
                        ->where(['truck_id'=>  $request->truck_id, 'active' =>  1])
                        ->orderBy('order')
                        ->get(['id', 'category_id', 'name', 'description', 'price', 'img', 'instock']);

                $arrCat = array();
                $arrItems = array();
                foreach ($arrCategory as $cat) {
                    $arrCat['categoryName'] = $cat->name;
                    $arrCat['items'] = [];
                    foreach ($items as $i) {
                        if($i->category_id == $cat->id){

                            $i->price = round($i->price / 100 , 2);
                            if($i->img != NULL)
                            {
                                $i->img =  url('/').'/uploads/items/'.$i->img;
                            }
                            else{
                                $i->img =  url('/').'/uploads/items/item_default.jpg';
                            }

                            $arrCat['items'][] = $i;        
                        }
                    }
                    $resData['category'][] = $arrCat;

                }

                

                if(!empty($request->customer_id)){
                    $order = order::where(['truck_id' => $request->truck_id,'customer_id' => $request->customer_id ,'order_placed' => 0, 'order_cancelled' => 0, 'order_rejected' => 0])->get(['id']);

                    if(!empty($order)){
                        $resData['order'] = $order;
                    }else{

                        $order = new \stdClass;
                        $order->id = 0;
                        $a[] = $order;
                        $resData['order'] = $a;
                    }
                }else{
                        $order = new \stdClass;
                        $order->id = 0;
                        $a[] = $order;
                        $resData['order'] = $a;
                }

                return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'Truck details');
            }else{
                 return $response = $this->responseData($data = $resData,$status = true ,$code = '200',$message = 'invalid truck request');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function setTruckLocation(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'hash' => 'required',
                'datetime' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            
            $truckDetails = DB::table('trucks')->where(['id'=> $arrData['truck_id']])
                                ->get(['on_off','latitude','longitude','operatingtime'])->first();

            if(!empty($truckDetails->operatingtime)){
                $operatingtime = explode(' ', $truckDetails->operatingtime);

                $truckDetails->from_operatingtime = $operatingtime[0];
                $truckDetails->to_operatingtime = $operatingtime[2];
            }else{
                $truckDetails->from_operatingtime = null;
                $truckDetails->to_operatingtime = null;
            }


            



            $result = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'hash' => $arrData['hash'] ])->get(['primary_device'])->first();
                
            if(!empty($result) && $result->primary_device == 1 && $truckDetails->on_off == 1){
                $truck = truck::find((int) $arrData['truck_id']);
                $truck->latitude = $arrData['latitude'];
                $truck->longitude = $arrData['longitude'];
                $truck->location_updated_at = time();

                $truck->save();


            }

            if(!empty($result)){
                $truckDetails->primary_device = $result->primary_device;
            }else{
                $truckDetails->primary_device = 0;
            }



            //CHECK TRUCK OPERATING TIME AND TURN OFF
            //SEND PUSH NOTIFICATION TO ALL TRUCK DEVICES

            $deviceTime = strtotime($arrData['datetime']);
            $truckEndTime = strtotime($truckDetails->to_operatingtime);
            //LOG::error($truckEndTime .'<='. $deviceTime);
            if($truckEndTime <= $deviceTime && $truckDetails->on_off == 1 && $result->primary_device == 1){
                

                //turn off the truck
                $truck = truck::find((int) $arrData['truck_id']);
                $truck->on_off = 0;
                $truck->updated_at = $arrData['datetime'];

                $truck->save();

                Log::error('truckEndTime=>'.$truckEndTime." <= ".'deviceTime=> '.$deviceTime );


                //as truck turned off now set the truck off 
                $truckDetails->on_off = 0;


                // send push notification
                $arrTruckToken = DB::table('truckdevicetokens')
                                ->where(['truck_id' => $arrData['truck_id']])
                                ->orderBy('id', 'desc')->pluck('token')->toArray(); 
                                  
                if(!empty($arrTruckToken)){
                    $push = new PushNotification('apn');
                    
                    $push->setConfig([
                        'dry_run' => env('DRY_RUN', FALSE),
                        'passPhrase' => '1234',
                        'certificate' => base_path() . '/vendor/edujugon/push-notification/src/Config/iosCertificates/M_Certificate.pem',
                    ]); 

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title' => 'Truck has been closed.',
                                'body' => 'Truck has been closed.',
                                'isTruckOn' => 0
                            ],
                            'badge' => 1,
                            'sound' => 'default'

                        ]
                    ];

                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrTruckToken)
                        ->send()
                        ->getFeedback();

                    if(!empty($result->tokenFailList)) {
                        foreach ($result->tokenFailList as $token) {
                            truckdevicetoken::where(['token' => $token])->delete();
                        }
                    }
                }

            }


            return $response = $this->responseData($data = $truckDetails,$status = true ,$code = '200',$message = '');


        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    public function getTruckLocation(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $primary_truck = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'primary_device' => 1])->get()->first();
            

            if(!empty($primary_truck)){
                $location = truck::where(['id'=>$arrData['truck_id']])->get(['latitude', 'longitude']);

                return $response = $this->responseData($data = $location,$status = true ,$code = '200',$message = 'Truck location updated successfully');
            }else{

                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'No primary device found, go to settings to make device primary.');
            }

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }



    public function setTruckOnOff(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'on_off' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            $truck = truck::find((int) $arrData['truck_id']);
            $truck->on_off = $arrData['on_off'];

            if($arrData['on_off'] == 1){

                //truck will be on for 5 min
                $operatingtime = explode(' ', $truck->operatingtime);

                $truck->operatingtime = $operatingtime[0]. ' TO '.date('h:iA', time() + 1800 );
                
                //UPDATE THE LOCATION TIME
                $truck->location_updated_at = time();


            }else{
                $operatingtime = explode(' ', $truck->operatingtime);

                $truck->operatingtime = $operatingtime[0]. ' TO '.date('h:iA', time());

            }

            $truck->save();

            if($truck->id > 0){
                $val = new \stdClass();
                $val->truck_id = (int) $truck->id;

				if($truck->on_off == 1)
					return $response = $this->responseData($data = $val,$status = true ,$code = '200',$message = 'Now you can accept the orders.');
				else
					return $response = $this->responseData($data = $val,$status = true ,$code = '200',$message = 'No order will be processed until you turn it on.');
            }else{
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while updating truck status.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    public function setTruckOperatingTime(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'from_operatingtime' => 'required',
                'to_operatingtime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $truck = truck::find((int) $arrData['truck_id']);

            $truck->on_off = 1;
            $truck->operatingtime = $arrData['from_operatingtime'].' TO '.$arrData['to_operatingtime'];

            $truck->save();

            return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Operating time is set to '.$truck->operatingtime ." .");

            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


    public function storeDeviceToken(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'app_type' => 'required',
                'device_type' => 'required',
                'device_token' => 'required',
                'datetime' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            if(!empty($arrData['hash'])){

                $arrTruckData = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'token' => $arrData['device_token'], 'hash' => $arrData['hash']])->get(['id', 'active'])->first();


                if(!empty($arrTruckData)){

                    $version = DB::table('versions')->where(['type' => $arrData['app_type'], 'device_type' => $arrData['device_type'] ])->orderBy('id', 'desc')->first(['version','mandatory']);

                    $dc = truckdevicetoken::find($arrTruckData->id);

                    $dc->updated_at = $arrData['datetime'];

                    $dc->save();

                    $data['device_token'] = $dc;
                    $data['version'] = $version;

                    return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token updated.');
                    
                }else{
                    $tc = truckdevicetoken::where('token', $arrData['device_token'])->delete();
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Your session get expired, Please relogin.");   
                }
            }


            //responsible to delete all previouse entries of same device truckdevicetokens
            $tc = truckdevicetoken::where('token', $arrData['device_token'])->delete();

            $primaryDeviceCount = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'primary_device' => 1])->get(['id'])->first();

            
            $dc = new truckdevicetoken; 

            $dc->truck_id = (int) $arrData['truck_id'];
            
            if(empty($primaryDeviceCount)){

                $dc->primary_device = 1;
            }

            $dc->device_type = $arrData['device_type'];
            $dc->token = $arrData['device_token'];
            $dc->hash = md5($arrData['datetime']);
            $dc->created_at = $arrData['datetime'];
            $dc->updated_at = $arrData['datetime'];
            $dc->save();

            $data['device_token'] = $dc;


            $version = DB::table('versions')->where(['type' => $arrData['app_type'], 'device_type' => $arrData['device_type'] ])->orderBy('id', 'desc')->first(['version','mandatory']);
            $data['version'] = $version;

            return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token added.');

            
        }
        catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function updateDeviceToken(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'app_type' => 'required',
                'device_type' => 'required',
                'device_token' => 'required',
                'hash' => 'required', 
                'datetime' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            if(!empty($arrData['hash'])){

                $arrTruckData = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'hash' => $arrData['hash']])->get(['id', 'active'])->first();


                if(!empty($arrTruckData)){

                    if($arrTruckData->active == 1){

                        $version = DB::table('versions')->where(['type' => $arrData['app_type'], 'device_type' => $arrData['device_type'] ])->orderBy('id', 'desc')->first(['version','mandatory']);

                        $dc = truckdevicetoken::find($arrTruckData->id);

                        $dc->truck_id = $arrData['truck_id'];
                        $dc->device_type = $arrData['device_type'];
                        $dc->token = $arrData['device_token'];
                        $dc->updated_at = $arrData['datetime'];

                        $dc->save();

                        $data['device_token'] = $dc;
                        $data['version'] = $version;

                        return $response = $this->responseData($data, $status = true ,$code = '200',$message = 'Token updated.');
                    }else{
                        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Your session got expired, Please login again.");  
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Hash not found.");   
                }
            }

        }
        catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }



    public function turnPrimaryDeviceOn(Request $request)
    {
        try{
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'hash' => 'required',
                'datetime' => 'required',
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $arrDevice = DB::table('truckdevicetokens')->where(['truck_id' => $arrData['truck_id'], 'hash' => $arrData['hash']])->get(['id', 'active'])->first();

            if(!empty($arrDevice)){

                    if($arrDevice->active == 1){

                        $ar = truckdevicetoken::where('truck_id' , $arrData['truck_id'])
                                                ->update(['primary_device' => 0]);


                        $dc = truckdevicetoken::find($arrDevice->id);

                        $dc->primary_device = 1;
                        $dc->updated_at = $arrData['datetime'];

                        $dc->save();


                        $vendors = DB::table('trucks')
                                        ->join('vendors', 'vendors.id', '=', 'trucks.vendor_id')
                                        ->where(['trucks.id' => $arrData['truck_id'] ])
                                        ->get(['trucks.name', 'vendors.firstname', 'vendors.country_code','vendors.mobile'])->first();

                        $message = "Hello ".$vendors->firstname.", Primary device for ".$vendors->name." has been changed.";

                        $sms = AWS::createClient('sns');
            
                        $sms->publish([
                                'Message' => $message,
                                'PhoneNumber' => $vendors->country_code.$vendors->mobile, 
                                'MessageAttributes' => [
                                    'AWS.SNS.SMS.SMSType'  => [
                                        'DataType'    => 'String',
                                        'StringValue' => 'Transactional',
                                     ]
                                 ],
                              ]);

                        return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Now this device became primary.');
                    }else{
                        return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Your session got expired, Please login again.");  
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = "Please do fresh istallation.");   
                }

        }
        catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function startStripeAccount(Request $request)
    {
        try{

            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required'
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

                $truck = truck::find((int) $arrData['truck_id']);


                \Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET']);

                $account = \Stripe\Account::retrieve($truck->stripe_connect_id);
                
                if($account->charges_enabled == 1){

                    $truck->stripe_status = 1;

                    $truck->save();

                    return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Credit/Debit card based payment enabled.');
                }
                else{

                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Your stripe account not activated yet. For more information please check with Stripe.');

                }

            
        
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    public function stopStripeAccount(Request $request)
    {
        try{
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required'
            );

            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

                $truck = truck::find((int) $arrData['truck_id']);

                $truck->stripe_status = 0;

                $truck->save();

            return $response = $this->responseData($data = new \stdClass, $status = true ,$code = '200',$message = 'Credit/Debit card based payment disabled.');

        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }


}

