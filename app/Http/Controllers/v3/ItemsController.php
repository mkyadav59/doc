<?php

namespace App\Http\Controllers\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use DB, Exception, Validator, Image, Log;
use App\vendor, App\truck, App\category, App\item;

class ItemsController extends BaseController
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'dailystock' => 'required',
                'instock' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $item = new item;
            $item->truck_id = (int)$arrData['truck_id'];
            $item->category_id = (int)$arrData['category_id'];
            $item->name = $arrData['name'];

            if(!empty($request->file('img'))){
                $image = $request->file('img') ;
            
                $guessExtension = $image->guessExtension();
                $fileName = time().'.'.$guessExtension;
                $path = public_path().'/uploads/items/'.$fileName;
                //$file->move($destinationPath,$fileName);
                $image = Image::make($image->getRealPath())->resize(768, 576)->save(sprintf($path));

                $item->img =  $fileName;
            }

            $item->description = $arrData['description'];
            $item->price = $arrData['price'] * 100;
            $item->veg = 1;
            $item->primary = 1;
            $item->dailystock = (int)$arrData['dailystock'];
            $item->instock = (int)$arrData['instock'];

            $itemCount = item::where( ['truck_id' => $arrData['truck_id'], 'category_id' => $arrData['category_id']] )->count();

            if($itemCount > 0){
                $itemCount = $itemCount + 1;
            }
            else{
                $itemCount = 1;
            }
            $item->order = $itemCount;
            $item->active = 1;
            $item->created_at = $arrData['datetime'];
            $item->updated_at = $arrData['datetime'];
        
            $item->save();

            $item->imgUrl = url('/').'/uploads/items/';
            $item->price = round($item->price / 100 , 2);

            if($item->id > 0){
				//$item = DB::table('items')->where(['id'=> $item->id])->get()->first();
                return $response = $this->responseData($data = $item, $status = true ,$code = '200',$message = 'Item added successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try 
        {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            $item = item::find($id);

            if(!empty($item)){

                $item->id = (int) $item->id;
                $item->truck_id = (int) $item->truck_id;
                $item->category_id = (int) $item->category_id;
                $item->price = round($item->price / 100, 2);
                $item->veg = (int) $item->veg;
                $item->dailystock   = (int) $item->dailystock;
                $item->instock   = (int) $item->instock;
                $item->order   = (int) $item->order;
                $item->primary   = (int) $item->primary;
                $item->active   = (int) $item->active;
                
                return $response = $this->responseData($data = $item,$status = true ,$code = '200',$message = 'Item details');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = false ,$code = '221',$message = 'invalid item id');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

  



    public function updateItem(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'item_id' => 'required',
                'truck_id' => 'required',
                'category_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $item = item::find($arrData['item_id']);

                if(!empty($item)){
                $item->truck_id = (int)$arrData['truck_id'];
                $item->category_id = (int)$arrData['category_id'];
                $item->name = $arrData['name'];

                if(!empty($request->file('img'))){

                    $image = $request->file('img') ;
            
                    $guessExtension = $image->guessExtension();
                    $fileName = time().'.'.$guessExtension;
                    $path = public_path().'/uploads/items/'.$fileName;
                    $image = Image::make($image->getRealPath())->resize(768, 576)->save(sprintf($path));

                    $item->img =  $fileName;
                }

                $item->description = $arrData['description'];
                $item->price = $arrData['price'] * 100;
                $item->updated_at = $arrData['datetime'];
                
                $item->save();

                if($item->id > 0){
                    $item = DB::table('items')->where(['id'=> $item->id])->get()->first();
                    return $response = $this->responseData($data = $item, $status = true ,$code = '200',$message = 'Item updated successfully.');
                }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
                }
            }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Invalid item id.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }



    public function updateItemOrder(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'item_id' => 'required',
                'truck_id' => 'required',
                'category_id' => 'required',
                'move' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            

            //insert in to database
            $item = item::find($arrData['item_id']);

            if(!empty($item))
            {
                if($arrData['move'] == "up"){

                    $o = (int) $item->order - 1;
                    
                    $moveUp = item::where(['truck_id' => $arrData['truck_id'], 'category_id' => $arrData['category_id'] , 'order' => $o])->first();
                    

                    //print_r($moveUp);exit();
                    if(!empty($moveUp)){
                        $moveUp->order  = $item->order;
                        $moveUp->updated_at = $arrData['datetime'];
                        $moveUp->save();

                        if($moveUp->id > 0){
                            $item->order = $o;
                            $item->updated_at = $arrData['datetime'];
                            $item->save();
                        }
                    }
                }elseif ($arrData['move'] == "down") {
                
                    $o = (int) $item->order + 1;
                    $moveDown = item::where(['truck_id' => $arrData['truck_id'], 'category_id' => $arrData['category_id'] , 'order' => $o])->first();

                    if(!empty($moveDown)){

                        $moveDown->order  = $item->order;
                        $moveDown->updated_at = $arrData['datetime'];
                        $moveDown->save();

                        if($moveDown->id > 0){
                            $item->order = $item->order + 1;
                            $item->updated_at = $arrData['datetime'];
                            $item->save();
                        }
                    }
                }

                if($item->id > 0){
                    $item = DB::table('items')->where(['id'=> $item->id])->get()->first();
                    return $response = $this->responseData($data = $item, $status = true ,$code = '200',$message = 'Item updated successfully.');
                }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
                }
            }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Invalid item id.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }




    public function updateItemStatus(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'item_id' => 'required',
                'active' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $item = item::find($arrData['item_id']);

                if(!empty($item)){
                $item->active = (int)$arrData['active'];
                $item->updated_at = $arrData['datetime'];
                
                $item->save();

                if($item->id > 0){
                    $item = DB::table('items')->where(['id'=> $item->id])->get()->first();
                    return $response = $this->responseData($data = $item, $status = true ,$code = '200',$message = 'Item updated successfully.');
                }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
                }
            }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Invalid item id.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

    public function updateItemInstock(Request $request)
    {
        //echo "<pre>"; print_r($request->all());exit();
        try
        {
            $arrData = $request->all();
            $rules = array(
                'item_id' => 'required',
                'instock' => 'required',
                'datetime' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $item = item::find($arrData['item_id']);

            if(!empty($item)){
                if((int)$arrData['instock'] >= 0){
                    $item->instock = (int)$arrData['instock'];
                    $item->updated_at = $arrData['datetime'];
                    
                    $item->save();

                    if($item->id > 0){
                        $item = DB::table('items')->where(['id'=> $item->id])->get()->first();
                        return $response = $this->responseData($data = $item, $status = true ,$code = '200',$message = 'Item stock updated successfully.');
                    }else{
                        return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
                    }
                }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Stock can not be negative.');
                }
            }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Invalid item id.');
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }
  


    public function allItemsByCategoryId($id)
    {
        try 
        {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            $items = DB::table('items')
                    ->Join('categories', '.categories.id', '=', 'items.category_id')
                    ->where(['items.category_id' => $id, 'items.active' => 1])
                    ->orderBy('items.order')->get(['items.id', 'items.name','categories.name as category_name', 'items.description', 'items.price', 'items.img', 'items.instock', 'items.order']);

            foreach ($items as $i) {
                $i->price = round($i->price / 100, 2);
            }
            if(!empty($items)){
                
                return $response = $this->responseData($data = $items,$status = true ,$code = '200',$message = 'All items by category id');
            }else{
                 return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'invalid category id');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }


    public function allItemsByTruckId($id)
    {
        try 
        {
            if(empty($id)){
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'No request found');
            }

            $items = DB::table('items')
                    ->leftJoin('categories', '.categories.id', '=', 'items.category_id')
                    ->where('items.truck_id', $id)
                    ->orderBy('items.order')->get(['items.*','categories.name as category_name']);

            if(!empty($items)){
                foreach ($items as $item) {
                   $item->id = (int) $item->id;
                   $item->truck_id = (int) $item->truck_id;
                   $item->category_id = (int) $item->category_id;
                   $item->price = round(($item->price / 100), 2);
                   $item->veg = (int) $item->veg;
                   $item->instock   = (int) $item->instock;
                   $item->order   = (int) $item->order;
                   $item->primary   = (int) $item->primary;
                   $item->active   = (int) $item->active;
                }

                $allItems =  new \stdClass;
                $allItems->items =  $items; 


                return $response = $this->responseData($data = $allItems,$status = true ,$code = '200',$message = 'All items of truck.');
            }else{
                 return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'No item added yet.');
            }
            
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        }
    }

    public function itemOrder(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'data' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $allCategories = category::where( ['truck_id' => $arrData['truck_id']] )->get();

            //print($allCategories); exit();

            foreach ($allCategories as $category ) {

                $i = 1;

                $allItems = item::where(['category_id' => $category->id])->get();

                foreach ($allItems as $item) {
                    $item->order = $i++;
                    $item->save();
                }
            }
        }catch (Exception $ex) {
            Log::error($ex);
            return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong, Please try after sometime.');
        } 
    }

}
