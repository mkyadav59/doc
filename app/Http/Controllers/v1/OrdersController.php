<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator;
use App\vendor, App\truck, App\item, App\customer, App\order, App\item_order, App\token;
use App\Http\Controllers\v1\PushnotifiController, App\customerdevicetoken,  App\truckdevicetoken;

use Edujugon\PushNotification\PushNotification;

class OrdersController extends BaseController
{
    protected $notification;
    public function __construct(PushnotifiController $notification)
    {
        $this->notification = $notification;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $order = DB::table('item_order')->where(['order_id' => 1])->get(['item_id','quantity', 'price']);
        // return $order;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function addToCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
                'order_id' => 'required',
                'item_id' => 'required',
                'quantity' => 'required',
                'price' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            //check if desired quantity is available in stock or not 
            // $stockStatus = item::find($arrData['item_id']);
            // if($stockStatus->instock >= $arrData['quantity'])
            // {   
            //     //$stockStatus->instock = $stockStatus->instock - $arrData['quantity'];

            //     //$stockStatus->save();
            // }
            // else{
            //     throw new Exception('only '.$stockStatus->instock.' available in stock.');
            // }
            $truck = DB::table('trucks')->where(['id'=> $request->truck_id, 'active' => 1])->get(['id']);
            if(!empty($truck) > 0){

                if($arrData['order_id'] == 0){
                    //insert in to database
                    $order = new order;
                    $order->customer_id = $arrData['customer_id'];
                    $order->truck_id = $arrData['truck_id'];
                    $order->pick_up_time = 0;
                    $order->total = 0;
                    $order->save();

                    $arrData['order_id'] = $order->id;
                }


                if($arrData['order_id'] > 0){

                    //check if item is already exists 
                    $itemOrder = item_order::where(['order_id' => $arrData['order_id'], 'item_id' => $arrData['item_id'] ])->get(['id']);
                   
                    if(!empty($itemOrder) > 0){
                        

                        $updateItem = item_order::find($itemOrder[0]['id']);

                        //update quantity else delete from cart
                        if($arrData['quantity'] > 0){
                            $updateItem->quantity = $arrData['quantity'];
                            $updateItem->price = $arrData['price'];
                            $updateItem->save();
                        }else{
                            $updateItem->delete();
                        }

                    }
                    else{
                        $item_order = new item_order;
                        $item_order->order_id = $arrData['order_id'];
                        $item_order->item_id = $arrData['item_id'];
                        $item_order->quantity = $arrData['quantity'];
                        $item_order->price = $arrData['price'];

                        $item_order->save();

                    }
                    
                    /*
                    //update order amount 
                    $amount = item_order::where(['order_id' => $arrData['order_id']])
                        ->get( 
                            array(
                                DB::raw('sum(price * quantity) AS total')
                            )
                        );

                    $updateOrder = order::find($arrData['order_id']);
                    $updateOrder->total = $amount[0]->total;

                    $updateOrder->save();
                    */

                    $updateOrder = order::find($arrData['order_id']);

                    return $response = $this->responseData($data =  $updateOrder , $status = true ,$code = '200',$message = 'Order updated successfully');

                }else{
                    return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = 'Something went wrong while uploading order');
                }
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'At present not accepting orders.');
            }   
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }



    public function showCart(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'order_id' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $cart_items = DB::table('items')
                        ->join('item_order', 'items.id', '=', 'item_order.item_id')
                        ->select('items.id','items.name','items.description','item_order.price', 'item_order.quantity')
                        ->where('item_order.order_id',  $arrData['order_id'])
                        ->get();
            
            return $response = $this->responseData($data =  $cart_items , $status = true ,$code = '200',$message = 'item of cart');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }



    public function placeOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'customer_id' => 'required',
                'truck_id' => 'required',
                'order_id' => 'required',
                'pickup_option' => 'required',
                'tip' => 'required',
                'sub_total' => 'required',
                'total' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 0])->first();
            
            if(!empty($order))
            {
                $order->pick_up_time = $arrData['pickup_option'];
                $order->tip = $arrData['tip'];
                $order->sub_total = $arrData['sub_total'];
                $order->total = $arrData['total'];
                $order->order_status = 'Order Placed';
                $order->order_placed = 1;
                $order->order_placed_time = Date('Y-m-d H:i:s');

                if(!empty($arrData['note']))
                $order->note = $arrData['note'];

                $order->save();
                
            
            //diduct item ordered item quantity from stock
            $orderItems = DB::table('item_order')->where(['order_id'=> $arrData['order_id']])->get();

            foreach ($orderItems as $i) {

                $item = item::find($i->item_id);

                $item->instock = $item->instock - $i->quantity;

                $item->save();
            }

            $res['order'] = $order;
            $res['item'] = $orderItems;

            $push = new PushNotification('apn');

            $message = [
                'aps' => [
                    'alert' => [
                        'title' => 'New Order',
                        'body' => 'Please check there is new order.'
                    ],
                    'sound' => 'default'

                ]
            ];

            $arrtoken = DB::table('truckdevicetokens')->where(['truck_id' => $order->truck_id, 'active' => 1])->orderBy('id', 'desc')->pluck('token')->toArray(); 
            
            $push->setMessage($message)
                ->setDevicesToken($arrtoken);

            $push = $push->send();
    
            return $response = $this->responseData($data = $res, $status = true ,$code = '200',$message = 'Order placed successfully.');
            }else{
                throw new Exception('Order already placed');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }



    public function orderDetailById($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            //$order = order::where(['id' => $id, 'order_placed' => 1])
            //        ->get(['id', 'truck_id', 'pick_up_time', 'note', 'total', 'order_status', 'order_placed_time']);
            

            $order = DB::table('orders')
                    ->join('trucks', 'orders.truck_id', '=', 'trucks.id')
                    ->select('orders.id', 'orders.truck_id','trucks.name as truck_name', 'trucks.phone', 'pick_up_time', 'note', 'sub_total', 'tip','total', 'order_status', 'order_placed_time') 
                    ->where(['orders.id' => $id, 'order_placed' => 1])
                    ->get();
            $resData['order'] = $order;
            //get order items 
            //$items = DB::table('item_order')->where(['order_id'=> $order[0]->id])->get();
            
            $items =  DB::table('item_order')
                    ->join('items', 'items.id', '=', 'item_order.item_id')
                    ->select('item_order.order_id', 'items.id','items.name', 'item_order.order_id', 'item_order.quantity', 'item_order.price')
                    ->where('item_order.order_id',  $order[0]->id)
                    ->get();

            if(!empty($items) > 0)
            $resData['items'] = $items;
            else
            $resData['items'] = [];

            return $response = $this->responseData($data = $resData, $status = true ,$code = '200',$message = 'Order details');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    public function pastOrdersByCustomerId($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }
             $orders = customer::find($id)->orders()->where(['order_placed' => 1])->orderBy('id', 'DESC')->limit(100)->get(['id', 'truck_id', 'pick_up_time', 'note', 'total', 'order_status', 'order_placed_time']);

            return $response = $this->responseData($data = $orders, $status = true ,$code = '200',$message = 'Past Orders');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    public function pastOrdersByTruckId($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }
            $arrOrders['order_placed'] = [];
            $arrOrders['order_accepted'] = [];
            $arrOrders['order_ready'] = [];
            $arrOrders['order_delivered'] = [];
            $arrOrders['order_rejected'] = [];
            $arrOrders['order_cancelled'] = [];

            $orders = DB::table('orders')
                                        ->join('customers', 'orders.customer_id', '=', 'customers.id')
                                        ->select('orders.id', 'customers.firstname', 'customers.mobile', 'truck_id', 'pick_up_time', 'note', 'total', 'order_status', 'order_placed_time', 'order_placed', 'order_accepted', 'order_ready', 'order_delivered', 'order_rejected', 'order_cancelled') 
                                        ->where(['truck_id' => $id])
                                        ->orderby('orders.id')
                                        ->whereDate('order_placed_time', '>=', date('Y-m-d').' 00:00:00')
                                        ->get();
            foreach ($orders as $o) {

                $o->id = (int)$o->id;
                $o->firstname = $o->firstname ." { ".$o->mobile ." }";
                $o->truck_id = (int)$o->truck_id;
                $o->pick_up_time = date("h:iA", strtotime($o->order_placed_time . "+".$o->pick_up_time." minutes"));
                $o->total = (float)$o->total;
                
                if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  )
                {
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_placed'],$o);
                }
                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_accepted'],$o);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 1 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_ready'],$o);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 1 AND
                   $o->order_ready == 1 AND 
                   $o->order_delivered == 1 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 0  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_delivered'],$o);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 1 AND 
                   $o->order_cancelled == 0  OR
                   $o->order_accepted == 1
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_rejected'],$o);
                }

                else if($o->order_placed == 1 AND
                   $o->order_accepted == 0 AND
                   $o->order_ready == 0 AND 
                   $o->order_delivered == 0 AND 
                   $o->order_rejected == 0 AND 
                   $o->order_cancelled == 1  
                  ){
                    unset($o->order_placed);
                    unset($o->order_accepted);
                    unset($o->order_ready);
                    unset($o->order_delivered);
                    unset($o->order_rejected);
                    unset($o->order_cancelled);
                    array_push($arrOrders['order_cancelled'],$o);
                }
            }
            

            return $response = $this->responseData($data = $arrOrders, $status = true ,$code = '200',$message = 'Past Orders..');
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    public function acceptOrder($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }
            
            $order = order::where(['id' => $id, 'order_placed' => 1,'order_accepted' => 0, 'order_cancelled' => 0])->first();

            if(!empty($order))
            {
                $order->order_status = 'Order Accepted';
                $order->order_accepted = 1;
                $order->order_accepted_time = Date('Y-m-d H:i:s');
                $order->save();
        
            
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Your Order #'.$id.' Accepted',
                                             'body'=>' #'.$id.' You will be notified when order is ready.',
                                             'truck_id' => $order->truck_id
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Your Order #'.$id.' Accepted',
                                'body'=>' #'.$id.' You will be notified when order is ready.',
                                'truck_id' => $order->truck_id
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }
                return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order accepted successfully.');
            }else{
                throw new Exception('Order already accepted');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }




    public function readyOrder($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }
            
            $order = order::where(['id' => $id, 'order_placed' => 1, 'order_accepted' => 1, 'order_ready' => 0])->first();
            
            if(!empty($order))
            {
                $order->order_status = 'Order Ready';
                $order->order_ready = 1;
                $order->order_ready_time = Date('Y-m-d H:i:s');
                $order->save();
                
                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Your Order #'.$id.' Ready',
                                             'body'=>'#'.$id.' Please come by and pickup your order.',
                                             'truck_id' => $order->truck_id
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Your Order #'.$id.' Ready',
                                'body'=>'#'.$id.' Please come by and pickup your order.',
                                'truck_id' => $order->truck_id
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }
            }else{
                throw new Exception('Order already ready');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order ready for pickup.');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }





    public function deliverOrder($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }
            
            $order = order::where(['id' => $id, 'order_placed' => 1, 'order_accepted' => 1, 'order_ready' => 1, 'order_delivered' => 0])->first();
            
            if(!empty($order))
            {
                $order->order_status = 'Order Delivered';
                $order->order_delivered = 1;
                $order->order_delivered_time = Date('Y-m-d H:i:s');
                $order->save();


                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();//->pluck('token', 'device_type')->toArray();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$id.' Picked Up',
                                             'body'=>'#'.$id.' Thank you! Enjoy your food!!',
                                             'truck_id' => $order->truck_id
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$id.' Picked Up',
                                 'body'=>'#'.$id.' Thank you! Enjoy your food!!',
                                 'truck_id' => $order->truck_id
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }
            }else{
                throw new Exception('Order already pickedup.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'You picked up your order. Enjoy your meal!!');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    //Customer is going to cancel the order 
    //So pushnotification will be sent to Truck devices
    public function cancelOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();
            
            if(!empty($order))
            {
                if($order->order_ready == 1){
                    throw new Exception('Order is ready, so can not be cancel.');
                }
                else if($order->order_accepted == 1){
                    throw new Exception('Order already accepted, so can not be cancel.');
                }

                $order->order_status = 'Order cancelled';
                $order->order_cancelled = 1;
                $order->order_cancelled_time = Date('Y-m-d H:i:s');
                $order->order_cancelled_remark = (!empty($arrData['remark'])) ? $arrData['remark'] : Null;
                $order->save();

                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();//->pluck('token', 'device_type')->toArray();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order Cancelled',
                                             'body'=>'You have cancelled your order!!'
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order Cancelled',
                                'body'=>'You have cancelled your order!!'
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }


            
            }else{
                throw new Exception('Order not placed or not found.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'Order cancelled successfully..');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }





    public function rejectOrder(Request $request)
    {
        try 
        {
            $arrData = $request->all();
            $rules = array(
                'order_id' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            $order = order::where(['id' => $arrData['order_id'], 'order_placed' => 1])->first();
            
            if(!empty($order))
            {

                $order->order_status = 'Order Rejected';
                $order->order_rejected = 1;
                $order->order_rejected_time = Date('Y-m-d H:i:s');
                $order->order_rejected_remark = (!empty($arrData['remark'])) ? $arrData['remark'] : Null;
                $order->save();

                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();//->pluck('token', 'device_type')->toArray();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $push->setMessage([
                                        'data' => [
                                             'title'=>'Order #'.$arrData['order_id'].' Rejected',
                                             'body'=>'#'.$id.' Order can not be proccessed at this time.',
                                             'truck_id' => $order->truck_id
                                             ],
                                        'sound' => 'default'

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title'=>'Order #'.$arrData['order_id'].' Rejected',
                                'body'=>'#'.$id.' Order can not be proccessed at this time.',
                                'truck_id' => $order->truck_id
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }

            }else{
                throw new Exception('Order not placed or not found.');
            }

            return $response = $this->responseData($data = $order, $status = true ,$code = '200',$message = 'order rejected successfully.');
            
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }
    
    public function testNotification($id)
    {
        try 
        {
            $result=null; 

            if(empty($id)){
                throw new Exception('No request found');
            }
            
            $order = order::where(['id' => $id])->first();

            if(!empty($order))
            {
                $order->order_status = 'Order Accepted';
                $order->order_accepted = 1;
                $order->order_accepted_time = Date('Y-m-d H:i:s');
                //$order->save();

                $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $order->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();//->pluck('token', 'device_type')->toArray();
                
                //echo "<pre>"; print_r($arrtoken);exit;
                
                if($arrtoken->device_type == 'android'){
                        $push = new PushNotification('fcm');
                        $result = $push->setMessage([
                                        'data' => [
                                             'title'=>'Order Accepted',
                                             'body'=>'#'.$id.' Your order has been accepted.',
                                             'truck_id' => $order->truck_id
                                             ]

                                     ])
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();
                        
                        $result->device_token = $arrtoken->token;
                }
                else{

                    $push = new PushNotification('apn');

                    $message = [
                        'aps' => [
                            'alert' => [
                                'title' => 'New Order',
                                'body' => 'Please check there is new order.',
                                'trucik_id' => 34
                            ],
                            'sound' => 'default'

                        ]
                    ];
                    
                    $result = $push->setMessage($message)
                        ->setDevicesToken($arrtoken->token)
                        ->send()
                        ->getFeedback();


                    $result->device_token = $arrtoken->token;
                }
            return $response = $this->responseData($data = $result, $status = true ,$code = '200',$message = 'Push notification sent successfully.');

                
            }else{
                throw new Exception('Order already accepted');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


    
}
