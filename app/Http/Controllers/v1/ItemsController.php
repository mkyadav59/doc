<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use DB, Exception, Validator, Image;
use App\vendor, App\truck, App\category, App\item;

class ItemsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'veg' => 'required',
                'primary' => 'required',
                'instock' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $item = new item;
            $item->truck_id = (int)$arrData['truck_id'];
            $item->category_id = (int)$arrData['category_id'];
            $item->name = $arrData['name'];

            if(!empty($request->file('img'))){
                $image = $request->file('img') ;
            
                $guessExtension = $image->guessExtension();
                $fileName = time().'.'.$guessExtension;
                $path = public_path().'/uploads/items/'.$fileName;
                //$file->move($destinationPath,$fileName);
                $image = Image::make($image->getRealPath())->resize(768, 424)->save(sprintf($path));

                $item->img =  $fileName;
            }

            $item->description = $arrData['description'];
            $item->price = (double) $arrData['price'];
            $item->veg = (int)$arrData['veg'];
            $item->primary = (int)$arrData['primary'];
            $item->instock = (int)$arrData['instock'];
            $item->order = (int)$arrData['order'];
            $item->active = 1;
        
            $item->save();

            $item->imgUrl = $request->root().'/uploads/items/';

            if($item->id > 0){
                /*$itemArr = DB::table('items')->where(['id'=> $item->id, 'active' => 1])->get()[0];
                foreach($itemArr as $key => $value) {
                   $newArray[$key] = (string) $value;
                }*/
				$item = DB::table('items')->where(['id'=> $item->id])->get()->first();
                return $response = $this->responseData($data = $item, $status = true ,$code = '200',$message = 'Item added successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $item = item::find($id);

            if(!empty($item)){

                $item->id = (int) $item->id;
                $item->truck_id = (int) $item->truck_id;
                $item->category_id = (int) $item->category_id;
                $item->price = (double) $item->price;
                $item->veg = (int) $item->veg;
                $item->instock   = (int) $item->instock;
                $item->order   = (int) $item->order;
                $item->primary   = (int) $item->primary;
                $item->active   = (int) $item->active;
                
                return $response = $this->responseData($data = $item,$status = true ,$code = '200',$message = 'Item details');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = false ,$code = '221',$message = 'invalid item id');
                 throw new Exception('invalid item id');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }



    public function updateItem(Request $request)
    {
        //echo "<pre>"; print_r($request->all());exit();
        try
        {
            $arrData = $request->all();
            $rules = array(
                'item_id' => 'required',
                'truck_id' => 'required',
                'category_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'veg' => 'required',
                'primary' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $item = item::find($arrData['item_id']);

                if(!empty($item)){
                $item->truck_id = (int)$arrData['truck_id'];
                $item->category_id = (int)$arrData['category_id'];
                $item->name = $arrData['name'];

                if(!empty($request->file('img'))){

                    $image = $request->file('img') ;
            
                    $guessExtension = $image->guessExtension();
                    $fileName = time().'.'.$guessExtension;
                    $path = public_path().'/uploads/items/'.$fileName;
                    //$file->move($destinationPath,$fileName);
                    $image = Image::make($image->getRealPath())->resize(768, 424)->save(sprintf($path));

                    $item->img =  $fileName;
                }

                $item->description = $arrData['description'];
                $item->price = (double)$arrData['price'];
                $item->veg = (int)$arrData['veg'];
                $item->primary = (int)$arrData['primary'];
                $item->instock = (int)$arrData['instock'];
                $item->order = (int)$arrData['order'];
                $item->active = (int)$arrData['active'];
            
                $item->save();

                if($item->id > 0){
                    $item = DB::table('items')->where(['id'=> $item->id])->get()->first();
                    return $response = $this->responseData($data = $item, $status = true ,$code = '200',$message = 'Item updated successfully.');
                }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
                }
            }else{
                    return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Invalid item id.');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function allItemsByCategoryId($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $item = DB::table('items')
                    ->leftJoin('categories', '.categories.id', '=', 'items.category_id')
                    ->where(['items.category_id' => $id, 'items.active' => 1])->orderBy('items.order')
                    ->orderBy('items.order')->get(['*','categories.name as category_name']);

            if(!empty($item)){
                
                return $response = $this->responseData($data = $item,$status = true ,$code = '200',$message = 'Category details');
            }else{
                 throw new Exception('invalid category id');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }


    public function allItemsByTruckId($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $items = DB::table('items')
                    ->leftJoin('categories', '.categories.id', '=', 'items.category_id')
                    ->where('items.truck_id', $id)
                    ->orderBy('items.order')->get(['items.*','categories.name as category_name']);

            if(!empty($items)){
                foreach ($items as $item) {
                   $item->id = (int) $item->id;
                   $item->truck_id = (int) $item->truck_id;
                   $item->category_id = (int) $item->category_id;
                   $item->price = (double) $item->price;
                   $item->veg = (int) $item->veg;
                   $item->instock   = (int) $item->instock;
                   $item->order   = (int) $item->order;
                   $item->primary   = (int) $item->primary;
                   $item->active   = (int) $item->active;
                }

                $allItems =  new \stdClass;
                $allItems->items =  $items; 


                return $response = $this->responseData($data = $allItems,$status = true ,$code = '200',$message = 'All items of truck.');
            }else{
                 return $response = $this->responseData($data = new \stdClass,$status = true ,$code = '200',$message = 'No item added yet.');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }

    public function itemOrder(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'data' => 'required'
            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }

            $arr = json_encode($arrData['data']);
            $arrItems = json_decode($arr);

            $o = 1;

            foreach ($arrItems->items as $ii) {
                $item = item::find($ii->id);
                $item->order = $o++;
                $item->save();
            }

            $allitems = new \stdClass;
            $items = DB::table('items')->where(['truck_id' => $arrData['truck_id'], 'active' => 1])->orderBy('order')->get();

            foreach ($items as $item) {
               $item->id = (int) $item->id;
               $item->truck_id = (int) $item->truck_id;
               $item->category_id = (int) $item->category_id;
               $item->price = (double) $item->price;
               $item->veg = (int) $item->veg;
               $item->instock   = (int) $item->instock;
               $item->order   = (int) $item->order;
               $item->primary   = (int) $item->primary;
               $item->active   = (int) $item->active;
            }

            $allitems->items = $items;
            
            if($item->id > 0){

                return $response = $this->responseData($data = $allitems, $status = true ,$code = '200',$message = 'Item orders updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading item');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

}
