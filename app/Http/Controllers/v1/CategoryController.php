<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator;
use App\vendor, App\category;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $arrData = $request->all();
            $rules = array(
                'truck_id' => 'required',
                'name' => 'required',
                'short_description' => 'required',
                'description' => 'required',

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $category = new category;
            $category->truck_id = $arrData['truck_id'];
            $category->name = $arrData['name'];
            $category->short_description = $arrData['short_description'];
            $category->description = $arrData['description'];
            $category->order = '1';
            $category->active = '1';
        
            $category->save();

            if($category->id > 0){
                return $response = $this->responseData($data = $category, $status = true ,$code = '200',$message = 'Category added successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $category = DB::table('categories')->where(['id'=> $id, 'active' => 1])->get();//category::find($id);

            if(!empty($category)){
                
                return $response = $this->responseData($data = $category[0],$status = true ,$code = '200',$message = 'Category details');
            }else{
                 throw new Exception('invalid category id');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }



    public function updateCategory(Request $request)
    {
        try
        {
            $arrData = $request->all();

            $rules = array(
                'category_id' => 'required',
                'truck_id' => 'required',
                'name' => 'required',
                'img' => 'required',
                'short_description' => 'required',
                'description' => 'required',
                'active' => 'required'

            );
            $validator = Validator::make($arrData,$rules);
            if($validator->fails()){
                return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $validator->errors()->all());
            }
            
            //insert in to database
            $category = category::find($arrData['category_id']);
            $category->truck_id = $arrData['truck_id'];
            $category->name = $arrData['name'];
            $category->img = $arrData['img'];
            $category->short_description = $arrData['short_description'];
            $category->description = $arrData['description'];
            $category->order = $arrData['order'];
            $category->active = $arrData['active'];
        
            $category->save();

            if($category->id > 0){

                return $response = $this->responseData($data = $category, $status = true ,$code = '200',$message = 'Category updated successfully');
            }else{
                return $response = $this->responseData($data = new \stdClass, $status = FALSE ,$code = '221',$message = 'Something went wrong while uploading category');
            }
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allcategoriesbytruckid($id)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('No request found');
            }

            $category = DB::table('categories')->where(['truck_id'=> $id, 'active' => 1])->orderBy('order')->get();

            if(!empty($category)){
                $allCategories = new \stdClass;
                $allCategories->category = $category;
                return $response = $this->responseData($data = $allCategories,$status = true ,$code = '200',$message = 'All categories belongs to a truck');
            }else{
                 throw new Exception('No catogry found.');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }


    public function categoryOrder(Request $request)
    {
        try 
        {
            if(empty($id)){
                throw new Exception('Done');
            }

            $category = DB::table('categories')->where(['truck_id'=> $id, 'active' => 1])->orderBy('order')->get();

            if(!empty($category)){
                
                return $response = $this->responseData($data = $category,$status = true ,$code = '200',$message = 'All categories belongs to a truck');
            }else{
                 throw new Exception('No catogry found.');
            }
            
        }catch (Exception $ex) {
            return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        }
    }
}
