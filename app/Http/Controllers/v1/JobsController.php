<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Exception, Validator;
use App\vendor, App\truck, App\item, App\customer, App\order, App\item_order, App\token;
use App\Http\Controllers\v1\PushnotifiController, App\customerdevicetoken,  App\truckdevicetoken;

use Edujugon\PushNotification\PushNotification;

use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Storage;

use Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class JobsController extends Mailable
{
  use Queueable, SerializesModels;
    protected $notification;

    public function __construct(PushnotifiController $notification)
    {
        $this->notification = $notification;
    }
  
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     
    public function index()
    {
    }

  public function template(){
    
    return Excel::download(new OrdersExport(31), 'report_'.date('Y-m-d-H-i-s').'.xlsx');
  }
  */
  public function index()
    { 
        try 
        {
      $trucks = DB::table('trucks')->get();
      
      foreach($trucks as $truck){

        $id = $truck->id;
        $completed_orders = 0;
        $sub_total=0;
        $tip_total=0;
        $grand_total=0;
        
        if(empty($id)){
          throw new Exception('No request found');
        }
        $arrOrders['order_placed'] = [];
        $arrOrders['order_accepted'] = [];
        $arrOrders['order_ready'] = [];
        $arrOrders['order_delivered'] = [];
        $arrOrders['order_rejected'] = [];
        $arrOrders['order_cancelled'] = [];

        $orders = DB::table('orders')
              ->where(['truck_id' => $id])
              ->orderby('orders.id')
              //->whereDate('order_placed_time', '>', date('Y-m-d').' 00:00:00')
              ->get();

        foreach ($orders as $o) {

          $o->id = (int)$o->id;
          $o->truck_id = (int)$o->truck_id;
          $o->pick_up_time = date("h:iA", strtotime($o->order_placed_time . "+".$o->pick_up_time." minutes"));
          $o->total = (float)$o->total;
          
          if($o->order_placed == 1 AND
             $o->order_accepted == 0 AND
             $o->order_ready == 0 AND 
             $o->order_delivered == 0 AND 
             $o->order_rejected == 0 AND 
             $o->order_cancelled == 0  
            )
          {
            unset($o->order_placed);
            unset($o->order_accepted);
            unset($o->order_ready);
            unset($o->order_delivered);
            unset($o->order_rejected);
            unset($o->order_cancelled);
            array_push($arrOrders['order_placed'],$o);
          }
          else if($o->order_placed == 1 AND
             $o->order_accepted == 1 AND
             $o->order_ready == 0 AND 
             $o->order_delivered == 0 AND 
             $o->order_rejected == 0 AND 
             $o->order_cancelled == 0  
            ){
            unset($o->order_placed);
            unset($o->order_accepted);
            unset($o->order_ready);
            unset($o->order_delivered);
            unset($o->order_rejected);
            unset($o->order_cancelled);
            array_push($arrOrders['order_accepted'],$o);
          }

          else if($o->order_placed == 1 AND
             $o->order_accepted == 1 AND
             $o->order_ready == 1 AND 
             $o->order_delivered == 0 AND 
             $o->order_rejected == 0 AND 
             $o->order_cancelled == 0  
            ){
            unset($o->order_placed);
            unset($o->order_accepted);
            unset($o->order_ready);
            unset($o->order_delivered);
            unset($o->order_rejected);
            unset($o->order_cancelled);
            array_push($arrOrders['order_ready'],$o);
          }

          else if($o->order_placed == 1 AND
             $o->order_accepted == 1 AND
             $o->order_ready == 1 AND 
             $o->order_delivered == 1 AND 
             $o->order_rejected == 0 AND 
             $o->order_cancelled == 0  
            ){
            unset($o->order_placed);
            unset($o->order_accepted);
            unset($o->order_ready);
            unset($o->order_delivered);
            unset($o->order_rejected);
            unset($o->order_cancelled);
            array_push($arrOrders['order_delivered'],$o);

            
            $completed_orders = $completed_orders + 1;
            //echo "<pre>"; print_r($o); exit;
            $sub_total=$sub_total + $o->sub_total;
            $tip_total= $tip_total + $o->tip;
            $grand_total=$grand_total + $o->total;
          }

          else if($o->order_placed == 1 AND
             $o->order_accepted == 0 AND
             $o->order_ready == 0 AND 
             $o->order_delivered == 0 AND 
             $o->order_rejected == 1 AND 
             $o->order_cancelled == 0  OR 
             $o->order_placed == 0
            ){
            unset($o->order_placed);
            unset($o->order_accepted);
            unset($o->order_ready);
            unset($o->order_delivered);
            unset($o->order_rejected);
            unset($o->order_cancelled);
            array_push($arrOrders['order_rejected'],$o);

          }

          else if($o->order_placed == 1 AND
             $o->order_accepted == 0 AND
             $o->order_ready == 0 AND 
             $o->order_delivered == 0 AND 
             $o->order_rejected == 0 AND 
             $o->order_cancelled == 1  
            ){
            unset($o->order_placed);
            unset($o->order_accepted);
            unset($o->order_ready);
            unset($o->order_delivered);
            unset($o->order_rejected);
            unset($o->order_cancelled);
            array_push($arrOrders['order_cancelled'],$o);
          }
        }
          

          $fileName = 'reports/'.date('Ymd').'/report_'.date('Y-m-d-H-i').'.csv';
          //return Excel::download(new OrdersExport(31), 'report_'.date('Y-m-d').'.csv');exit;
          Excel::store(new OrdersExport($id), 'public/'.$fileName);
          //exit;
          $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
          $pathToFile = $storagePath.$fileName;
        
          $result = DB::table('trucks')
              ->join('vendors', 'vendors.id', '=', 'trucks.vendor_id')
              ->select('trucks.name','vendors.firstname','vendors.lastname','vendors.email')
              ->where('trucks.id', $id)
              ->get()[0];
          
          $data['total_orders_count'] = count($orders);
          $data['truck_name'] = $result->name;
          $data["report_date"] = date("Y-m-d H:i");
          $data["total_order_count"] = $completed_orders;
          $data["total_order_amount"] = $sub_total;
          $data["total_tip_amount"] =  $tip_total;
          $data["grand_amount"]  = $grand_total;
          
          $mailToEmailId = $result->email;
          $mailToName = $result->firstname." ".$result->lastname; 
          $mailSubject = "Daily Report For ".date("F j, Y H-i");
          
          //echo "<pre>"; print_r($data);exit;
          Mail::send('emails.mail', $data, function($message) use($mailToEmailId , $mailToName, $mailSubject, $pathToFile){
            $message->to($mailToEmailId, $mailToName)
                ->subject($mailSubject)
                ->attach($pathToFile)
                ->from('donotreply@grilledchili.com','Grilledchili');
          });
      
      }

        }catch (Exception $ex) {
            //return $response = $this->responseData($data = new \stdClass,$status = FALSE ,$code = '221',$message = $ex->getMessage());
        } 
    }


}
