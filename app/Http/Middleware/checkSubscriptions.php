<?php

namespace App\Http\Middleware;

use Closure;

class checkSubscriptions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->subscribed('main')){
            return redirect('payment page');
        }
        return $next($request);
    }
}
