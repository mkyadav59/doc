<?php

namespace App\Exports;


use DB;
use App\Orders, App\Customers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrderDetailReportSheet implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle
{	
	private $id;

	public function __construct($id)
    {
        $this->id = $id;
    }

   
	public function headings(): array
    {
        return [
            'Order Number',
            'Final Order Status',
            'Order Wanted Time',
			'Item Name',
            'Item level customizations',
            'Quantity Ordered',
			'Price',
			'Item Level Special Instructions',
            
        ];
    }
	
    public function collection()
    {		
            $orders = DB::table('item_order')
					->join('items', 'items.id', '=', 'item_order.item_id')
					->join('orders', 'orders.id', '=', 'item_order.order_id')
					->select('order_id', 'orders.order_status', 'orders.order_wanted_time', 'items.name', 'customizations','quantity', 'item_order.price', 'item_order.note', 'order_rejected_remark') 
					->where(['orders.truck_id' => $this->id, 'order_placed' => 1])
					->whereDate('orders.created_at', '=', date('Y-m-d', strtotime("-1 days")))
                    ->orderby('item_order.order_id')
					->get();

            foreach ($orders as $o) {

                $o->price = round($o->price / 100, 2);

                if($o->order_status == 'Rejected'){
                    $o->order_status = "Rejected By Merchant";
                }elseif ($o->order_status == 'Cancelled') {
                    $o->order_status = "Cancelled By Customer";
                }


                if(!empty($o->order_rejected_remark) && $o->order_rejected_remark == "auto_rejected"){
                    $o->order_status = "Auto Rejeted By System";
                    unset($o->order_rejected_remark);
                }

            }


			return $orders;
    }

   	 /**
     * @return string
     */
    public function title(): string
    {
        return 'Order Details';
    }


}