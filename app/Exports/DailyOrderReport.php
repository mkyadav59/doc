<?php

namespace App\Exports;


use DB;
use App\Orders, App\Customers;
use App\Exports\OrderOverviewReportSheet;
use App\Exports\OrderDetailReportSheet;
use App\Exports\OrderProcessingTimeReportSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DailyOrderReport implements WithMultipleSheets
{	
	use Exportable;
    private $id;

	public function __construct($id)
    {
        $this->id = $id;
    }


    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];



        $sheets[] = new OrderOverviewReportSheet($this->id);
        $sheets[] = new OrderDetailReportSheet($this->id);
        $sheets[] = new OrderProcessingTimeReportSheet($this->id);

        return $sheets;
    }

}