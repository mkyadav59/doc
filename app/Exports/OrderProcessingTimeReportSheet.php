<?php

namespace App\Exports;


use DB;
use App\Orders, App\Customers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrderProcessingTimeReportSheet implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle
{	
	private $id;

	public function __construct($id)
    {
        $this->id = $id;
    }

   
	public function headings(): array
    {
        return [
            'Order Number',
			'Final Order Status',
			'Order Wanted Time',
			'Order Placed Time',
			'Order Accepted Time',
			'Acceptance Interval in min. (Placed minus Accepted)',
			'Order Ready Time',
			'Ready Interval in min. (Wanted minus Ready)',
			'Order Delivered Time',
			'Pickup Interval in min. (Delivered minus Ready)',
			'Order Rejected Time',
			'Order Rejected Interval (Order Rejected Time - Order Placed Time)',
			'Order Cancelled Time',
			'Order Cancelled Interval (order Cancelled Time - Order Placed Time)',
        ];
    }
	
    public function collection()
    {		
            $orders = DB::table('orders')
					->join('customers', 'orders.customer_id', '=', 'customers.id')
					->select('orders.id', 'order_status', 'order_wanted_time','order_placed_time', 'order_accepted_time', 'order_acceptance_interval', 'order_ready_time', 'order_ready_interval', 'order_delivered_time', 'order_pickup_interval', 'order_rejected_time', 'order_rejected_time as order_rejected_time_interval', 'order_cancelled_time', 'order_cancelled_time as order_cancelled_time_interval', 'order_rejected_remark') 
					->where(['truck_id' => $this->id, 'order_placed' => 1])
					->orderby('orders.id')
					->whereDate('orders.created_at', '=', date('Y-m-d', strtotime("-1 days")))
					->get();


			foreach ($orders as $o) {


				if($o->order_status == 'Rejected'){
					$o->order_rejected_time_interval = Date( "i:s",strtotime($o->order_rejected_time) - strtotime($o->order_accepted_time));
				}
				
				if ($o->order_status == 'Cancelled') {
					$o->order_cancelled_time_interval = Date( "i:s",strtotime($o->order_cancelled_time) - strtotime($o->order_accepted_time));
				}


				if($o->order_status == 'Rejected'){
					$o->order_status = "Rejected by merchant";
				}elseif ($o->order_status == 'Cancelled') {
					$o->order_status = "Cancelled by customer";
				}


				if(!empty($o->order_rejected_remark) && $o->order_rejected_remark == "auto_rejected"){
					$o->order_status = "Auto Rejeted By System";
					unset($o->order_rejected_remark);
				}


			}

			return $orders;
    }

   	 /**
     * @return string
     */
    public function title(): string
    {
        return 'Order Processing Time';
    }


}