<?php

namespace App\Exports;


use DB;
use App\Orders, App\Customers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrderOverviewReportSheet implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle
{	
	private $id;

	public function __construct($id)
    {
        $this->id = $id;
    }

   
	public function headings(): array
    {
        return [
            'Order Number',
			'Customer Name',
            'Note',
			'Sub Total',
			'Tip',
			'Tax',
			'Total',
			'Transaction Type',
			'Final Order Status',
        ];
    }
	
    public function collection()
    {		
            $orders = DB::table('orders')
					->join('customers', 'orders.customer_id', '=', 'customers.id')
					->select('orders.id',  'customers.firstname', 'note', 'sub_total', 'tip', 'tax', 'total', 'payment_method' ,'order_status', 'order_rejected_remark') 
					->where(['truck_id' => $this->id, 'order_placed' => 1])
					->orderby('orders.id')
					->whereDate('orders.created_at', '=', date('Y-m-d', strtotime("-1 days")))
					->get();

			foreach ($orders as $o) {

				$o->sub_total 	= round($o->sub_total / 100, 2);
				$o->tip 		= round($o->tip / 100, 2);
				$o->tax 		= round($o->tax / 100, 2);
				$o->total 		= round($o->total / 100, 2);

				if(empty($o->payment_method)){
					$o->payment_method = "Paid At Counter";
				}

				if($o->order_status == 'Rejected'){
					$o->order_status = "Rejected By Merchant";
				}elseif ($o->order_status == 'Cancelled') {
					$o->order_status = "Cancelled By Customer";
				}


				if(!empty($o->order_rejected_remark) && $o->order_rejected_remark == "auto_rejected"){
					$o->order_status = "Auto Rejeted By System";
					unset($o->order_rejected_remark);
				}

			}
			return $orders;
    }

   	 /**
     * @return string
     */
    public function title(): string
    {
        return 'Order Overview';
    }


}