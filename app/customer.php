<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
class customer extends Model
{
    use Billable; 

    public function trucks()
    {
    	return $this->belongsToMany(truck::class);
    }

    public function orders()
    {
    	return $this->hasMany(order::class);
    }

    public function tokens()
    {
    	return $this->hasMany(token::class);
    }


}
