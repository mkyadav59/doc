<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\truck_subscription;
use Stripe\Stripe;
use DB, Validator, Exception;
use Illuminate\Support\Facades\Auth;

class CheckSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Track the vendor subscriptions plan.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try 
        {
            $customer = truck_subscription::find(1);
        //  auth('api')->user()->createSetupIntent();
            $key = env('STRIPE_SECRET');
            $stripe = new \Stripe\StripeClient($key);
            $plansraw = $stripe->plans->all();
            $plans = $plansraw->data;
            $product = [];
            foreach($plans as $plan) {
                $add_prod = [];
                $prod = $stripe->products->retrieve($plan->product);
                $add_prod['name'] = $prod['name'];
                $add_prod['id'] = $plan->id;
                $add_prod['interval'] = !empty($plan->interval) ? $plan->interval : 0;
                $add_prod['interval_count'] = !empty($plan->interval_count) ? $plan->interval_count : 0;
                $product[] = $add_prod;
            }
            $data = [
                'plans' => $product,
                'cancel_plan' => 'Cancel',
            ];
            return $data; 
        }catch (Exception $ex) {
            return  $ex->getMessage();
        }
    }
}
