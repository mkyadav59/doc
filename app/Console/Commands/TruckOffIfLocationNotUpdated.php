<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB, Exception, Log;
use App\truck;

class TruckOffIfLocationNotUpdated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'truck:truckoffiflocationnotupdated';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Track truck location if its on and turn off in not updated for more then 5 min.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try 
        {
             $trucks = DB::table('trucks')
                    ->where(['on_off' => 1])
                    ->get(['id','location_updated_at']);

              foreach ($trucks as $t) {
                  
                  //check if location_updated_at not updated since 5 min then turn off the truck
                  if( ($t->location_updated_at + 300) < time()){

                      $tr = truck::where(['id' => $t->id])->first();
                      $tr->on_off = 0;
                      $tr->save();
                  }

              }

        }catch (Exception $ex) {
            return  $ex->getMessage();
        }
    }
}
