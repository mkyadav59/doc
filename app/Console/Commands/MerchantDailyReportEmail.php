<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use DB, Exception;
use App\vendor, App\truck, App\order;


use App\Exports\OrdersExport;
use App\Exports\DailyOrderReport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

use Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MerchantDailyReportEmail extends Command
{

    use Queueable, SerializesModels;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:merchantdailyreport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try 
        {
          $trucks = DB::table('trucks')
                              ->get();
          
          foreach($trucks as $truck){
            $id = $truck->id;
            $completed_orders = 0;
            $sub_total=0;
            $tip_total=0;
            $grand_total=0;

            if(empty($id)){
              throw new Exception('No request found');
            }
            $arrOrders['order_placed'] = [];
            $arrOrders['order_accepted'] = [];
            $arrOrders['order_ready'] = [];
            $arrOrders['order_delivered'] = [];
            $arrOrders['order_rejected'] = [];
            $arrOrders['order_cancelled'] = [];

            $orders = DB::table('orders')
                  ->where(['truck_id' => $id])
                  ->orderby('orders.id')
                  ->whereDate('order_placed_time', '=', date('Y-m-d', strtotime("-1 days")))
                  ->get();

            // foreach ($orders as $o) {

            //   $o->id = (int)$o->id;
            //   $o->truck_id = (int)$o->truck_id;
            //   $o->pick_up_time = date("h:iA", strtotime($o->order_placed_time . "+".$o->pick_up_time." minutes"));
            //   $o->total = (float)$o->total;
              
            //   if($o->order_placed == 1 AND
            //      $o->order_accepted == 0 AND
            //      $o->order_ready == 0 AND 
            //      $o->order_delivered == 0 AND 
            //      $o->order_rejected == 0 AND 
            //      $o->order_cancelled == 0  
            //     )
            //   {
            //     unset($o->order_placed);
            //     unset($o->order_accepted);
            //     unset($o->order_ready);
            //     unset($o->order_delivered);
            //     unset($o->order_rejected);
            //     unset($o->order_cancelled);
            //     array_push($arrOrders['order_placed'],$o);
            //   }
            //   else if($o->order_placed == 1 AND
            //      $o->order_accepted == 1 AND
            //      $o->order_ready == 0 AND 
            //      $o->order_delivered == 0 AND 
            //      $o->order_rejected == 0 AND 
            //      $o->order_cancelled == 0  
            //     ){
            //     unset($o->order_placed);
            //     unset($o->order_accepted);
            //     unset($o->order_ready);
            //     unset($o->order_delivered);
            //     unset($o->order_rejected);
            //     unset($o->order_cancelled);
            //     array_push($arrOrders['order_accepted'],$o);
            //   }

            //   else if($o->order_placed == 1 AND
            //      $o->order_accepted == 1 AND
            //      $o->order_ready == 1 AND 
            //      $o->order_delivered == 0 AND 
            //      $o->order_rejected == 0 AND 
            //      $o->order_cancelled == 0  
            //     ){
            //     unset($o->order_placed);
            //     unset($o->order_accepted);
            //     unset($o->order_ready);
            //     unset($o->order_delivered);
            //     unset($o->order_rejected);
            //     unset($o->order_cancelled);
            //     array_push($arrOrders['order_ready'],$o);
            //   }

            //   else if($o->order_placed == 1 AND
            //      $o->order_accepted == 1 AND
            //      $o->order_ready == 1 AND 
            //      $o->order_delivered == 1 AND 
            //      $o->order_rejected == 0 AND 
            //      $o->order_cancelled == 0  
            //     ){
            //     unset($o->order_placed);
            //     unset($o->order_accepted);
            //     unset($o->order_ready);
            //     unset($o->order_delivered);
            //     unset($o->order_rejected);
            //     unset($o->order_cancelled);
            //     array_push($arrOrders['order_delivered'],$o);

                
            //     $completed_orders = $completed_orders + 1;
            //     //echo "<pre>"; print_r($o); exit;
            //     $sub_total=$sub_total + $o->sub_total;
            //     $tip_total= $tip_total + $o->tip;
            //     $grand_total=$grand_total + $o->total;
            //   }

            //   else if($o->order_placed == 1 AND
            //      $o->order_accepted == 0 AND
            //      $o->order_ready == 0 AND 
            //      $o->order_delivered == 0 AND 
            //      $o->order_rejected == 1 AND 
            //      $o->order_cancelled == 0  OR 
            //      $o->order_placed == 0
            //     ){
            //     unset($o->order_placed);
            //     unset($o->order_accepted);
            //     unset($o->order_ready);
            //     unset($o->order_delivered);
            //     unset($o->order_rejected);
            //     unset($o->order_cancelled);
            //     array_push($arrOrders['order_rejected'],$o);

            //   }

            //   else if($o->order_placed == 1 AND
            //      $o->order_accepted == 0 AND
            //      $o->order_ready == 0 AND 
            //      $o->order_delivered == 0 AND 
            //      $o->order_rejected == 0 AND 
            //      $o->order_cancelled == 1  
            //     ){
            //     unset($o->order_placed);
            //     unset($o->order_accepted);
            //     unset($o->order_ready);
            //     unset($o->order_delivered);
            //     unset($o->order_rejected);
            //     unset($o->order_cancelled);
            //     array_push($arrOrders['order_cancelled'],$o);
            //   }
            // }

            $fileName = 'reports/'.date('Ymd').'/report_truck_id_'.$id.'_'.date('Y-m-d', strtotime("-1 days")).'.xlsx';
            //return Excel::download(new OrdersExport(1), 'report_'.date('Y-m-d').'.csv');exit;
            Excel::store(new DailyOrderReport($truck->id), 'public/'.$fileName);

            $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
            $pathToFile = $storagePath.$fileName;

            $result = DB::table('trucks')
                ->join('vendors', 'vendors.id', '=', 'trucks.vendor_id')
                ->select('trucks.name', 'trucks.alias','vendors.firstname','vendors.lastname','vendors.email')
                ->where('trucks.id', $id)
                ->get()->first();

              if(!empty($result->email)){

                  $data['total_orders_count'] = count($orders);
                  $data['truck_name'] = $result->name;
                  $data['truck_alias'] = $result->alias;
                  $data["report_date"] = date('Y-m-d', strtotime("-1 days"));
                  $data["total_order_count"] = $completed_orders;
                  $data["total_order_amount"] = round($sub_total / 100, 2);
                  $data["total_tip_amount"] =  round($tip_total / 100, 2);
                  $data["grand_amount"]  = round($grand_total / 100, 2);
                  
                  $mailToEmailId = $result->email;
                  $mailToName = $result->firstname." ".$result->lastname; 
                  $mailSubject = "Daily Report For ".date("F j, Y", strtotime("-1 days"));
                  
                  //echo "<pre>"; print_r($data);
                  Mail::send('emails.mail', $data, function($message) use($mailToEmailId , $mailToName, $mailSubject, $pathToFile){
                    $message->to($mailToEmailId, $mailToName)
                        ->subject($mailSubject)
                        ->attach($pathToFile)
                        ->from('donotreply@grilledchili.com','Grilledchili');
                  });
              }

            }
          }catch (Exception $ex) {
            return $ex->getMessage();
          } 
    }
}
