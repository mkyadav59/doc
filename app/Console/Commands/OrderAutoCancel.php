<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB, Exception, Log;
use App\order;

class OrderAutoCancel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:autocancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try 
        {
             $orders = DB::table('orders')
                    ->where(['order_placed' => 0, 'order_rejected' => 0, 'order_cancelled' => 0])
                    ->get();

              foreach ($orders as $o) {
                  
                  //check if order placed time is grater then updated_at - 15 min then cancel else update
                  if(strtotime($o->created_at) < (strtotime($o->updated_at) - 900)){
                      $r = order::where(['id' => $o->id])->first();
                      $r->order_status = "Cancelled";
                      $r->order_cancelled = 1;
                      $r->order_cancelled_time = Date('Y-m-d H:i:s', (strtotime($o->updated_at) + 60) );
                      $r->order_cancelled_remark = "auto_cancelled"; 
                      $r->updated_at = Date('Y-m-d H:i:s', (strtotime($o->updated_at) + 60) );
                      $r->save();

                  }else{
                      $r = order::where(['id' => $o->id])->first();
                      $r->updated_at = Date('Y-m-d H:i:s', (strtotime($o->updated_at) + 60) );
                      $r->save();
                  }

              }

        }catch (Exception $ex) {
            return  $ex->getMessage();
        }
    }
}
