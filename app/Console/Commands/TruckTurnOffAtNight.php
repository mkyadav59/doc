<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB, Exception;

class TruckTurnOffAtNight extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'truck:truckturnoffatnight';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $val = DB::update(DB::raw("update trucks set on_off = 0 where on_off = 1"));
        }
        catch(Exception $ex){
          return $ex->getMessage();
        }
    }
}
