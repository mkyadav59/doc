<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\v3\StripeController;

use Illuminate\Notifications\Notifiable;
use Edujugon\PushNotification\PushNotification;
use DB, Exception, Log;
use App\order, App\item, App\item_order;

class OrderAutoReject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:autoreject';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'order auto reject if not accepted within 15 min of placed.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try 
        {
             $orders = DB::table('orders')
                    ->where(['order_placed' => 1, 'order_accepted' => 0, 'order_cancelled' => 0, 'order_rejected' => 0])
                    ->get();
            //echo "<pre>"; print_r($order); exit();

            foreach ($orders as $o) {
                

                if((strtotime($o->updated_at) < strtotime($o->order_placed_time) + 1200)){
                    $r = order::where(['id' => $o->id])->first();
                    $r->updated_at = Date('Y-m-d H:i:s',strtotime($o->updated_at) + 60);
                    $r->save();
                  }

                //check if order placed time is grater then updated_at - 15 min then reject else update
                if( (strtotime($o->updated_at) > (strtotime($o->order_placed_time) + 900) )
                    &&
                    (strtotime($o->updated_at) < (strtotime($o->order_placed_time) + 980) )
                    ) {

                    // $arrItems=[];
                    // $orderItems = item_order::where(['order_id' => $o->id])->get();

                    // foreach ($orderItems as $items) {
                        
                    //     $requestedStock = $items->quantity;

                    //     if($requestedStock > 0 ){

                    //         $item = item::where(["id" => $items->item_id])->first();
                            
                    //         $item->instock = $item->instock + $requestedStock;
                    //         $item->save();

                    //     }
                    // }

                    // $r = order::where(['id' => $o->id])->first();
                    // $r->order_status = "Rejected";
                    // $r->order_rejected = 1;
                    // $r->order_rejected_time = Date('Y-m-d H:i:s',strtotime($o->updated_at) + 60);
                    // $r->order_rejected_remark = "auto_rejected"; 
                    // $r->updated_at = Date('Y-m-d H:i:s',strtotime($o->updated_at) + 60);
                    // $r->order_json = $this->updateOrderJson($o->id);
                    // $r->save();




                    // //REUND IF ORDER PAID BY STRIPE
                    // $refundMsg = '';

                    // if($r->payment_method == 'CARD'){
                        
                    //     $stripObj = new StripeController();
                    //     $result = $stripObj->refundOrder($o->id);

                    //     if($result == "succeeded"){

                    //         $r->payment_method = 'REFUNDED';
                    //         $r->save();
                            
                    //         $refundMsg = " Refund initiated.";
                    //     }
                    // }


                    //  //Update order JSON
                    // $r->order_json =  $this->updateOrderJson($o->id);
                    // $r->save();


                
                    $arrtoken = DB::table('customerdevicetokens')->where(['customer_id' => $o->customer_id, 'active' => 1])->orderBy('id', 'desc')->first();
              
                      if($arrtoken->device_type == 'android'){
                              $push = new PushNotification('fcm');
                              $push->setMessage([
                                              'data' => [
                                               'title'=>'Order #'.$o->id,
                                               'body'=>'Please call merchant to get order status.',
                                               'order_id' => $o->id,
                                               ],
                                          'sound' => 'default'

                                           ])
                              ->setDevicesToken($arrtoken->token)
                              ->send()
                              ->getFeedback();
                      }
                      else{

                              $push = new PushNotification('apn');

                              $message = [
                                  'aps' => [
                                  'alert' => [
                                      'title'=>'Order #'.$o->id,
                                      'body'=>'Please call merchant to get order status.',
                                      'order_id' => $o->id,
                                  ],
                                  'sound' => 'default'

                                  ]
                              ];
                              
                              $result = $push->setMessage($message)
                                  ->setDevicesToken($arrtoken->token)
                                  ->send()
                                  ->getFeedback();


                              $result->device_token = $arrtoken->token;
                        }


                    //SEND PUSH NOTIFICATION TO MERCHANT 

                    $arrTruckToken = DB::table('truckdevicetokens')
                                ->where(['truck_id' => $o->truck_id])
                                ->orderBy('id', 'desc')->pluck('token')->toArray(); 
                                  
                    if(!empty($arrTruckToken)){
                        $push = new PushNotification('apn');
                        
                        $push->setConfig([
                            'dry_run' => env('DRY_RUN', FALSE),
                            'passPhrase' => '1234',
                            'certificate' => base_path() . '/vendor/edujugon/push-notification/src/Config/iosCertificates/M_Certificate.pem',
                        ]); 

                        $message = [
                            'aps' => [
                                'alert' => [
                                      'title'=>'Order #'.$o->id,
                                      'body'=>'Order #'.$o->id.' is waiting get accepted since 15 minutes.',
                                      'order_id' => $o->id,
                                ],
                                'badge' => 1,
                                'sound' => 'default'

                            ]
                        ];

                        $result = $push->setMessage($message)
                            ->setDevicesToken($arrTruckToken)
                            ->send()
                            ->getFeedback();

                        if(!empty($result->tokenFailList)) {
                            foreach ($result->tokenFailList as $token) {
                                truckdevicetoken::where(['token' => $token])->delete();
                            }
                        }
                    }

                }


            }

        }catch (Exception $ex) {
            Log::error($ex->getMessage());
        }
    }


    public function updateOrderJson($id)
    {
        try 
        {
            //update order json
             $o= DB::table('orders')
                                    ->join('customers', 'orders.customer_id', '=', 'customers.id')
                                    ->select('orders.id', 'customers.firstname', 'customers.mobile', 'truck_id', 'pick_up_time', 'note', 'tip','total', 'order_status', 'order_placed_time') 
                                    ->where(['orders.id' => $id])
                                    ->first();


            $o->id = (int)$o->id;
            $o->firstname = $o->firstname;
            $o->truck_id = (int)$o->truck_id;
            $o->pick_up_time = date("h:iA", strtotime($o->order_placed_time . "+".$o->pick_up_time." minutes"));
            $o->tip = round($o->tip / 100, 2);
            $o->total = round($o->total /100, 2);


            

            $json= new \stdClass;
            $json->details = $o;

            $orderItems = DB::table('item_order')
                            ->join('items', 'items.id', '=', 'item_order.item_id')
                            ->select( 'items.id as item_id','items.name', 'item_order.quantity', 'item_order.price','item_order.note', 'item_order.customizations')
                            ->where('item_order.order_id',  $id)
                            ->get();
            foreach ($orderItems as $i) {
                $i->price = round($i->price / 100, 2);
            }
            $json->items = $orderItems;

            return json_encode($json);
        }catch (Exception $ex) {
           Log::error($ex->getMessage());
        }
    }
}
