<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\OrderAutoReject::class,
        Commands\OrderAutoCancel::class,
        Commands\MerchantDailyReportEmail::class,
        Commands\ResetDailyStock::class,
        Commands\TruckOffIfLocationNotUpdated::class,
        Commands\TruckTurnOffAtNight::class,
        Commands\CheckSubscriptions::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //EVERY MINUTE CALLS
        $schedule->command('order:autoreject')->everyMinute();
        $schedule->command('order:autocancel')->everyMinute();
        //$schedule->command('truck:truckoffiflocationnotupdated')->everyMinute();

        //MIDNIGHT CALLS
        $schedule->command('email:merchantdailyreport')->dailyAt('01:00');
        $schedule->command('auto:resetdailystock')->dailyAt('01:00');
        $schedule->command('truck:truckturnoffatnight')->dailyAt('01:00');


        $schedule->command('backup:clean')->dailyAt('01:00');
        $schedule->command('backup:run --only-db')->dailyAt('02:00');

        # Check subscription
        $schedule->command('subscriptions:check')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
