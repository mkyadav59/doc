<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    public function trucks()
    {
    	return $this->belongsTo(truck::class);
    }

    public function items()
    {
    	return $this->hasMany(item::class);
    }
}
