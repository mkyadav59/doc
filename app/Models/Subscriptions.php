<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
    protected $fillable = ['truck_subscription_id', 'stripe_id', 'stripe_status', 'name']; 
    protected $table = 'subscriptions';
}
