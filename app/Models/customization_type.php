<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class customization_type extends Model
{
    public function items()
    {
    	return $this->belongsTo(item::class);
    }

    public function customization_options()
    {
    	return $this->hasMany(customization_option::class);
    }
}
