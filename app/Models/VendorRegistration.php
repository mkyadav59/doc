<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorRegistration extends Model
{
    protected $fillable = ['name', 'mobile', 'email', 'password', 'created_at']; 
}
