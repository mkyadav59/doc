<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class truck_setting extends Model
{
    public function trucks()
    {
    	return $this->belongsTo(truck::class);
    }
}