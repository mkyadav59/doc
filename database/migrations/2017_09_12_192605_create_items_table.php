<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('truck_id')->index();
            $table->integer('category_id')->index();
            $table->string('name', 255);
            $table->string('description', 255);
            $table->integer('price');
            $table->string('img', 255)->nullable();
            $table->integer('veg');
            $table->integer('dailystock')->default(0);
            $table->integer('instock')->default(0);
            $table->integer('order');
            $table->integer('primary');
            $table->tinyInteger('parent_id')->default(0);
            $table->integer('active')->default(1);
            $table->timestamps();

            $table->foreign('truck_id')->references('id')->on('trucks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
