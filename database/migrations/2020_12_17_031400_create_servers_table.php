<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        if(!Schema::hasTable('servers')){
            Schema::create('servers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('truck_id')->index();
                $table->string('name', 255);
                $table->string('country_code', 2);
                $table->string('mobile', 20);
                $table->integer('send_sms')->default(0);
                $table->integer('active')->default(1);
                $table->timestamps();

                $table->foreign('truck_id')->references('id')->on('trucks')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
