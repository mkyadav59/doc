<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppVersionToTruckdevicetokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('truckdevicetokens', function (Blueprint $table) {
            $table->string('app_version',10)->after('app_type')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('truckdevicetokens', function (Blueprint $table) {
            $table->dropColumn('app_version');
        });
    }
}
