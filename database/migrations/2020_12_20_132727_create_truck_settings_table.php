<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTruckSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truck_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('truck_id');
            $table->integer('tax')->default(0);
            $table->integer('stripe_connect')->default(0);
            $table->integer('stripe_device')->default(0);
            $table->integer('accept_cash')->default(0);
            $table->integer('max_cash')->default(0);
            $table->integer('accept_card')->default(0);
            $table->integer('min_card')->default(0);
            $table->integer('pickup_options')->default(1);
            $table->integer('service_tables')->default(0);
            $table->integer('pickup_stations')->default(0);
            $table->integer('servers')->default(0);
            $table->integer('order_auto_accept')->default(0);
            $table->integer('active')->default(1);
            $table->timestamps();

            $table->foreign('truck_id')->references('id')->on('trucks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('truck_settings');
    }
}
