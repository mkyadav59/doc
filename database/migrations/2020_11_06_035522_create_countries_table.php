<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('mytable')){
            Schema::create('countries', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',50);
                $table->string('icon',100);
                $table->integer('country_code');
                $table->string('currency_sign',50);
                $table->integer('order');
                $table->integer('active')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
