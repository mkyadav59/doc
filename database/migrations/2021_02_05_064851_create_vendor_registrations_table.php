<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_registrations', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('mobile',15);
            $table->string('email',50);
            $table->string('password');
            $table->timestamp('email_verify')->nullable();
            $table->integer('email_verify_status')->default(0);
            $table->text('email_verify_token')->nullable();
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_registrations');
    }
}
