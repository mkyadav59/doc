<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vendor_id')->index();
            $table->string('name',255);
            $table->string('logo',255)->nullable();
            $table->string('latitude',25)->nullable();
            $table->string('longitude',25)->nullable();
            $table->string('location_updated_at',20)->nullable();
            $table->string('description',500)->nullable();
            $table->string('phone',20)->nullable();
            $table->string('website',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('operatingtime',50)->nullable();
            $table->string('weekdaytime',50)->nullable();
            $table->string('weekendtime',50)->nullable();
            $table->float('rating', 2, 1)->nullable();
            $table->tinyInteger('on_off')->default(1);
            $table->string('stripe_connect_id', 50);
            $table->tinyInteger('stripe_status')->default(0);
            $table->integer('active')->default(1);
            $table->timestamps();

            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucks');
    }
}
