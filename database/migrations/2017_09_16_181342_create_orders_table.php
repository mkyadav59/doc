<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id')->index();
            $table->unsignedBigInteger('truck_id')->index();
            $table->integer('pick_up_time')->nullable();
            $table->dateTime('order_wanted_time')->nullable();
            $table->string('note', 255)->nullable();
            $table->integer('sub_total')->nullable();
            $table->integer('tip')->nullable();
            $table->integer('total')->nullable();
            $table->string('order_status', 20)->nullable();
            $table->string('order_status_notification', 255)->nullable();
            $table->tinyInteger('order_paid')->default(0);
            $table->string('payment_method', 50)->nullable();
            $table->tinyInteger('order_placed')->default(0);
            $table->dateTime('order_placed_time')->nullable();
            $table->tinyInteger('order_accepted')->default(0);
            $table->dateTime('order_accepted_time')->nullable();
            $table->string('order_acceptance_interval', 10)->nullable();
            $table->tinyInteger('order_ready')->default(0);
            $table->dateTime('order_ready_time')->nullable();
            $table->string('order_ready_interval',10)->nullable();
            $table->tinyInteger('order_delivered')->default(0);
            $table->dateTime('order_delivered_time')->nullable();
            $table->string('order_pickup_interval',10)->nullable();
            $table->tinyInteger('order_cancelled')->default(0);
            $table->dateTime('order_cancelled_time')->nullable();
            $table->string('order_cancelled_remark',255)->nullable();
            $table->tinyInteger('order_rejected')->default(0);
            $table->dateTime('order_rejected_time')->nullable();
            $table->string('order_rejected_interval',10)->nullable();
            $table->string('order_rejected_remark',255)->nullable();
            $table->text('order_json')->nullable();
            $table->string('lat', 20)->nullable();
            $table->string('long', 20)->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('truck_id')->references('id')->on('trucks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
