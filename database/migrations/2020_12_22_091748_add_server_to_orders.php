<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServerToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('server_id')->after('total')->nullable()->default(0);
            $table->string('server_name', 50)->after('server_id')->nullable();

            $table->integer('service_table_id')->after('server_id')->nullable()->default(0);
            $table->string('service_table_name', 50)->after('service_table_id')->nullable();

            $table->integer('pickup_station_id')->after('service_table_name')->nullable()->default(0);
            $table->string('pickup_station_name', 50)->after('pickup_station_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('server_id');
            $table->dropColumn('server_name');
            $table->dropColumn('service_table_id');
            $table->dropColumn('service_table_name');
            $table->dropColumn('pickup_station_id');
            $table->dropColumn('pickup_station_name');
        });
    }
}
