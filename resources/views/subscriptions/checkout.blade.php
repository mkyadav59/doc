@extends('layouts.app')
@section('title', 'Dashboard')
@section('head')
  <title>Subscription Checkout</title>
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Load Stripe.js on your website. -->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/normalize.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/global.css') }}" />
@endsection

@section('content')
<style>
 .price-table-container{
    width: 500px;
 }
</style>
<div class="togethere-background"></div>
    <div class="sr-root">
      <div class="sr-main">
        <header class="sr-header">
          <div class="sr-header__logo">
          </div>
        </header>
        <h1>Checkout Payment</h1>

        <div class="price-table-container">
          <section>
            <div  class="name" id="p_id" data-plan-name="{{$data['plan_name']}}" data-plan-key="{{$data['plan_id']}}">Subscribe to <b style="color:brown">{{$data['plan_name']}}</b></div>
            <div class="price" id="p_price" data-plan-price="{{$data['price']}}">${{$data['price']}}</div>
            @if($data['plan_name'] == 'Basic')
                <div class="duration">For 3 Month</div>
            @endif
            @if($data['plan_name'] == 'For Silver')
                <div class="duration">For 6 Month</div>
            @endif
            @if($data['plan_name'] == 'Gold')
                <div class="duration">For 1 Year</div>
            @endif
          </section>
          <section style="width:350px">
              <div class="name">Pay with card</div>
              <div class="card" style="width:300px">    
                <input placeholder="Card Holder" class="form-control" id="card-holder-name" type="text">
                <div id="card-element"></div>
                <span id="error" style="color:red"></span>
            </div>
            <button id="card-button" data-secret="{{$data['intent']['client_secret']}}">
                Subscribe
            </button>
            <a href="{{route('subscribe')}}"  style="text-decoration:none">
                <button id="card-button" style="color:red">
                    Cancel
                </button> 
            </a>
            </section>
        </div>
      </div>
    </div>
    <div id="error-message" class="error-message"></div>
@endsection

@section('js')
<script src="https://js.stripe.com/v3/"></script>
<script>
        window.addEventListener('load', function() {
            const stripe = Stripe('{{env('STRIPE_KEY')}}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#card-element');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;
            const planName =   document.getElementById("p_id").getAttribute('data-plan-name');
            const planKey =  document.getElementById("p_id").getAttribute('data-plan-key');
            const price =  document.getElementById("p_price").getAttribute('data-plan-price');

            cardButton.addEventListener('click', async (e) => {
                const { setupIntent, error } = await stripe.handleCardSetup(
                    clientSecret, cardElement, {
                        payment_method_data: {
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );
                if (error) {
                   if(error['code'] !=''){
                        var msg; 
                        if(error['code'] != '' && error['message'] != ''){
                            msg = error['message'];
                            alert(msg);
                        }
                   }
                } else {
                    axios.post('/get-subscribe',{
                        payment_method: setupIntent.payment_method,
                        plan_types : planName,
                        plan_key: planKey,
                        price: price,
                    }).then((data)=>{
                        var app_url = {!! json_encode(url('/')) !!};
                        if(data['data']['status'] != 'undefined'){
                            var status = data['data']['status'] ;
                            if(!status){
                                var msg = data['data']['message'];
                                alert(msg);
                                var url = app_url+'/subscribe';
                                window.location.replace(url);
                            }else{
                                var app_url = {!! json_encode(url('/')) !!};
                                var url = app_url+'/payment-success';
                                window.location.replace(url);
                            }
                        }   
                    });
                }
            });
        })
    </script>

@endsection
