@extends('layouts.app')
@section('title', 'Success')
@section('head')
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Load Stripe.js on your website. -->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/normalize.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/global.css') }}" />
@endsection

@section('content')
<style>
 .price-table-container{
    width: 500px;
 }
</style>
<div class="togethere-background"></div>
    <div class="sr-root">
      <div class="sr-main">
        <header class="sr-header">
          <div class="sr-header__logo"></div>
        </header>
        <div class="sr-payment-summary completed-view">
            <h1>Your payment succeeded ..!</h1>
            <a href="{{route('subscribe')}}"  style="text-decoration:none">
                <button id="card-button" style="color: blue">
                    Back
                </button> 
            </a>
          </div>
          <div class="completed-view-section">
            <form id="manage-billing-form">
              <button>Manage Billing</button>
            </form>
          </div>
        </div>
        <div class="sr-content">
    </div>
    </div>
</div>
@endsection
