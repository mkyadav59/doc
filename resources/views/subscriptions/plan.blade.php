@extends('layouts.app')
@section('title', 'Subscribe')
@section('head')
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Load Stripe.js on your website. -->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/normalize.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/global.css') }}" />
@endsection

@section('content')
  <div class="togethere-background"></div>
      <div class="sr-root">
        <div class="sr-main">
          <header class="sr-header">
            <div class="sr-header__logo"></div>
          </header>
          <h1>Choose a collaboration plan</h1>
          <div class="price-table-container">
            @foreach($data['plans'] as $plan)
              <section>
                @if($plan['plan_types'] == 'Basic')
                  <img
                    src="{{asset('images/starter.png')}}"
                    width="120"
                    height="120"
                  />
                @else
                <img
                    src="{{asset('images/professional.png')}}"
                    width="120"
                    height="120"
                  />
                @endif
                <div class="name">{{$plan['plan_types']}}</div>
                <div class="price">${{$plan['amount']}}</div>
                <div class="duration">For {{$plan['interval_count']}} {{$plan['interval']}}</div>
                <button id="{{$plan['plan_types']}}-plan-btn">Select</button>
              </section>
            @endforeach
        </div>
      </div>
    </div>
  <div id="error-message" class="error-message"></div>

@endsection

@section('js')
<script src="{{asset('js/script.js')}}"></script>
@endsection
