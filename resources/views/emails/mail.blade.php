
<div id=":3y6" class="ii gt"><div id=":3y7" class="a3s aXjCH " role="gridcell" tabindex="-1"><u></u>

    <div>
      <u></u>
      
          <div>
            <div style="width:649px;margin:0 auto;border:#ececec solid 1px">
              <div style="padding:22px 34px 15px 34px">
                <div>
                  <br>
                    <img title="GrilledChili Logo" src="https://grilledchili.com/wp-content/uploads/2020/06/web_logo_small-2.png" alt="GrilledChili" class="CToWUd">
                    </div>
						<div style="float:left;color:#333333;font:normal 14px Arial,Helvetica,sans-serif;width:250px;margin-top:18px;">
							  
							<div style="font:bold 21px Arial,Helvetica,sans-serif;margin-top:10px">
							{{$truck_name}} <span style="color: red">({{$truck_alias}})</span>
							</div>
							<div style="font:bold 12px Arial,Helvetica,sans-serif;margin-top:10px">Daily Transaction Report</div>
							<div style="font:bold 12px Arial,Helvetica,sans-serif;">{{$report_date}}</div>
                        </div>


                        <div style="float:right;width:300px;margin-top:41px">
                          <div style="float:left;font:normal 12px Arial,Helvetica,sans-serif;color:#999999;width:210px;text-align:right;margin-top:10px;margin-left:6px">
                            <br> We are here for you! 
							<br><a href="mailto:support@grilledchili.com" target="_blank">support@grilledchili.com</a> to reach us.
                        
                              </div>
                              <div style="float:right">
                                <img title="GrilledChili Seal of trust" src="https://grilledchili.com/wp-content/uploads/2020/06/seal-of-trust.png" alt="GrilledChili Seal of trust" class="CToWUd">
                                </div>
                              </div>
                              <div style="clear:both"></div>
                            </div>
                            <div style="width:584px;background-color:#ffffff;border:#e8e7e7 solid 1px;padding:27px 0;margin:0 auto;border-bottom:0">
                              <div style="border-bottom:#717171 dotted 1px;font:normal 14px Arial,Helvetica,sans-serif;color:#666666;padding:0px 33px 10px">
                                <br>
                                  <table style="width:100%" border="0" cellspacing="0" cellpadding="2">
                                    <tbody>
                                      <tr>
                                        <td width="450px">Total number of orders completed {{$total_order_count}} </td>
                                        <td><div style="text-align: right;">${{$total_order_amount}}</div></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div style="border-bottom:#717171 dotted 1px;font:normal 14px Arial,Helvetica,sans-serif;color:#666666;padding:10px 33px 10px">
                                  <br>
                                    <table style="width:100%" border="0" cellspacing="0" cellpadding="2">
                                      <tbody>
                                        <tr>
                                          <td width="450px">Total Tip amount </td>
                                        <td><div style="text-align: right;">${{$total_tip_amount}}</div></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div style="border-bottom:#717171 dotted 1px;font:600 14px Arial,Helvetica,sans-serif;color:#333333;padding:17px 33px 17px">
                                    <table style="width:100%" border="0" cellspacing="0" cellpadding="2">
                                      <tbody>
                                        <tr>
                                          <td width="450px">Total Amount</td>
                                          <td><div style="font:bold 25px Arial,Helvetica,sans-serif;">${{$grand_amount}}</div></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>    
                                  
                                        <form action="https://survey.grilledchili.com/dailyreport/feedbackform" method="GET" style="margin:0;font:normal 12px Arial,Helvetica,sans-serif" id="m_7480946395694996270surveyForm" target="_blank">
                                          <table style="padding:15px" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                              <tr>
											  <td>
											  <div style="margin-top:15px;color:#666;font-size:11px">
												Your response will help us serve you better!       
											  </div>
											  <div style="margin-top:15px;color:rgb(102,102,102);font-size:11px;line-height:13px;margin-top:28px">          Disclaimer:  Please do not share your confidential information with anyone even if he/she claims to be from GrilledChili. We advise our customers to completely ignore such communications. 

										  
												<br>
												<br> If you did not create this account on GrilledChili and you think someone else has used your Email ID to create an account, please contact us.
										  
												<a href="mailto:support@grilledchili.com" target="_blank">click here</a>
											  </div>
											</td>
										  </tr>
										</tbody>
									  </table>
									</form>
								  </div>
								  <div style="margin:0 auto;width:594px">
									<img title="" src="https://grilledchili.com/wp-content/uploads/2020/06/unnamed.png" alt="" class="CToWUd">
								  </div>
                                                                                      
                                                                                            

																		<div class="yj6qo"></div>

																		<div class="adL">
																		</div></div><div class="adL">
																																								
																		</div></div></div></div>